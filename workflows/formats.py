#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2015 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import jflow
from jflow import seqio
from jflow.featureiolib.gff3 import GFF3IO,GFF3Record
import os
import sys

def any(ifile):
    pass

def bam(ifile):
    pass

def srnatabacfasta(ifile):
    try:
        reader = seqio.FastaReader(ifile, wholefile=True)
        nb_seq = 0
        for sid, desc, seq, qualities in reader:
            nb_seq += 1
            # >lmo0001_CDS|317-1673|+|317-1673|mRNA
            # >rli13-rliE|1584586-1584808|+|ncRNA
            if '|' not in sid: raise
            tmpid = sid[1:].split('|') if '>' in sid else sid.split('|')
            if len(tmpid) != 4 and len(tmpid) != 5: raise
            #num test
            if '-' not in tmpid[1]: raise
            coords = tmpid[1].split('-')
            if not coords[0].isdigit() or not coords[1].isdigit(): raise
            #strand test
            if len(tmpid[2]) not in [1, 2]: raise
            if tmpid[2][0] not in ['+', '-']: raise
            #ncrna
            if len(tmpid) == 4:
                if tmpid[3] != 'ncRNA': raise
            else: #mrna
                if tmpid[4] != 'mRNA': raise
                if '_' not in tmpid[0]: raise
                if tmpid[0].split('_')[1] not in ['CDS', '5UTR', '3UTR']: raise
                if '-' not in tmpid[3]: raise
                coords = tmpid[3].split('-')
                if not coords[0].isdigit() or not coords[1].isdigit(): raise

            # only check the first 10 sequences
            if nb_seq == 10: break
    except:
        raise jflow.InvalidFormatError("The provided file '{}' is not a well-formed fasta file! See the documentation".format(ifile))

def gff3(ifile):
    try:
        reader = GFF3IO(ifile, mode='r')
        nb_seq = 0
        for rec in reader:
            # check attributes
            rec.seq_id
            rec.source
            rec.type
            rec.start
            rec.end
            rec.score
            rec.strand
            rec.phase
            rec.attributes

            nb_seq += 1
            # only check the first 10 sequences
            if nb_seq > 10:
                break
    except:
        raise jflow.InvalidFormatError("The provided file '{}' is not a gff3 file!".format(ifile))
    finally:
        reader.close()

def fastq(ifile):
    try:
        reader = seqio.FastqReader(ifile)
        nb_seq = 0
        for id, desc, seq, qualities in reader:
            nb_seq += 1
            # only check the first 10 sequences
            if nb_seq == 10:
                break
    except:
        raise jflow.InvalidFormatError("The provided file '{}' is not a fastq file!".format(ifile))

def fasta(ifile):
    try:
        reader = seqio.FastaReader(ifile, wholefile=True)
        nb_seq = 0
        for id, desc, seq, qualities in reader:
            nb_seq += 1
             # only check the first 10 sequences
            if nb_seq == 10:
                break
    except:
        raise jflow.InvalidFormatError("The provided file '{}' is not a fasta file!".format(ifile))

def sff(ifile):
    try:
        reader = seqio.SFFReader(ifile)
        nb_seq = 0
        for id, desc, seq, qualities in reader:
            nb_seq += 1
            # only check the first 10 sequences
            if nb_seq == 10:
                break
    except:
        raise jflow.InvalidFormatError("The provided file '{}' is not a sff file!".format(ifile))

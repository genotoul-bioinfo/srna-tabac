#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro, Anais Painset - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2015 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.1'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import os

from jflow.component import Component
from weaver.function import PythonFunction


def create_alignlike_output(list_pred, align_output):
    """ Create Alignment-like output based on interactions
    
    Keyword arguments:
    list_pred -- the predictions/interactions list
    align_output -- the corresponding alignment output
    """
    import pickle
    import gzip
    from srnatabac.core.interaction import Interaction, InteractionDetail
    import csv
    # Load interactions
    predictions = []
    with gzip.open(list_pred, 'rb') as f:
        predictions.extend(pickle.load(f))
    with open(align_output, 'w') as f:
        if len(predictions) != 0:
            for i in predictions:
                f.write(i.get_alignment_like())
        else:
            f.write("No predictions. Nothing passed threshold!")


def create_gff3_output(list_pred, gff3_output):
    """ Create GFF3 output based on interactions
    
    Keyword arguments:
    list_pred -- the predictions/interactions list
    gff3_output -- the corresponding gff3 output
    """
    import pickle
    import gzip
    from srnatabac.core.interaction import Interaction, InteractionDetail
    from srnatabac.utils.gff3_converter import GFF3Converter
    from jflow.featureio import GFF3IO, GFF3Record
    # Load interactions
    predictions = []
    with gzip.open(list_pred, 'rb') as f:
        predictions.extend(pickle.load(f))
    if len(predictions) != 0:
        target_dict = {}
        actor_dict = {}
        pred_gff_dict = []
        for i in predictions:
            if i.mrna_id not in list(target_dict.keys()):
                target_dict[i.mrna_id] = GFF3Converter.from_mrna(i.mrna)
            if i.ncrna_id not in list(actor_dict.keys()):
                actor_dict[i.ncrna_id] = GFF3Converter.from_ncrna(i.ncrna)
            pred_gff_dict.append(GFF3Converter.from_interaction(i))

        # gff3 directives
        with open(gff3_output, mode='a') as f:
            f.write('##gff-version 3\n')
        # interactions
        f = GFF3IO(gff3_output, mode='a')
        for actor in list(actor_dict.values()):
            f.write(actor)
        for target in list(target_dict.values()):
            f.write(target)
        for gff_rec in pred_gff_dict:
            f.write(gff_rec)
        f.close()
    else:  # empty
        with open(gff3_output, 'a') as f:
            f.write("No predictions. Nothing passed threshold!")


def create_csv_output(list_pred, csv_output):
    """ Create CSV output based on interactions
    /!\ the CSV output can be really HEAVY compared to GFF3 /!\
    
    Keyword arguments:
    list_pred -- the predictions/interactions list
    csv_output -- the corresponding gff3csv output
    """
    import pickle
    import gzip
    import csv
    from srnatabac.core.interaction import Interaction, InteractionDetail
    # Load interactions
    predictions = []
    with gzip.open(list_pred, 'rb') as f:
        predictions.extend(pickle.load(f))
    if len(predictions) != 0:
        with open(csv_output, 'w', newline='') as csvfile:
            # header
            fieldnames = [
                'id', 'mrna_id', 'ncrna_id', 'region_id', 'software',
                'mrna_begin', 'mrna_end', 'mrna_seq',
                'ncrna_begin', 'ncrna_end', 'ncrna_seq',
                'symbolism', 'energy_software', 'energy_duplex',
                'overlap_length', 'overlap_start', 'overlap_stop',
                'pos_to_gene_start', 'pos_to_gene_stop',
                'match_count', 'match_successive',
                'pair_pct_AU', 'pair_pct_GC', 'pair_pct_GU'
            ]
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames, quoting=csv.QUOTE_ALL)
            writer.writeheader()
            # data
            for p in predictions:
                writer.writerow({
                    'id': p.id,
                    'mrna_id': p.mrna_id,
                    'ncrna_id': p.ncrna_id,
                    'region_id': p.region_id,
                    'software': p.details.software,
                    'mrna_begin': p.details.mrna_begin,
                    'mrna_end': p.details.mrna_end,
                    'mrna_seq': ''.join(p.details.mrna_seq),
                    'ncrna_begin': p.details.ncrna_begin,
                    'ncrna_end': p.details.ncrna_end,
                    'ncrna_seq': ''.join(p.details.ncrna_seq),
                    'symbolism': ''.join(p.details.symbolism),
                    'energy_software': p.details.energy_software,
                    'energy_duplex': p.details.energy_duplex,
                    'overlap_length': p.details.overlap_length,
                    'overlap_start': p.details.overlap_start,
                    'overlap_stop': p.details.overlap_stop,
                    'pos_to_gene_start': p.details.pos_to_gene_start,
                    'pos_to_gene_stop': p.details.pos_to_gene_stop,
                    'match_count': p.details.match_count,
                    'match_successive': p.details.match_successive,
                    'pair_pct_AU': p.details.pair_pct_AU,
                    'pair_pct_GC': p.details.pair_pct_GC,
                    'pair_pct_GU': p.details.pair_pct_GU
                })
    else:
        with open(csv_output, 'w') as f:
            f.write("No predictions. Nothing passed threshold!")


def index_predictions(pred, ind):
    """ Create a indexed pgz interactions file output based on interactions
    
    Keyword arguments:
    pred -- the predictions/interactions list
    ind -- the corresponding indexed interactions output
    """
    import pickle
    import gzip
    from srnatabac.core.interaction import Interaction, InteractionDetail
    from srnatabac.utils.common import Common
    # build prediction list
    predictions = []
    with gzip.open(pred, 'rb') as reader:
        predictions.extend(pickle.load(reader))
    # build prediction index list (same order)
    predictions_index = []
    for p in predictions:
        predictions_index.append(p.export_index())
    # pickle it
    with gzip.open(ind, 'ab') as writer:
        pickle.dump(predictions_index, writer, protocol=Common.PICKLE_COMPRESSION_RATE)


def summarize(ask_gff3, ask_align, ask_csv, pred, summ):
    import pickle
    import gzip
    empty_pred = False
    out_format = ['indexes']
    # build prediction list
    predictions = []
    with gzip.open(pred, 'rb') as reader:
        predictions.extend(pickle.load(reader))
    if len(predictions) == 0:
        empty_pred = True
        predictions = None
    if ask_gff3:
        out_format.append('gff3')
    if ask_csv:
        out_format.append('csv')
    if ask_align:
        out_format.append('alignment-like')
    # summary
    with open(summ, 'w') as f:
        f.write("- Output format: {}.\n".format(', '.join(out_format)))
        if empty_pred:
            f.write("- ... but no predictions were found, it seems no predictions passed the threshold.\n")


class FormatOutput (Component):
    """ Component: format output into several formats. """

    def define_parameters(self, final_predictions, align_output, gff3_output, csv_output):
        # PARAM
        self.add_parameter("align_output", "Alignment-like output",
                           default=align_output, required=False)
        self.add_parameter("gff3_output", "GFF3 output", default=gff3_output, required=False)
        self.add_parameter("csv_output", "CSV output", default=csv_output, required=False)
        # INPUTS
        self.add_input_file("predictions", "All predictions",
                            default=final_predictions, required=True)
        # OUTPUTS
        self.add_output_file("indexes", "The file containing prediction indexes",
                             filename="predictions_index.pgz")
        self.add_output_file("index_stderr", "Standard error for index", filename="index.stderr")

        if align_output == True:
            self.add_output_file(
                "predictions_align", "Produce interactions in alignment-like format", filename="predictions_alignlike.txt")
            self.add_output_file(
                "alignlike_stderr", "Standard error for alignment-like file creation", filename="alignlike_output.stderr")

        if gff3_output == True:
            self.add_output_file("predictions_gff3",
                                 "Produce interactions in GFF3 format", filename="predictions.gff3")
            self.add_output_file(
                "gff3_stderr", "Standard error for GFF3 file creation", filename="gff3_output.stderr")

        if csv_output == True:
            self.add_output_file(
                "predictions_csv", "Produce interactions in CSV format", filename="predictions.csv")
            self.add_output_file(
                "csv_stderr", "Standard error for CSV file creation", filename="csv_output.stderr")

        self.add_output_file(
            "summary", "Summary of all actions done in this component", filename="sum_tmp.txt")
        self.add_output_file("output_summary_stderr",
                             "Standard error for summary output", filename="output_summary.stderr")

    def process(self):
        if self.align_output == True:
            align_out = PythonFunction(create_alignlike_output,
                                       cmd_format="{EXE} {IN} {OUT}  2>> " + self.alignlike_stderr)
            align_out(inputs=self.predictions, outputs=[self.predictions_align])

        if self.gff3_output == True:
            gff3_out = PythonFunction(
                create_gff3_output, cmd_format="{EXE} {IN} {OUT}  2>> " + self.gff3_stderr)
            gff3_out(inputs=self.predictions, outputs=[self.predictions_gff3])

        if self.csv_output == True:
            csv_out = PythonFunction(
                create_csv_output, cmd_format="{EXE} {IN} {OUT}  2>> " + self.csv_stderr)
            csv_out(inputs=self.predictions, outputs=[self.predictions_csv])
        # index predictions
        indexer = PythonFunction(
            index_predictions, cmd_format="{EXE} {IN} {OUT} 2>> " + self.index_stderr)
        indexer(inputs=self.predictions, outputs=[self.indexes])
        # summarize
        sum_up = PythonFunction(
            summarize, cmd_format="{EXE} {ARG} {IN} {OUT} 2>> " + self.output_summary_stderr)
        sum_up(arguments=[self.gff3_output, self.align_output, self.csv_output],
               inputs=self.predictions, outputs=[self.summary])

#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2015 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

from jflow.component import Component
from weaver.function import PythonFunction

def index_predictions(pred, ind):
    import pickle
    import gzip
    from srnatabac.core.interaction import Interaction, InteractionDetail
    from srnatabac.utils.common import Common
    # build prediction list
    predictions = []
    with gzip.open(pred, 'rb') as reader:
        predictions.extend(pickle.load(reader))
    # build prediction index list (same order)
    predictions_index = []
    for p in predictions:
        predictions_index.append(p.export_index())
    # pickle it
    with gzip.open(ind, 'ab') as writer:
        pickle.dump(predictions_index, writer, protocol=Common.PICKLE_COMPRESSION_RATE)

def summarize(unused, sum_file):
    with open(sum_file, 'w') as s:
        s.write("- Indexes created.\n")

class IndexPredictions(Component):
    """ Component: create indexes from gzip predictions. """

    def define_parameters(self, final_predictions):
        # INPUTS
        self.add_input_file("predictions", "All predictions", default=final_predictions, required=True)
        # OUTPUTS
        self.add_output_file("indexes", "Summary of all actions done in this component", filename="predictions_index.pgz")
        self.add_output_file("summary", "Summary of all actions done in this component", filename="sum_tmp.txt")
        # ERRORS
        self.add_output_file("stderr", "Standard error for index", filename="index.stderr")

    def process(self):
        # main function
        indexer = PythonFunction(index_predictions, cmd_format="{EXE} {IN} {OUT} 2>> " + self.stderr)
        indexer( inputs=self.predictions, outputs=[self.indexes] )

        #summ
        sum_function = PythonFunction(summarize, cmd_format="{EXE} {IN} {OUT}")
        sum_function( inputs=self.indexes, outputs=self.summary )

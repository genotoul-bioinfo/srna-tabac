#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2015 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import os
from jflow.component import Component
from weaver.function import PythonFunction
from weaver.abstraction import Map

def summarize(project_name, output_information, output_summary, *all_input_sum_files):
    """ Aggregates all sum files from all components.

    Keywords arguments:
    project_name -- the project name
    output_information -- the project information file
    output_summary -- the final file path
    all_input_sum_files -- the list of all sum files
    """
    import pickle
    import gzip
    from srnatabac.utils.common import Common

    # save project informations
    with gzip.open(output_information, 'wb') as f:
        project_name = project_name if project_name != '' and project_name != None else 'Untitled'
        pickle.dump({'name':project_name}, f, protocol=Common.PICKLE_COMPRESSION_RATE)

    # create summary file
    with open(output_summary, 'a') as s:
        s.write("------------------------------------\n")
        s.write("-- Project: {}\n".format(project_name))
        s.write("------------------------------------\n")

        # for all summary files
        for sf in all_input_sum_files:
            with open(sf, 'r') as fsum:
                s.write('{}\n'.format(fsum.read()))

class Summarize(Component):
    """ Component: aggregates all sum files from all components into one file. """

    def define_parameters(self, p_project_name, sum_all):
        # PARAMETERS
        self.add_parameter("project_name", "The project name.", default=p_project_name, type="str", required=True)

        # INPUTS
        self.add_input_file_list("all_sum_files", "All sum files from all components of the workflow", default=sum_all, required=True)

        # OUTPUT
        self.add_output_file("project_summary", "Summary of all actions of the workflow", filename="project_summary.txt")
        self.add_output_file("project_information", "Informations about the project", filename="project_info.pgz")

        # ERROR
        self.add_output_file("summarize_stderr", "Error summarize", filename="summarize.stderr")

    def process(self):

        # dump all sum files from different component
        summarize_func = PythonFunction(summarize, cmd_format = "{EXE} {ARG} {OUT} {IN} 2>> " + self.summarize_stderr)
        summarize_func( arguments=['"' + self.project_name + '"'], outputs=[self.project_information, self.project_summary], inputs=[self.all_sum_files])

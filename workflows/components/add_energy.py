#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro, Anais Painset - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2015 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.1'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import os
from jflow.component import Component
from weaver.function import PythonFunction, ShellFunction

def build_prernaeval(interaction_tmp_file, pre_rnaeval):
    """ Get and add the duplex energy to the parsed file
    
    Keyword arguments:
    interaction_tmp_file -- the temporary interaction file without energy
    pre_rnaeval -- the pre-rnaeval file needed to got duplex energy
    """
    import pickle
    import gzip
    from srnatabac.core.interaction import Interaction, InteractionDetail
    interactions = []
    with gzip.open(interaction_tmp_file, 'rb') as f:
        interactions.extend(pickle.load(f))
    with open(pre_rnaeval, 'w') as f:
        for i in interactions:
            seq, struct = i.get_dbn()
            f.write(">{}\n{}\n{}\n".format(i.id, seq, struct))

def add_energy(interaction_tmp_file, structure_file, energy_file, interaction_final_file):
    """ Get and add the duplex energy to the parsed file
    
    Keyword arguments:
    interaction_tmp_file -- the temporary interaction file without energy
    structure_file -- the structure and sequence file
    energy_file -- the energy file
    interaction_final_file -- the interaction_final_file file,
    """
    import pickle
    import gzip
    import os
    from srnatabac.utils.common import Common
    from srnatabac.core.interaction import Interaction, InteractionDetail
    from srnatabac.core.strand import Strand
    from srnatabac.core.region import Region

    # build prediction list
    predictions = []
    with gzip.open(interaction_tmp_file, 'rb') as f:
        predictions.extend(pickle.load(f))

    # open structure and energy files
    if not os.path.isfile(structure_file):
        raise IOError('File "{}" not found or it is not a file.'.format(structure_file))
    if not os.path.isfile(energy_file):
        raise IOError('File "{}" not found or it is not a file.'.format(energy_file))

    # cursor temp and end dict
    res = {
        'id':None,
        'seq':None,
        'struct':None,
        'val': None
    }
    energy_dict = {}

    # check file length
    num_struct_lines = sum(1 for x in open(structure_file))
    num_energy_lines = sum(1 for x in open(energy_file))
    if num_energy_lines > num_struct_lines: raise Exception('File size error')

    #files reader generator
    energy_lines = (x for x in open(energy_file))
    struct_lines = (x for x in open(structure_file))

    # load first line
    empty_lines = False
    try:
        energy_line = next(energy_lines).rstrip('\n')
        struct_line = next(struct_lines).rstrip('\n')
    except StopIteration:
        empty_lines = True

    # -- patterns
    # structure file            | energy file
    #----------------------------------------------------
    # >idA                      | sequenceA
    # sequenceA                 | structureA (energyA)
    # structureA                | sequenceB
    # >idB                      | structureB (energyB)
    # sequenceB                 | sequenceC
    # structureB                | structureC (energyC)
    # ....                      | ....
    #
    # -- patterns

    if not empty_lines:
        # read both files until one is done
        ended = False
        while not ended:
            try:
                #avoid blank lines
                while not energy_line:
                    energy_line = next(energy_lines).rstrip('\n')
                while not struct_line:
                    struct_line = next(struct_lines).rstrip('\n')

                if res['id'] == None and struct_line[0:1] == '>':
                    res['id'] = struct_line[1:]

                elif res['seq'] == None:
                    if struct_line != energy_line:
                        raise Exception('Sequences are not equals\nstruct: {}\nenergy: {}'.format(struct_line, energy_line))
                    res['seq'] = energy_line
                    #incr, next energy line (contains energy value at the end)
                    energy_line = next(energy_lines).rstrip('\n')

                elif res['struct'] == None:
                    tmp_e = energy_line.split(' ', maxsplit=1)
                    energy_line_without_val = tmp_e[0].strip()
                    energy_val = tmp_e[1].lstrip('(').rstrip(')').strip()
                    if struct_line != energy_line_without_val:
                        raise Exception('Structures are not equals')
                    if not energy_val:
                        raise Exception('No energy value found')
                    res['struct'] = energy_line_without_val
                    res['val'] = float(energy_val)
                    #incr, next energy line
                    energy_line = next(energy_lines).rstrip('\n')

                else:
                    raise Exception('Add energy process got a bad ending')

                # save
                if res['id'] != None and res['seq'] != None\
                    and res['struct'] != None and res['val'] != None:
                    energy_dict[res['id']] = res['val']
                    res = { 'id':None, 'seq':None, 'struct':None, 'val': None }

                #main cursor loop
                struct_line = next(struct_lines).rstrip('\n')

            except StopIteration:
                # save the last item
                if res['id'] != None and res['seq'] != None\
                    and res['struct'] != None and res['val'] != None:
                    energy_dict[res['id']] = res['val']
                    res = { 'id':None, 'seq':None, 'struct':None, 'val': None }

                # end while
                ended = True

        # close files
        struct_lines.close()
        energy_lines.close()

        # add energy to prediction/interaction objects
        try:
            for p in predictions:
                p.details.energy_duplex = float(energy_dict[p.id])

                #define position to gene start / stop for 5'UTR/3'UTR
                #DONE HERE TO FACTORIZE BETWEEN WF
                if p.region_id == Region.region_5UTR:
                    p.details.pos_to_gene_stop = 0
                    #here, if the five_primes_UTR tag was generated, pos_to_gene_start might be negative
                    #in that case, it means the mrna_begin is inside the CDS region
                    if p.mrna.strand == Strand.plus:
                        p.details.pos_to_gene_start = p.mrna.gene_start - p.details.mrna_begin -1
                    else:
                        p.details.pos_to_gene_start = p.details.mrna_begin - p.mrna.gene_start -1

                elif p.region_id == Region.region_3UTR:
                    p.details.pos_to_gene_start = 0
                    #here, if the three_primes_UTR tag was generated, pos_to_gene_stop might be negative
                    #in that case, it means the mrna_end is inside the CDS region
                    if p.mrna.strand == Strand.plus:
                        p.details.pos_to_gene_stop = p.mrna.gene_stop - p.details.mrna_end -1
                    else:
                        p.details.pos_to_gene_stop = p.details.mrna_end - p.mrna.gene_stop -1

                else:
                    #CDS region, none of offset is take into account
                    p.details.pos_to_gene_start = 0
                    p.details.pos_to_gene_stop = 0

                # defined overlapping
                specific_pos = len(p.details.mrna_seq) - p.details.mrna_seq.count("-")
                if p.details.pos_to_gene_start != "/" and p.details.pos_to_gene_start > 0 and p.details.pos_to_gene_start <= specific_pos:
                    p.details.overlap_start = True

                if p.details.pos_to_gene_stop != "/" and p.details.pos_to_gene_stop > 0 and p.details.pos_to_gene_stop <= specific_pos:
                    p.details.overlap_stop = True
        except KeyError:
            raise Exception('One interaction key has been forgotten')

        # dump each interaction into pickle gzip file
        with gzip.open(interaction_final_file, 'ab') as f:
            pickle.dump(predictions, f, protocol=Common.PICKLE_COMPRESSION_RATE)

def summarize(sum_file):
    with open(sum_file, 'w') as s:
        s.write("- Duplex energy added to each interactions.\n")

class AddEnergy (Component):
    """ Component: add the duplex energy to a predictions file."""

    def define_parameters(self, gff3_interaction_we):
        #INPUTS
        self.add_input_file("predictions_tmp", "Temporary file of Interactions without NRJ of duplex", default=gff3_interaction_we, required=True)
        #OUTPUTS
        self.add_output_file("pre_rnaeval", "List of duplex with sequence/structure to calculate NRJ through RNAeval", filename="pre_rnaeval.txt")
        self.add_output_file("duplex_energy", "Energy for each duplex", filename="duplex_energy.txt")
        self.add_output_file("predictions_complete", "File of Interactions with NRJ duplex", filename="predictions.pgz")
        self.add_output_file("summary", "Summary of all actions done in this component", filename="sum_tmp.txt")
        #ERRORS
        self.add_output_file("stderr_prernaeval", "Standard error output for sequence/structure duplex building", filename="prernaeval.stderr")
        self.add_output_file("stderr_energy", "Standard error output for add energy", filename="energy.stderr")
        self.add_output_file("stderr_rnaeval", "Standard error output for RNAeval calculation", filename="rnaeval.stderr")

    def process(self):
        """ Run the component to fold ncRNA from a fasta file and convert it into a json file """
        bprernaeval = PythonFunction(build_prernaeval, cmd_format="{EXE} {IN} {OUT} 2>> " + self.stderr_prernaeval)
        bprernaeval(inputs=[self.predictions_tmp], outputs=[self.pre_rnaeval])
        #compute energy of each duplex
        rnaeval = ShellFunction(self.get_exec_path("RNAeval") + " <$1 >$2 2>> " + self.stderr_rnaeval, cmd_format='{EXE} {IN} {OUT}')
        rnaeval(inputs=[self.pre_rnaeval], outputs=[self.duplex_energy])
        #add energy to Interaction objects
        add_NRJ = PythonFunction(add_energy, cmd_format="{EXE} {IN} {OUT} 2>> " + self.stderr_energy)
        add_NRJ(inputs=[self.predictions_tmp, self.pre_rnaeval, self.duplex_energy], outputs=[self.predictions_complete])
        #summ
        sum_function = PythonFunction(summarize, cmd_format="{EXE} {IN} {OUT}")
        sum_function( outputs=[self.summary] )

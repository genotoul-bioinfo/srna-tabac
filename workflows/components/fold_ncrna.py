#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro, Anais Painset - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2015 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.1'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import os
from jflow.component import Component
from weaver.function import ShellFunction, PythonFunction

def linearize_fasta(input, output):
    """Create a one-line sequence fasta from a multi-lines sequence fasta.

    Keywords arguments:
    input -- the multi-line sequence fasta.
    output -- the one-line sequence fasta.
    """
    with open(input, 'r') as fi:
        list_seq = []
        curr_seq = None

        for l in fi:
            line = l.strip()
            if '>' in line:
                line = line if '|' not in line else line.split('|')[0]
                if curr_seq:
                    list_seq.append(curr_seq)
                curr_seq = line + '\n' # add id of sequence
            else:
                curr_seq += line # add sequence
        if curr_seq:
            list_seq.append(curr_seq)

    with open(output, 'w') as fo:
        for seq in list_seq :
            fo.write(seq + "\n")


def strip_energy(input, json_dbn, summary):
    """Delete energy annotations from file.

    Keyword arguments:
    input -- the structure dbn file from RNAFold.
    json_dbn -- the ncrna dbn file without energy.
    summary -- the sum file.
    """
    import json
    with open(input, 'r') as inp:
        l = inp.readlines()
        list_ncrna = {}
        for lines in l:
            if '>' in lines:
                tmp = lines.split('|')
            elif lines.startswith(('G', 'A', 'U', 'C')):
                sequence = lines[:-1]
            elif lines.startswith(('.', '(', ')')):
                stru = lines.split()[0]
                list_ncrna[tmp[0][1:-1]] = {
                    "sequence": sequence,
                    "structure": stru
                }

    # dump the results into a json
    with open(json_dbn, "a") as fichier:
        json.dump(list_ncrna, fichier, separators=(',',':'))

    # summarize actions
    with open(summary, 'w') as f:
        f.write("- Fold ncRNA: {} ncRNA folded.\n".format(len(list_ncrna)))

class FoldNcrna(Component):
    """Component: fold ncRNAs to give them to RNAFold."""

    def define_parameters(self, fasta):
        # INPUTS
        self.add_input_file("fasta", "ncRNA fasta file", required=True, default=fasta, file_format="fasta")

        # OUPUTS
        self.add_output_file("fasta_oneline", "fasta file in one line", filename="oneline.fasta")
        self.add_output_file("dbn", "Dot Bracket Notation file", filename="struct.dbn")
        self.add_output_file("json_dbn", "json DBN file", filename="ncrna_dbn.json")
        self.add_output_file("summary", "Summary of all actions done in this component", filename="sum_tmp.txt")

        # ERRORS
        self.add_output_file("rnafold_stderr", "error rnafold", filename="rnafold.stderr")
        self.add_output_file("linearize_fasta_stderr", "error linearize fasta", filename="linearize_fasta.stderr")
        self.add_output_file("strip_energy_stderr", "error strip energy", filename="strip_energy.stderr")

    def process(self):
        """ Run the component to fold ncRNA from a fasta file and convert it into a json file """
        # Convert your fasta file multi line into a one line fasta
        format_input = PythonFunction(linearize_fasta, cmd_format = "{EXE} {IN} {OUT} 2>> " + self.linearize_fasta_stderr)
        format_input(inputs=[self.fasta], outputs=[self.fasta_oneline])

        # Fold your RNA
        rnafold = ShellFunction(self.get_exec_path("RNAfold") + " < $1 > $2 2>> " + self.rnafold_stderr, cmd_format='{EXE} {IN} {OUT}')
        rnafold(inputs=[self.fasta_oneline], outputs=[self.dbn])

        # Delete NRJ and convert into json
        strip_NRJ = PythonFunction(strip_energy, cmd_format="{EXE} {IN} {OUT} 2>> " + self.strip_energy_stderr)
        strip_NRJ(inputs=[self.dbn], outputs=[self.json_dbn, self.summary])

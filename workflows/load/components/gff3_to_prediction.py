#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro, Anais Painset - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2015 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.1'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import os
from jflow.component import Component
from weaver.function import PythonFunction, ShellFunction


def GFF2inter(gff3_file, reference_genome, fasta_ncRNA, predictions_tmp, ncrna_mrna, softwares, summary):
    """ Convert a gff3 file into interactions/predictions objects.

    Keyword arguments:
    gff3_file -- the gff3 file ready to be parsed
    reference_genome -- the reference genome
    fasta_ncRNA -- the future folded ncrna file
    predictions_tmp -- the tmp predictions file (without duplex energy)
    pre_rnaeval -- the rnaeval pre-file
    ncrna_mrna -- the mrna and ncrna list file
    softwares -- all softwares found in the gff3 file
    summary -- the sum file
    """
    import pickle
    import gzip
    from jflow.featureio import GFF3IO
    from jflow.seqio import FastaReader

    from srnatabac.utils.common import Common

    from srnatabac.core.strand import Strand
    from srnatabac.core.rna_feature import ncRNA, mRNA
    from srnatabac.core.region import Region
    from srnatabac.core.interaction import Interaction, InteractionDetail
    from srnatabac.utils.bio_helper import BioHelper
    from srnatabac.utils.gff3_converter import GFF3Converter

    ### MAIN ###
    gff3_reader = GFF3IO(gff3_file, "r")
    fasta_reader = FastaReader(reference_genome)

    for seq in fasta_reader:
        genome_ref = seq.sequence

    # softwares
    softs = []
    # interactions
    ncrna_entries = {}
    gene_entries = {}
    interactions = []
    # used in actors and targets json file
    list_actors = []
    list_targets = []
    # read gff3 interactions file
    for entry in gff3_reader:
        # entry 'ncRNA'
        if entry.type == "ncRNA":
            list_actors.append(entry.seq_id)

            # if not present in ncrna entries, add it
            if entry.seq_id not in list(ncrna_entries.keys()):
                nc = GFF3Converter.to_ncrna(entry)
                nc.sequence = BioHelper.extract_from(genome_ref, int(entry.start),
                                                     int(entry.end), strand=Strand.get_string_value(nc.strand),
                                                     circular=(entry.end > len(genome_ref))
                                                     )
                ncrna_entries[entry.seq_id] = nc

        # entry 'gene' = mRNA
        elif entry.type == "gene":
            list_targets.append(entry.seq_id)
            # if not present in mrna entries, add it
            if entry.seq_id not in list(gene_entries.keys()):
                mr = GFF3Converter.to_mrna(entry)
                gene_entries[entry.seq_id] = mr

        # entry 'region' is 5UTR/CDS/3UTR
        elif entry.type in Region.get_all_gff3_values():
            # create an interaction from the gff3 record
            ir = GFF3Converter.to_interaction(entry)
            # get information about gene_entries (get start/stop missing values)
            ir.mrna.gene_start = gene_entries[ir.mrna_id].start
            ir.mrna.gene_stop = gene_entries[ir.mrna_id].stop
            # save softwares
            if not Common.is_software_authorized(ir.details.software.lower()):
                raise Exception('Software not allowed: {}'.format(ir.details.software))
            if ir.details.software.lower() not in softs:
                softs.append(ir.details.software.lower())
            # get ncRNA object
            ir.ncrna = ncrna_entries[ir.ncrna_id]
            t = entry.attributes['Target'].split()
            if ir.ncrna.strand == Strand.plus:
                ir.details.ncrna_begin = ir.ncrna.start + int(t[1])
                ir.details.ncrna_end = ir.ncrna.start + int(t[2])
            else:
                ir.details.ncrna_begin = ir.ncrna.stop - int(t[2])
                ir.details.ncrna_end = ir.ncrna.stop - int(t[1])
            # mrna seq
            temp_extract = BioHelper.extract_from(genome_ref, ir.details.mrna_begin, ir.details.mrna_end, strand=Strand.get_string_value(
                ir.mrna.strand), circular=(ir.details.mrna_end > len(genome_ref)), to_uracil=True)
            j = 0
            tmp_mrna_seq = list(ir.details.mrna_seq)
            for i in range(len(tmp_mrna_seq)):
                if tmp_mrna_seq[i] == "N":
                    tmp_mrna_seq[i] = temp_extract[j]
                    j += 1
            ir.details.mrna_seq = ''.join(tmp_mrna_seq)
            # ncrna seq
            temp_extract = BioHelper.extract_from(genome_ref, ir.details.ncrna_begin, ir.details.ncrna_end, strand=Strand.get_string_value(
                ir.ncrna.strand), circular=(ir.details.ncrna_end > len(genome_ref)), to_uracil=True)[::-1]
            #apply transformation based on combinaison mrna/ncrna:
            j = 0
            tmp_ncrna_seq = list(ir.details.ncrna_seq)
            for i in range(len(tmp_ncrna_seq)):
                if tmp_ncrna_seq[i] == "N":
                    tmp_ncrna_seq[i] = temp_extract[j]
                    j += 1
            ir.details.ncrna_seq = ''.join(tmp_ncrna_seq)
            # compute pairing frequencies
            tmp_symbolism = list(ir.details.symbolism)
            for i in range(len(ir.details.symbolism)):
                if ir.details.symbolism[i] != ' ':
                    if (ir.details.mrna_seq[i] == 'G' and ir.details.ncrna_seq[i] == 'U') or (ir.details.mrna_seq[i] == 'U' and ir.details.ncrna_seq[i] == 'G'):
                        tmp_symbolism[i] = '.'
                        ir.details.pair_pct_GU += 1.0 / ir.details.match_count
                    elif (ir.details.mrna_seq[i] == 'G' and ir.details.ncrna_seq[i] == 'C') or (ir.details.mrna_seq[i] == 'C' and ir.details.ncrna_seq[i] == 'G'):
                        ir.details.pair_pct_GC += 1.0 / ir.details.match_count
                    else:
                        ir.details.pair_pct_AU += 1.0 / ir.details.match_count
            ir.details.symbolism = ''.join(tmp_symbolism)
            # add object to interaction list
            interactions.append(ir)

        # forbid this
        else:
            raise TypeError('Unknwown type: ' + entry.type)

    # create a pickle with list of actors and targets ids
    with gzip.open(ncrna_mrna, 'wb') as f:
        pickle.dump({"actors": list_actors, "targets": list_targets},
                    f, protocol=Common.PICKLE_COMPRESSION_RATE)

    # create a pickle with list of softwres
    with gzip.open(softwares, 'wb') as f:
        pickle.dump({"softwares": softs}, f, protocol=Common.PICKLE_COMPRESSION_RATE)

    # create fasta file of ncRNA to be fold
    with open(fasta_ncRNA, "w") as fichier:
        for nc in ncrna_entries:
            fichier.write(">" + ncrna_entries[nc].id + "\n" + ncrna_entries[nc].sequence + "\n")

    # for each interaction DUMP it into pickle gzip file
    with gzip.open(predictions_tmp, 'ab') as f:
        pickle.dump(interactions, f, protocol=Common.PICKLE_COMPRESSION_RATE)

    # summarize actions
    with open(summary, 'w') as f:
        f.write("- GFF3 file parsing:\n")
        f.write("-   {} ncRNA found. \n".format(len(ncrna_entries)))
        f.write("-   {} mRNA found.\n".format(len(gene_entries)))
        f.write("-   {} interactions found.\n".format(len(interactions)))


class GFF3ToPrediction (Component):
    """ Component: create a interactions file from a gff3 file """

    def define_parameters(self, gff3_predictions_file, reference_genome):
        # INPUTS
        self.add_input_file("reference_genome", "FASTA file",
                            default=reference_genome, required=True)
        self.add_input_file("gff3_predictions_file", "GFF3 file list",
                            default=gff3_predictions_file, required=True)
        # OUTPUTS
        self.add_output_file("fasta_ncRNA", "Output FASTA file of ncRNA",
                             filename="ncRNA_sequences.fa")
        self.add_output_file(
            "predictions_tmp", "Temporary file of Interactions without NRJ of duplex", filename="predictions_tmp.pgz")
        self.add_output_file("ncrna_mrna_pickle",
                             "List of regulators and targets", filename="ncrna_mrna.pgz")
        self.add_output_file("softwares_pickle", "List of softwares used", filename="softwares.pgz")
        self.add_output_file(
            "summary", "Summary of all actions done in this component", filename="sum_tmp.txt")
        # ERRORS
        self.add_output_file(
            "stderr_convert", "Standard error output for convert GFF3 to Interaction", filename="convert.stderr")

    def process(self):
        # convert GFF3 to Interaction object and generate fasta ncRNA to fold,
        # list of actors/targets predictions index and predictions without energy
        GFF3_to_inter = PythonFunction(
            GFF2inter, cmd_format="{EXE} {IN} {ARG} {OUT} 2>> " + self.stderr_convert)
        GFF3_to_inter(inputs=[self.gff3_predictions_file, self.reference_genome],
                      outputs=[self.fasta_ncRNA, self.predictions_tmp, self.ncrna_mrna_pickle, self.softwares_pickle, self.summary])

#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro, Anais Painset - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2015 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.1'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import os
import logging
from jflow.workflow import Workflow
from srnatabac.core.user_information import UserInformation, UserProject
from srnatabac.dao.user_handler import UserHandler

class Load(Workflow):
    """ Load module: pretty-print a 'GFF3 interaction' file on dynamic web page """

    def get_description(self):
        """ Description for the load module """
        return """Extract interactions from a GFF3 file and load them
            into a specific visualization interface. Allows you to gain some
            time by giving produced prediction projects GFF3 files. """

    def define_parameters(self, function="process"):

        # INPUTS
        self.add_input_file("gff3", "[GFF3] Features file", display_name="Features file",
            file_format="gff3", required=True, group="Data files", type="browsefile")
        self.add_input_file("reference_genome", "[FASTA] Genome file reference", display_name="Reference Genome",
            file_format="fasta", required=True, group="Data files", type="browsefile")

        # PARAMETERS
        self.add_parameter("project_name", "Give a name to your project",\
            display_name="Project name", type="str", required=True, group="Project informations")
        self.add_parameter("align_output", "Do you want an alignment-like output?",\
            display_name="Alignment output", type="bool", default=False, group="Project informations")
        self.add_parameter("user_email", "Your email address, you will receive the direct link to your workspace.",\
            display_name="Email", type="email", required=True, group="Project informations")

    def pre_process(self):
        """ Before processing: register the user """
        user = UserHandler.register_user(self, self.user_email, self.project_name)

    def process(self):
        """ Run the workflow """
        # project name
        project_name = self.project_name if self.project_name != None else os.path.splitext(self.reference_genome)[0].split('/')[-1]

        # list of summary files
        summary = []

        # Convert GFF3 file into Interaction object and add duplex energy to each interaction
        convert = self.add_component("GFF3ToPrediction", [self.gff3, self.reference_genome])
        summary.append(convert.summary)

        # add duplex energy to all interactions
        add_energy = self.add_component("AddEnergy", [convert.predictions_tmp])
        summary.append(add_energy.summary)

        # Folding ncRNA -> VARNA applet
        fold_ncRNA = self.add_component("FoldNcrna", [convert.fasta_ncRNA])
        summary.append(fold_ncRNA.summary)

        # Create outputs. The GFF3 file were given in input but create it even though.
        # It will be in the outputs files and would be downloaded.
        format_output = self.add_component("FormatOutput", [add_energy.predictions_complete, self.align_output, True, False])
        summary.append(format_output.summary)

        # summarize all action
        sum_up = self.add_component("Summarize", [project_name, summary])

        # email
        user_info = UserHandler().get_user_by_email(self.user_email)
        if user_info is None: raise Exception('User not found')

        project_info = user_info.get_project_by_workflow_id(self.id)
        socket_opt = self.jflow_config_reader.get_socket_options()
        status_placeholder = '###get_status()###'
        user_url = "http://{}:{}/projects?user={}".format(socket_opt[0], socket_opt[1], user_info.get_user_key().get_key())
        user_msg = "Dear sRNA-Tabac user,<br/><br/>"
        user_msg += "Your project {} (#{}) is {}. It is available for 7 days.<br/>".format(project_name, project_info.get_workflow_id(), status_placeholder)
        if user_info.has_only_one_project(): # warn the user
            user_msg += "Since you only have one project available on your workspace, <u>your workspace will also be deleted in 7 days</u>.<br/>"
            user_msg += "Consider downloading generated files.<br/>"
        user_msg += "You can access to <a href='{}'>your workspace here</a> to see the details.<br/><br/>".format(user_url)
        user_msg += "The sRNA-Tabac team"
        self.set_to_address(self.user_email)
        self.set_subject("[sRNA-Tabac] your project {} (#{}) is {}".format(project_name, project_info.get_workflow_id(), status_placeholder))
        self.set_message(user_msg)

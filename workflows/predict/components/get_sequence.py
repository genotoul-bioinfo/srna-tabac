#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro, Anais Painset - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2015 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.1'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import os
from jflow.component import Component
from weaver.abstraction import Map
from weaver.function import PythonFunction
from collections import namedtuple


def validate_gff3(unchecked_gff3):
    """ Validate the initial GFF3

    Keyword arguments:
    unchecked_gff3 -- The GFF3 initial file
    """
    from jflow.featureio import GFF3Record, GFF3IO
    from srnatabac.utils.gff3_feature_manager import GFF3FeatureManager, GFF3Reader
    from srnatabac.core.exceptions.gff3_exceptions import GFF3WrongFormatError, GFF3PositionError

    #------------------------------------
    # get directives and records
    directives = []
    features = []
    valid_types = ['ncRNA', 'mRNA', 'CDS', 'five_prime_UTR', 'three_prime_UTR', 'gene']
    gff3_reader = GFF3Reader(unchecked_gff3, valid_types)
    for (rec_type, rec_content) in gff3_reader:
        if rec_type == 'feature':
            features.append(rec_content)
        elif rec_type == 'directive':
            directives.append(rec_content)
    gff3_reader.close()

    #------------------------------------
    # check directives
    sequence_region = None
    for d in directives:
        if d.startswith('##gff-version'):
            vtmp = d.split()[1]
            vtmp = int(vtmp.split('.')[0] if '.' in d.split()[1] else vtmp)
            if vtmp != 3:
                raise GFF3WrongFormatError('GFF version expected 3, found {}'.format(vtmp))
        elif d.startswith('##FASTA'):
            raise GFF3WrongFormatError('Remove directive "##FASTA" and FASTA sequence from file')
        elif d.startswith('##sequence-region'):
            rtmp = d.split()
            sequence_region = {'id': rtmp[1], 'start': int(rtmp[2]), 'end': int(rtmp[3])}
            try:
                if rtmp[4] == 'circular':
                    sequence_region['is_circular'] = True
            except IndexError:
                sequence_region['is_circular'] = False

    #------------------------------------
    # check features
    feature_manager = GFF3FeatureManager(features)
    if not feature_manager.get_records_of_type('ncRNA'):
        raise GFF3WrongFormatError('No ncRNA-type records found')
    if not feature_manager.get_records_of_type('gene'):
        raise GFF3WrongFormatError('No gene-type records found')
    if not feature_manager.get_records_of_type('CDS'):
        raise GFF3WrongFormatError('No CDS-type records found')

    # feature details
    ids_list = []
    for r in feature_manager.get_records():
        # ID attribute
        try:
            rid = r.attributes['ID']
        except KeyError:
            raise GFF3WrongFormatError('All records must have "ID" attributes')
        # forbid duplicate ID attribute
        if rid in ids_list:
            raise GFF3WrongFormatError(
                'Each ID must be unique, {} ID="{}" found multiple times'.format(r.type, rid))
        ids_list.append(rid)
        # check start/end
        if r.start < sequence_region['start']:
            raise GFF3PositionError(
                '{} ID="{}" start before global region start'.format(r.type, rid))
        if r.end > sequence_region['end'] and not sequence_region['is_circular']:
            raise GFF3PositionError(
                '{} ID="{}" end after the non circular global region stop'.format(r.type, rid))

        # phase
        if r.type == 'CDS' and r.phase not in ['0', '1', '2']:
            raise GFF3WrongFormatError('CDS-type records must have phase in [0, 1, 2]')
        elif r.type != 'CDS' and r.phase != '.':
            raise GFF3WrongFormatError('{}-type records must have phase = "."'.format(r.type))

        if r.type == 'gene':
            # attribute
            try:
                r.attributes['locus_tag']
            except KeyError:
                raise GFF3WrongFormatError('Gene-type records must have "locus_tag" attributes')
            try:
                r.attributes['Parent']
                raise GFF3WrongFormatError('Gene-type records must not have "Parent" attributes')
            except KeyError:
                pass
            # check children
            children = feature_manager.get_children(r)
            for c in children:
                try:
                    if c.type != 'mRNA' and c.type != 'CDS':
                        raise GFF3WrongFormatError(
                            'Gene ID="{}": child ID="{}" is not a mRNA/CDS'.format(rid, c.attributes['ID']))
                    if c.start < r.start:
                        raise GFF3PositionError(
                            'Gene ID="{}": mRNA ID="{}" starts before gene start'.format(rid, c.attributes['ID']))
                    if c.end > r.end:
                        raise GFF3PositionError(
                            'Gene ID="{}": mRNA ID="{}" ends after gene end'.format(rid, c.attributes['ID']))
                except KeyError:
                    raise GFF3WrongFormatError(
                        'Gene ID="{}": child has no ID attribute'.format(rid))

        elif r.type in ['mRNA', 'CDS', 'five_prime_UTR', 'three_prime_UTR']:

            # attribute
            try:
                r.attributes['locus_tag']
                raise GFF3WrongFormatError('mRNA-type records must not have "locus_tag" attributes')
            except KeyError:
                pass

            try:
                r.attributes['Parent']
            except KeyError:
                raise GFF3WrongFormatError('mRNA-type records must have "Parent" attributes')

            # parents
            parents = feature_manager.get_parents(r)
            if not parents:
                raise GFF3WrongFormatError('{} ID="{}" has no parents'.format(r.type, rid))

            for p in parents:
                try:
                    if r.type == 'mRNA' and p.type != 'gene':
                        raise GFF3WrongFormatError(
                            '{} ID="{}": parent is not a "gene"'.format(r.type, rid))
                    elif r.type in ['CDS', 'five_prime_UTR', 'three_prime_UTR'] and p.type != 'mRNA' and p.type != 'gene':
                        raise GFF3WrongFormatError(
                            '{} ID="{}": parent is not a "mRNA/gene"'.format(r.type, rid))

                    if r.start < p.start:
                        raise GFF3PositionError(
                            '{} ID="{}": starts before its parent start'.format(r.type, rid))
                    if r.end > p.end:
                        raise GFF3PositionError(
                            '{} ID="{}": ends after its parent end'.format(r.type, rid))

                except KeyError:
                    raise GFF3WrongFormatError(
                        '{} ID="{}": parent has no ID attribute'.format(r.type, rid))

            # check children
            if r.type == 'mRNA':
                children = feature_manager.get_children(r)
                if not children:
                    raise GFF3WrongFormatError('mRNA ID="{}" has no children'.format(rid))

                for c in children:
                    try:
                        if c.type != 'mRNA':
                            raise GFF3WrongFormatError(
                                'mRNA ID="{}": child ID="{}" is not a mRNA'.format(rid, c.attributes['ID']))

                        if c.start < r.start:
                            raise GFF3PositionError(
                                'mRNA ID="{}": starts before its parent start'.format(rid))
                        if c.end > r.end:
                            raise GFF3PositionError(
                                'mRNA ID="{}": ends after its parent end'.format(rid))
                    except KeyError:
                        raise GFF3WrongFormatError(
                            'mRNA ID="{}": child has no ID attribute'.format(rid))


def extract_ncrna(reference_genome, gff3, summary, fasta_output):
    """ Extract ncRNAs from GFF3 file to a FASTA file

    Keyword arguments:
    summary -- the summary file
    reference_genome -- The reference genome in FASTA format
    gff3 -- The GFF3 predictions file
    fasta_output -- The FASTA sequences file
    """
    from jflow.featureio import GFF3IO
    from jflow.seqio import FastaReader
    from srnatabac.utils.bio_helper import BioHelper

    # get all ncrna in the file
    gff3_reader = GFF3IO(gff3, "r")
    valid_gff3_records = [gff3_rec for gff3_rec in gff3_reader if gff3_rec.type == 'ncRNA']
    gff3_reader.close()

    if valid_gff3_records == None:
        raise Exception('No ncRNA found in the GFF3 file!')

    # get the genome sequence
    GENOME_SEQ = None
    turn = 0
    for seq in FastaReader(reference_genome, wholefile=True):
        # there should be only one sequence in the reference genome file
        GENOME_SEQ = seq.sequence
        if turn > 0:
            raise Exception(
                "Not a genome reference file! The file must have only one sequence.")
        turn += 1
    if GENOME_SEQ == None:
        raise Exception('Error on genome reference: no sequence')

    # sequence extracted
    i = 0
    with open(fasta_output, "w") as output:
        # list of ids already selected
        ids_list = []
        for r in valid_gff3_records:
            # get the sequence
            if r.start == r.end:
                raise Exception('Bad definition of ncRNA in GFF3: start == end')
            # forbid duplication
            if r.attributes['ID'] in ids_list:
                raise Exception('Bad definition of ncRNA in GFF3: duplicate ID')
            # write output
            output.write(">{}|{}-{}|{}|ncRNA\n{}\n".format(r.attributes['ID'],
                                                           r.start, r.end, r.strand,
                                                           BioHelper.extract_from(GENOME_SEQ, r.start, r.end, r.strand, circular=(r.end > len(GENOME_SEQ)))))
            # duplication list
            ids_list.append(r.attributes['ID'])
            # information counter
            i += 1

    # create the brief summary file
    with open(summary, 'w') as s:
        s.write("- {} ncRNAs extracted \n".format(i))
        s.write("-   From: {}\n".format(gff3))


def extract_mrna(reference_genome, gff3, get_five, get_cds, get_three, before_5utr, after_5utr, before_3utr, after_3utr, summary, fasta_output):
    """ Extract the mRNAs from GFF3 file to FASTA file

    Keyword arguments:
    summary -- the summary file
    reference_genome -- The reference genome in FASTA format
    get_five -- does the user want the 5'UTR region?
    get_cds -- does the user want the CDS region?
    get_three -- does the user want the 3'UTR region?
    before_5utr -- parameter, if no five_prime_UTR found in the GFF3 file, defines the #bp considered before CDS start
    after_5utr -- parameter, defines the #bp considered after CDS start
    before_3utr -- parameter, defines the #bp considered before CDS stop
    after_3utr -- parameter, if no three_prime_UTR found in the GFF3 file, defines the #bp considered after CDS end
    gff3 -- The GFF3 predictions file
    fasta_output -- The FASTA sequences file
    """
    from jflow.featureio import GFF3IO
    from jflow.seqio import FastaReader
    from srnatabac.utils.bio_helper import BioHelper
    from srnatabac.utils.gff3_feature_manager import GFF3FeatureManager

    # help variables, valid types
    _FIVE = 'five_prime_UTR'
    _THREE = 'three_prime_UTR'
    _CDS = 'CDS'
    _GENE = 'gene'

    MAPPER = {
        _FIVE: '5UTR',
        _CDS: _CDS,
        _THREE: '3UTR'
    }

    # get all ncrna in the file
    gff3_reader = GFF3IO(gff3, "r")
    valid_mrna_types = [_GENE, _CDS, _FIVE, _THREE]
    valid_gff3_records = [gff3_rec for gff3_rec in gff3_reader if gff3_rec.type in valid_mrna_types]
    gff3_reader.close()

    if valid_gff3_records == None:
        raise Exception('No ncRNA found in the GFF3 file!')

    # get the genome sequence
    GENOME_SEQ = None
    turn = 0
    for seq in FastaReader(reference_genome, wholefile=True):
        # there should be only one sequence in the reference genome file
        GENOME_SEQ = seq.sequence
        if turn > 0:
            raise Exception(
                "Not a genome reference file! The file must have only one sequence.")
        turn += 1
    if GENOME_SEQ == None:
        raise Exception('Error on genome reference: no sequence')

    def _simple_extract(records, records_type=None):
        """ Extract a 5'UTR/CDS/3'UTR from gff3 records.
        /!\
        Only do this when tags 'five_prime_UTR' and/or 'three_prime_UTR' have been found in the GFF3
        /!\

        records -- pre-filtered gff3 records
        records_type -- the type we want to extract, can only be 'five_prime_UTR', 'three_prime_UTR' or 'CDS'
        b -- see 'before_5utr'/'before_3utr'
        a -- see 'after_5utr'/'after_3utr'
        """
        if records_type != _FIVE and records_type != _CDS and records_type != _THREE:
            raise TypeError
        mapper = {
            _FIVE: '5UTR',
            _CDS: _CDS,
            _THREE: '3UTR'
        }
        ids_list = []
        feature_manager = GFF3FeatureManager(records)
        for r in feature_manager.get_records():
            if r.type != records_type:
                continue
            # mrna precise identifier
            mrna_id = "{}#{}_{}".format(feature_manager.get_locus_name(
                r), r.attributes['Parent'], MAPPER[records_type])
            # forbid duplication
            if mrna_id in ids_list:
                continue
            # return
            # seq = GENOME_SEQ[r.start:r.end]
            yield ">{}|{}-{}|{}|{}-{}|mRNA\n{}\n".format(
                mrna_id, r.start, r.end, r.strand, r.start, r.end,
                BioHelper.extract_from(GENOME_SEQ, r.start, r.end, r.strand,
                                       circular=(r.end > len(GENOME_SEQ)))
            )

            # duplication list
            ids_list.append(mrna_id)

    def _simulate_five_prime_regions(records, b5, a5):
        """ Extract 5'UTR region based on GFF3 CDS record start/stop
        /!\
        Only do this when tags 'five_prime_UTR' have not been found in the GFF3
        /!\

        cds_record -- a GFF3Record of type CDS
        b5 -- see 'before_5utr'
        a5 -- see 'after_5utr'
        """
        ids_list = []
        feature_manager = GFF3FeatureManager(records)
        for cds_record in feature_manager.get_records():
            # only allow CDS to extract virtual 5'UTR
            if cds_record.type != _CDS:
                continue
            utr5_id = "{}#{}_{}".format(feature_manager.get_locus_name(
                cds_record), cds_record.attributes['Parent'], MAPPER[_FIVE])
            # forbid duplication
            if utr5_id in ids_list:
                continue
            # get fasta param
            if cds_record.strand == '+':
                extracted_start = cds_record.start - b5
                extracted_stop = cds_record.start + a5
            elif cds_record.strand == '-':
                extracted_stop = cds_record.end + b5
                extracted_start = cds_record.end - a5
            else:
                raise Exception
            # write output
            # seq = GENOME_SEQ[extracted_start:extracted_stop]
            yield ">{}|{}-{}|{}|{}-{}|mRNA\n{}\n".format(
                utr5_id, extracted_start, extracted_stop, cds_record.strand, cds_record.start, cds_record.end,
                BioHelper.extract_from(GENOME_SEQ, extracted_start, extracted_stop,
                                       cds_record.strand, circular=(extracted_stop > len(GENOME_SEQ)))
            )
            # forbid duplication
            ids_list.append(utr5_id)

    def _simulate_three_prime_regions(records, b3, a3):
        """ Extract 3'UTR region based on GFF3 CDS record start/stop
        /!\
        Only do this when tags 'three_prime_UTR' have not been found in the GFF3
        /!\

        cds_record -- a GFF3Record of type CDS
        b3 -- see 'before_3utr'
        a3 -- see 'after_3utr'
        """
        ids_list = []
        feature_manager = GFF3FeatureManager(records)
        for cds_record in feature_manager.get_records():
            # only allow CDS to extract virtual 5'UTR
            if cds_record.type != _CDS:
                continue
            utr3_id = "{}#{}_{}".format(feature_manager.get_locus_name(
                cds_record), cds_record.attributes['Parent'], MAPPER[_THREE])
            # forbid duplication
            if utr3_id in ids_list:
                continue
            # get fasta param
            if cds_record.strand == '+':
                extracted_start = cds_record.end - b3
                extracted_stop = cds_record.end + a3
            elif cds_record.strand == '-':
                extracted_stop = cds_record.start + b3
                extracted_start = cds_record.start - a3
            else:
                raise Exception
            if extracted_start > extracted_stop:
                raise Exception
            # write output
            seq = GENOME_SEQ[extracted_start:extracted_stop]
            yield ">{}|{}-{}|{}|{}-{}|mRNA\n{}\n".format(
                utr3_id, extracted_start, extracted_stop, cds_record.strand, cds_record.start, cds_record.end,
                BioHelper.extract_from(GENOME_SEQ, extracted_start, extracted_stop,
                                       cds_record.strand, circular=(extracted_stop > len(GENOME_SEQ)))
            )
            # forbid duplication
            ids_list.append(utr3_id)

    def _five_prime_extractor(records, b5, a5, tag_found=False):
        """ Extract 5'UTR from gff3 records

        records -- pre-filtered gff3 records
        b5 -- see 'before_5utr'
        a5 -- see 'after_5utr'
        tag_found -- 'five_prime_UTR' tag found?
        """
        return _simple_extract(records, records_type=_FIVE) if tag_found else _simulate_five_prime_regions(records, b5, a5)

    def _three_prime_extractor(records, b3, a3, tag_found=False):
        """ Extract 3'UTR from gff3 records

        records -- pre-filtered gff3 records
        b3 -- see 'before_3utr'
        a3 -- see 'after_3utr'
        tag_found -- 'three_prime_UTR' tag found?
        """
        return _simple_extract(records, records_type=_THREE) if tag_found else _simulate_three_prime_regions(records, b3, a3)

    def _cds_extractor(records):
        """ Extract CDS from gff3 records

        records -- pre-filtered gff3 records
        """
        return _simple_extract(records, records_type=_CDS)

    #-------------
    #--START process
    #-------------

    # do 5'/3' tags found in gff3 file
    tags_found = {
        _FIVE: False,
        _THREE: False
    }
    for r in valid_gff3_records:
        if r.type == _FIVE:
            tags_found[_FIVE] = True
        elif r.type == _THREE:
            tags_found[_THREE] = True

    #-------------
    #--execute user choices
    #-------------

    # sequence counter
    cnt_five = 0
    cnt_cds = 0
    cnt_three = 0
    with open(fasta_output, "w") as output:

        # extract 5'UTR region from gff3
        if get_five == 'True':
            # automatic choice
            for fasta_str in _five_prime_extractor(valid_gff3_records, int(before_5utr), int(after_5utr), tags_found[_FIVE]):
                output.write(fasta_str)
                cnt_five += 1

        # extract CDS region from gff3
        if get_cds == 'True':
            for fasta_str in _cds_extractor(valid_gff3_records):
                output.write(fasta_str)
                cnt_cds += 1

        # extract 3'UTR region from gff3
        if get_three == 'True':
            # automatic choice
            for fasta_str in _three_prime_extractor(valid_gff3_records, int(before_3utr), int(after_3utr), tags_found[_THREE]):
                output.write(fasta_str)
                cnt_three += 1

    #-------------
    #--summarize
    #-------------
    with open(summary, 'w') as s:
        tmp = "- {} records extracted (5'UTR: {}, CDS: {}, 3'UTR: {})\n".format(
            cnt_five + cnt_cds + cnt_three, cnt_five, cnt_cds, cnt_three)
        tmp += "-  From GFF3: {}\n".format(gff3)
        tmp += "-  reference genome: {}\n".format(reference_genome)
        s.write(tmp)

        # user wanted 5'UTR AND/OR 3'UTR...
        if get_five or get_three:
            #...but didn't write tags in GFF3
            if not tags_found[_FIVE] and not tags_found[_THREE]:
                tmp = "-  GFF3 file does not contain explicit 'five_prime_UTR' or 'three_prime_UTR' tags\n"
                tmp += "-    but regions were calculated with CDS start/stop and parameters you gave in the form\n"
                tmp += "-  These regions should be declared with 'five_prime_UTR' or 'three_prime_UTR' tags\n"
                tmp += "-  See: http://www.sequenceontology.org/gff3.shtml >> The Canonical Gene >> NOTE 1\n"
                s.write(tmp)

            #...and gave well-formatted file
            else:
                tags = ''
                if get_five and tags_found[_FIVE]:
                    tags += "'{}' ".format(_FIVE)
                if get_three and tags_found[_THREE]:
                    tags += "and '{}' ".format(_THREE)
                if not ftmp and not ttmp:
                    raise Exception
                tmp = "-  GFF3 file does contain explicit {}tags\n".format(tags)
                tmp += "-    Thus, UTR regions have been calculated based on start/end of GFF3 records\n"
                s.write(tmp)


class GetSequence (Component):
    """ Extract sequence from GFF3 files (based on a reference genome) to a FASTA file. """

    def define_parameters(self, reference_genome_file, gff3_sequences_file, g5, gcds, g3, b5, a5, b3, a3):
        """ Get DNA sequences from a GFF3 and output a FASTA files

        Keyword arguments:
        reference_genome_file -- The reference genome file
        gff3_sequences_file -- the list of sequence in a GFF3 format file
        g5 -- parameter, does mr. user want 5'UTR region?
        gcds -- parameter, does mr. user want CDS region?
        g3 -- parameter, does mr. user want 3'UTR region?
        b5 -- parameter, if no five_prime_UTR found in the GFF3 file, defines the #bp considered before CDS start
        a5 -- parameter, defines the #bp considered after CDS start
        b3 -- parameter, defines the #bp considered before CDS stop
        a3 -- parameter, if no three_prime_UTR found in the GFF3 file, defines the #bp considered after CDS stop
        """
        # PARAMETERS
        self.add_parameter(
            "get_5utr", "Tell if the user wants 5'UTR region in results", default=g5, required=True)
        self.add_parameter(
            "get_cds", "Tell if the user wants CDS region in results", default=gcds, required=True)
        self.add_parameter(
            "get_3utr", "Tell if the user wants 3'UTR region in results", default=g3, required=True)
        self.add_parameter(
            "before_5utr", "If no 'five_prime_UTR' tag found in the GFF3 file, defines the #bp considered before CDS start", default=b5, required=True)
        self.add_parameter(
            "after_5utr", "Defines the #bp considered after CDS start", default=a5, required=True)
        self.add_parameter(
            "before_3utr", "Defines the #bp considered before CDS stop", default=b3, required=True)
        self.add_parameter(
            "after_3utr", "if no 'three_prime_UTR' tag found in the GFF3 file, defines the #bp considered after CDS end", default=a3, required=True)
        # INPUTS
        self.add_input_file("reference_genome_file", "FASTA file",
                            default=reference_genome_file, required=True)
        self.add_input_file("gff3_sequences_file", "GFF3 file list",
                            default=gff3_sequences_file, required=True)
        # OUTPUTS
        self.add_output_file(
            "summary", "Summary of all actions done in this component", filename="sum_tmp.txt")
        self.add_output_file("fasta_file", "Ouptut FASTA file", filename='sequences.fa')
        self.add_output_file("valid_gff3", "Valid GFF3", filename='validate.gff3')
        # ERRORS
        self.add_output_file("stderr_extract",
                             "Standard error for extraction errors", filename="extract.stderr")
        self.add_output_file(
            "stderr_validate", "Standard error for validation errors", filename="validate.stderr")

    def process(self):
        if self.prefix != 'mrna' and self.prefix != 'ncrna':
            raise Exception("Component prefix error, must be 'mrna' or 'ncrna'")

        # validate GFF3
        validate = PythonFunction(
            validate_gff3, cmd_format="{EXE} {IN} {ARG} {OUT} 2>> " + self.stderr_validate)
        validate(inputs=[self.gff3_sequences_file])

        # process ncRNA or mRNA extraction
        if self.prefix == 'ncrna':
            extract_nc = PythonFunction(
                extract_ncrna,
                cmd_format="{EXE} {IN} {ARG} {OUT} 2>> " + self.stderr_extract
            )
            extract_nc(
                inputs=[self.reference_genome_file, self.gff3_sequences_file],
                outputs=[self.summary, self.fasta_file]
            )
        else:  # mrna
            extract_m = PythonFunction(
                extract_mrna,
                cmd_format="{EXE} {IN} {ARG} {OUT} 2>> " + self.stderr_extract
            )
            extract_m(
                inputs=[self.reference_genome_file, self.gff3_sequences_file],
                arguments=[self.get_5utr, self.get_cds, self.get_3utr,
                           self.before_5utr, self.after_5utr, self.before_3utr, self.after_3utr],
                outputs=[self.summary, self.fasta_file]
            )

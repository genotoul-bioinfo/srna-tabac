#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro, Anais Painset - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2015 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.1'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import os
from subprocess import Popen, PIPE

from jflow.component import Component
from jflow.abstraction import MultiMap

from weaver.function import ShellFunction, PythonFunction
from weaver.abstraction import Map

from srnatabac.utils.common import Common

def hybridation_matrix(mfile):
    """
    @summary: Writes the hybridation matrix file
    @return: [str] the file name
    """
    mat = open(mfile,'w')
    mat.write("# RNA matrix favorising hybridation\n \
   A  C  G  T  U  R  Y  M  W  S  K  D  H  V  B  N  X\n \
A -1 -1 -1  2  2 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
C -1 -1  2 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
G -1  2 -1  0  0 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
T  2 -1  0 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
U  2 -1  0 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
R -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
Y -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
M -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
W -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
S -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
K -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
D -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
H -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
V -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
B -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
N -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
X -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1")
    mat.close()
    return mat.name

def reverse(input, output):
    import jflow.seqio as seqio
    from srnatabac.utils.bio_helper import BioHelper
    # from srnatabac.utils.bio_lambda import BioLambda
    # rev = BioLambda.reverse_complement()
    reader = seqio.FastaReader(input)
    with open(output, 'w') as out:
        for id, desc, seq, qual in reader:
            # seqio.writefasta(out, [[id, desc, rev(seq), qual]])
            seqio.writefasta(out, [[id, desc, BioHelper.reverse_complement(seq), qual]])

def parse_ssearch(input, output, out_rnaeval):
    import pickle
    import gzip
    from srnatabac.utils.common import Common
    from srnatabac.core.interaction import Interaction, InteractionDetail

    def dict_interaction(inp):
        # from srnatabac.core.m_rna import mRNA
        # from srnatabac.core.nc_rna import ncRNA
        from srnatabac.core.rna_feature import mRNA, ncRNA
        from srnatabac.core.region import Region
        from srnatabac.core.strand import Strand

        with open(inp, 'r', errors='replace') as inputfile:
            lines = inputfile.readlines()
            results = {}        # all interaction predicted in ssearch file
            pred = []           # pred format object
            id_inter = 0
            for l in range(0, len(lines)):
                if '>>>' in lines[l]:  # for a query
                    query = lines[l].split('>>>')[1].split()[0]
                    hit = 0  # number of hit
                    interaction = {}
                        # information for a hit : overlap, match, successive
                        # match,sequences and structure

                elif '>' in lines[l]:  # for all hits

                    inter = Interaction()
                    inter.details.software = Common.SOFTWARE_SSEARCH

                    hit += 1
                    if '>>' in lines[l]:  # first hit --> name target recuperation
                        id_inter += 1
                        target = lines[l].split()[0].lstrip('>')
                        hit = 1  # number of hit

                    id_hit = str(query + "&" + target)
                    info = lines[l + 2].split('identity')[1].split(' ')
                    overlapping = int(info[4])

                    coordinates = info[7].lstrip('(').rstrip(')\n').split(':')
                    query_coord = coordinates[0].split('-')
                    target_coord = coordinates[1].split('-')

                    q = []
                    t = []
                    o = []

                    c = 3
                    while (len(q) < overlapping):
                        for i in lines[l + c + 2].split()[1].replace("T", "U"):
                            q.append(i)
                        for i in lines[l + c + 3][7:-1].replace(":", " "):
                            o.append(i)
                        for i in lines[l + c + 4].split()[1].replace("T", "U"):
                            t.append(i)
                        c += 6

                    # interaction characteristics : name, coordinates + sequences
                    interaction = {
                        'mRNA': '>' + target,
                        'ncRNA': '>' + query,
                        'mRNA_coord': target_coord,
                        'ncRNA_coord': query_coord,
                        'q': q,
                        't': t,
                        'o': o,
                        'struct': structure(o, q, t)
                    }

                    inter.id = "%04d" % (id_inter) + '.' + str(hit)
                    inter.mrna_id = target.split('|')[0].split('_')[0]

                    inter.region_id = Region.get_region_from_string(target.split('|')[0].split('_')[1])

                    m_start = int(target.split('|')[1].split('-')[0])
                    m_stop = int(target.split('|')[1].split('-')[1])
                    m_strand = Strand.get_strand_from_string(target.split('|')[2][0])
                    m_gene_start = int(target.split('|')[3].split('-')[0])
                    m_gene_stop = int(target.split('|')[3].split('-')[1])

                    inter.mrna = mRNA(inter.mrna_id, m_start, m_stop, m_strand, m_gene_start, m_gene_stop)

                    inter.ncrna_id = query.split('|')[0]

                    nc_start = int(query.split('|')[1].split('-')[0])
                    nc_stop = int(query.split('|')[1].split('-')[1])
                    nc_strand = Strand.get_strand_from_string(query.split('|')[2][0])

                    inter.ncrna = ncRNA(inter.ncrna_id, nc_start, nc_stop, nc_strand)

                    inter.details.overlap_length = len(interaction['o'])
                    inter.details.match_count, inter.details.match_successive = stats(interaction)
                    inter.details.mrna_seq = t[::-1]
                    inter.details.ncrna_seq = q[::-1]
                    inter.details.symbolism, inter.details.pair_pct_GC, inter.details.pair_pct_AU, inter.details.pair_pct_GU = draw_inter(
                        interaction, inter.details.match_count)

                    inter.details.mrna_begin, inter.details.mrna_end = coord(target, target_coord)
                    inter.details.ncrna_begin, inter.details.ncrna_end = coord(query, query_coord)
                    #DONE IN add_energy component
                    # inter.define_start_stop()

                    results[str(id_inter) + '.' + str(hit)] = interaction
                    pred.append(inter)

        return results, pred

    def structure(o, q, t):
        seq = []
        dot_bracket = []
        M = t[::-1]
        sym = o[::-1]

        for i in range(0, len(o)):
            if (sym[i] != ' ') and (M[i] != '-'):
                dot_bracket.append('(')
                seq.append(M[i])
            elif (sym[i] == ' ') and (M[i] != '-'):
                dot_bracket.append('.')
                seq.append(M[i])

        dot_bracket.append('&')
        seq.append('&')
        for i in range(0, len(sym)):
            if (o[i] != ' ') and (q[i] != '-'):
                dot_bracket.append(')')
                seq.append(q[i])
            elif (o[i] == ' ') and (q[i] != '-'):
                dot_bracket.append('.')
                seq.append(q[i])

        return ''.join(seq), ''.join(dot_bracket)

    def stats(inter):
        app_succ = []
        app = 0
        tmp = 0
        for i in inter['o']:
            if i != ' ':
                app += 1
                tmp += 1
            else:
                if tmp != 0:
                    app_succ.append(tmp)
                tmp = 0
        if tmp != 0:
            app_succ.append(tmp)

        return app, max(app_succ)

    def coord(header, pos):
        start = int(header.split("|")[1].split('-')[0])  # sequence start
        stop = int(header.split("|")[1].split('-')[1])  # sequence stop
        strand = header.split("|")[2]

        if 'ncRNA' in header:
            if "+" in strand:
                startG = start + (int(pos[0]) - 1)
                stopG = start + (int(pos[1]) - 1)
            else:
                startG = stop - (int(pos[1]) - 1)
                stopG = stop - (int(pos[0]) - 1)

        elif 'mRNA' in header:
            gstart = int(header.split("|")[-2].split('-')[0])
            gstop = int(header.split("|")[-2].split('-')[1])

            if "+" in strand:
                startG = stop - (int(pos[1]) - 1)
                stopG = stop - (int(pos[0]) - 1)

            else:
                startG = start + (int(pos[0]) - 1)
                stopG = start + (int(pos[1]) - 1)

        return startG, stopG

    def draw_inter(inter, app):
        GC = 0
        AU = 0
        GU = 0

        for i in range(0, len(inter['o'])):
            if inter['o'][i] == '.':
                if inter['q'][i] == 'G' and inter['t'][i] == 'C':
                    GC += 1.0
                    inter['o'][i] = '|'
                elif inter['q'][i] == 'C' and inter['t'][i] == 'G':
                    GC += 1.0
                    inter['o'][i] = '|'
                elif inter['q'][i] == 'A' and inter['t'][i] == 'U':
                    AU += 1.0
                    inter['o'][i] = '|'
                elif inter['q'][i] == 'U' and inter['t'][i] == 'A':
                    AU += 1.0
                    inter['o'][i] = '|'
                elif inter['q'][i] == 'G' and inter['t'][i] == 'U':
                    GU += 1.0
                elif inter['q'][i] == 'U' and inter['t'][i] == 'G':
                    GU += 1.0
            else:
                pass

        return inter['o'][::-1], GC / app, AU / app, GU / app

    # MAIN #######
    # all the results
    dict_inter = dict_interaction(input)
    results = dict_inter[0]
    predictions = dict_inter[1]

    with gzip.open(output, 'ab') as f:
        pickle.dump(predictions, f, protocol=Common.PICKLE_COMPRESSION_RATE)

    with open(out_rnaeval, 'a') as output_fh2:
        for inter in results:
            tmp = "\n>%04d" % int(inter.split('.')[0]) + "." + inter.split('.')[1]
            tmp += "\n{}".format(results[inter]['struct'][0])
            tmp += "\n{}".format(results[inter]['struct'][1])
            output_fh2.write(tmp)

def add_energy(interaction_tmp_file, structure_file, energy_file, interaction_final_file):
    """ Get and add the duplex energy to the parsed file

    @param interaction_tmp_file: the temporary interaction file without energy
    @param structure_file: the structure and sequence file
    @param energy_file: the energy file
    @param interaction_final_file: the interaction_final_file file,
    """
    import pickle
    import gzip
    import os
    from srnatabac.utils.common import Common

    # build prediction list
    predictions = []
    with gzip.open(interaction_tmp_file, 'rb') as f:
        predictions.extend(pickle.load(f))

    # open structure and energy files
    if not os.path.isfile(structure_file):
        raise IOError('File "{}" not found or it is not a file.'.format(structure_file))
    if not os.path.isfile(energy_file):
        raise IOError('File "{}" not found or it is not a file.'.format(energy_file))

    # cursor temp and end dict
    res = { 'id':None, 'seq':None, 'struct':None, 'val': None }
    energy_dict = {}

    # check file length
    num_struct_lines = sum(1 for x in open(structure_file))
    num_energy_lines = sum(1 for x in open(energy_file))
    if num_energy_lines > num_struct_lines: raise Exception('File size error')

    #files reader generator
    energy_lines = (x for x in open(energy_file))
    struct_lines = (x for x in open(structure_file))

    # load first line
    energy_line = next(energy_lines).rstrip('\n')
    struct_line = next(struct_lines).rstrip('\n')

    # -- patterns
    # structure file            | energy file
    #----------------------------------------------------
    # >idA                      | sequenceA
    # sequenceA                 | structureA (energyA)
    # structureA                | sequenceB
    # >idB                      | structureB (energyB)
    # sequenceB                 | sequenceC
    # structureB                | structureC (energyC)
    # ....                      | ....
    #
    # -- patterns

    # read both files until one is done
    ended = False
    while not ended:
        try:
            #avoid blank lines
            while not energy_line: energy_line = next(energy_lines).rstrip('\n')
            while not struct_line: struct_line = next(struct_lines).rstrip('\n')

            # process
            # if res['id'] == None:
            #     print('{}'.format(struct_line))

            if res['id'] == None and struct_line[0:1] == '>':
                res['id'] = struct_line[1:]

            elif res['seq'] == None:
                if struct_line != energy_line:
                    raise Exception('Sequences are not equals')
                res['seq'] = energy_line
                #incr, next energy line (contains energy value at the end)
                energy_line = next(energy_lines).rstrip('\n')

            elif res['struct'] == None:
                tmp_e = energy_line.split(' ', maxsplit=1)
                energy_line_without_val = tmp_e[0].strip()
                energy_val = tmp_e[1].lstrip('(').rstrip(')').strip()
                if struct_line != energy_line_without_val:
                    raise Exception('Structures are not equals')
                if not energy_val:
                    raise Exception('No energy value found')
                res['struct'] = energy_line_without_val
                res['val'] = float(energy_val)
                #incr, next energy line
                energy_line = next(energy_lines).rstrip('\n')

            else:
                raise Exception('Add energy process got a bad ending')

            # save
            if res['id'] != None and res['seq'] != None\
                and res['struct'] != None and res['val'] != None:
                energy_dict[res['id']] = res['val']
                res = { 'id':None, 'seq':None, 'struct':None, 'val': None }

            #main cursor loop
            struct_line = next(struct_lines).rstrip('\n')

        except StopIteration:
            # save the last item
            if res['id'] != None and res['seq'] != None\
                and res['struct'] != None and res['val'] != None:
                energy_dict[res['id']] = res['val']
                res = { 'id':None, 'seq':None, 'struct':None, 'val': None }

            # end while
            ended = True

    # close files
    struct_lines.close()
    energy_lines.close()

    # add energy to prediction/interaction objects
    try:
        for p in predictions:
            p.details.energy_duplex = float(energy_dict[p.id])
    except KeyError:
        raise Exception('One interaction key has been forgotten')

    # dump each interaction into pickle gzip file
    with gzip.open(interaction_final_file, 'ab') as f:
        pickle.dump(predictions, f, protocol=Common.PICKLE_COMPRESSION_RATE)

def summarize(sum_file):
    with open(sum_file, 'w') as s:
        s.write('- SSearch executed.\n')

class Ssearch (Component):

    def define_parameters(self, query_fasta, target_fasta):

        # INPUT
        self.add_input_file("query_fasta", "Query sequences fasta format", default=query_fasta, required=True, file_format="fasta")
        self.add_input_file_list("target_fasta", "Target sequences fasta format [list|uniq file]", default=target_fasta, required=True, file_format="fasta")

        # OUTPUT
        self.add_output_file_list(
            "target_reverse", "Reverse target fasta sequence to orientate the sequence",
            pattern="{basename_woext}_orientate_ssearch.fasta", items=self.target_fasta)
        self.add_output_file_list(
            "output_ssearch", "Raw output of ssearch software",
            pattern="{basename_woext}_ssearch.raw", items=self.target_fasta)
        self.add_output_file_list(
            "input_rnaeval", "Convert duplex found by ssearch36 to calculate NRJ through RNAeval",
            pattern="{basename_woext}_ssearch.rnaeval", items=self.target_fasta)
        self.add_output_file_list(
            "output_energy", "Energy for each duplex", pattern="{basename_woext}_ssearch.energy", items=self.target_fasta)
        self.add_output_file_list(
            "output_parser", "Temporary file in object interaction (pickle gzip) format before adding energy of interaction",
            pattern="{basename_woext}_ssearch_tmp.pgz", items=self.target_fasta)
        self.add_output_file_list(
            "output_file", "Object interaction Pickle file gzip", pattern="{basename_woext}_ssearch.pgz", items=self.target_fasta)
        self.add_output_file("summary", "Summary of all actions done in this component", filename="sum_tmp.txt")

        # ERROR
        self.add_output_file(
            "stderr_reverse", "Standard error output for reverse sequence", filename="reverse_ssearch.stderr")
        self.add_output_file_list(
            "stderr_ssearch", "Standard error output for ssearch",
            pattern="{basename_woext}_ssearch.stderr", items=self.target_fasta)
        self.add_output_file(
            "stderr_rnaeval", "Standard error output for RNAeval calculation", filename="rnaeval_ssearch.stderr")
        self.add_output_file(
            "stderr_parse", "Standard error output for parsing", filename="parse_ssearch.stderr")
        self.add_output_file(
            "stderr_energy", "Standard error output for add energy", filename="energy_ssearch.stderr")

    def process(self):
        matrix_file = os.path.join(self.output_directory,"hybridation.mat")
        hybridation_matrix(matrix_file)

        target_reverse = PythonFunction(reverse, cmd_format='{EXE} {IN} {OUT} 2>> ' + self.stderr_reverse)
        target_reverse = MultiMap(target_reverse, inputs=[self.target_fasta], outputs=[self.target_reverse])

        ssearch = ShellFunction(self.get_exec_path("ssearch36") + " -f -3 -g -1 -3 -W 00 -w 500 -s " + matrix_file
                                + " -E 10000 " + self.query_fasta + " $1 >$2 2>> $3", cmd_format='{EXE} {IN} {OUT}')
        ssearch = MultiMap(ssearch, inputs=[self.target_reverse], outputs=[self.output_ssearch, self.stderr_ssearch], includes=self.query_fasta)

        parser = PythonFunction(parse_ssearch, cmd_format="{EXE} {IN} {OUT} 2>> " + self.stderr_parse)
        parser = MultiMap(parser, inputs=[self.output_ssearch], outputs=[self.output_parser, self.input_rnaeval])

        rnaeval = ShellFunction(self.get_exec_path("RNAeval") + "<$1 >$2 2>> " + self.stderr_rnaeval, cmd_format='{EXE} {IN} {OUT}')
        rnaeval = Map(rnaeval, inputs=[self.input_rnaeval], outputs=[self.output_energy])

        add_NRJ = PythonFunction(add_energy, cmd_format="{EXE} {IN} {OUT} 2>> " + self.stderr_energy)
        add_NRJ = MultiMap(add_NRJ, inputs=[self.output_parser, self.input_rnaeval, self.output_energy], outputs=[self.output_file])

        sum_function = PythonFunction(summarize, cmd_format="{EXE} {IN} {OUT}")
        sum_function( outputs=[self.summary] )

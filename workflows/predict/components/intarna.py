#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro, Anais Painset - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2015 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.1'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import os
from subprocess import Popen, PIPE

from jflow.component import Component
from jflow.abstraction import MultiMap

from weaver.function import ShellFunction, PythonFunction
from weaver.abstraction import Map

from srnatabac.utils.common import Common


def parse_intarna(input, output):
    import re
    import pickle
    import gzip
    from srnatabac.utils.common import Common
    from srnatabac.core.interaction import Interaction, InteractionDetail

    def parse_interaction(inp):
        """ Convert raw output of intaRNA into Interactions Objects """
        from srnatabac.core.region import Region
        from srnatabac.core.strand import Strand
        from srnatabac.core.rna_feature import mRNA, ncRNA
        with open(inp, 'r') as inputfile:
            lines = inputfile.readlines()
            iid = 0
            list_inter = []

            for l in range(0, len(lines)):
                # using tag to detect a new interaction
                if '|mRNA' in lines[l]:
                    inter = Interaction()
                    iid += 1
                    inter.id = "%04d" % (iid)
                    #inter.soft = "inta"
                    inter.details.software = Common.SOFTWARE_INTARNA
                    # get information about target (split fasta header)
                    ls = lines[l].split('|')
                    inter.mrna_id = ls[0].lstrip('>').split('_')[0]
                    inter.region_id = Region.get_region_from_string(ls[0].lstrip('>').split('_')[1])
                    m_start = int(ls[1].split('-')[0])
                    m_stop = int(ls[1].split('-')[1])
                    m_strand = Strand.get_strand_from_string(ls[2][0])
                    m_gene_start = int(ls[3].split('-')[0])
                    m_gene_stop = int(ls[3].split('-')[1])
                    inter.mrna = mRNA(inter.mrna_id, m_start, m_stop,
                                      m_strand, m_gene_start, m_gene_stop)
                    # get information about ncRNA (split fasta header)
                    ls2 = lines[l + 2].split('|')
                    inter.ncrna_id = ls2[0].lstrip('>')
                    nc_start = int(ls2[1].split('-')[0])
                    nc_stop = int(ls2[1].split('-')[1])
                    nc_strand = Strand.get_strand_from_string(ls2[2][0])
                    inter.ncrna = ncRNA(inter.ncrna_id, nc_start, nc_stop, nc_strand)
                    # get energy of software
                    inter.details.energy_software = float(lines[l + 20].split(' ')[1])
                    # get sequences involve into interaction
                    mRNA_s = lines[l]  # sequence mRNA
                    ncRNA_s = lines[l + 2]  # sequence ncRNA
                    mRNA_coord = lines[l + 10].split(': ')[1].rstrip().split(' -- ')
                    ncRNA_coord = lines[l + 13].split(': ')[1].rstrip().split(' -- ')
                    context_target_seq = []  # sequence mRNA NOT included into interaction
                    context_ncrna_seq = []  # sequence sRNA NOT included into interaction
                    duplex_target_seq = []  # nt from mRNA included into interaction // involved into duplex
                    duplex_ncrna_seq = []  # nt from ncRNA included into interaction // involved into duplex
                    for i in str(lines[l + 5]).rstrip():  # recuperation nts mRNA
                        context_target_seq.append(i)
                    for i in str(lines[l + 6]).rstrip():  # recuperation nts mRNA
                        duplex_target_seq.append(i)
                    for i in str(lines[l + 7]).rstrip():  # recuperation nts ncRNA
                        duplex_ncrna_seq.append(i)
                    for i in str(lines[l + 8]).rstrip():  # recuperation nts sRNA
                        context_ncrna_seq.append(i)
                    # interaction characteristics: name, coordinates + sequences
                    interaction_characteristics = {
                        'mRNA': mRNA_s,
                        'ncRNA': ncRNA_s,
                        'mRNA_coord': mRNA_coord,
                        'ncRNA_coord': ncRNA_coord, 'context_target_seq': context_target_seq,
                        'duplex_target_seq': duplex_target_seq, 'duplex_ncrna_seq': duplex_ncrna_seq,
                        'context_ncrna_seq': context_ncrna_seq
                    }

                    #inter.overlap, inter.app, inter.app_succ = stats(interaction_characteristics)
                    inter.details.overlap_length, inter.details.match_count, inter.details.match_successive = stats(
                        interaction_characteristics)
                    inter.details.mrna_seq, inter.details.symbolism, inter.details.ncrna_seq, inter.details.pair_pct_GC, inter.details.pair_pct_AU, inter.details.pair_pct_GU = duplex_information(
                        interaction_characteristics, inter.details.match_count)
                    inter.details.mrna_begin, inter.details.mrna_end = coord(mRNA_s, mRNA_coord)
                    inter.details.ncrna_begin, inter.details.ncrna_end = coord(ncRNA_s, ncRNA_coord)
                    # all interactions contained in the inputfile
                    list_inter.append(inter)

        return list_inter

    def duplex_information(interaction_characteristics, app):
        """ Construct duplex from intaRNA information
        Calculate base pairing statistiques
        """
        # defined start of interaction_characteristics in the sequence of mRNA
        it = re.finditer(r"\w+", ''.join(interaction_characteristics['duplex_target_seq']))
        for match in it:
            inter_start = match.span()[0]
            break
        # duplex information
        dmrna = []
        dncrna = []
        sym = []
        # base pairing stats
        GC = 0.0
        AU = 0.0
        GU = 0.0

        for i in range(inter_start, len(interaction_characteristics['duplex_target_seq'])):
            if (interaction_characteristics['context_target_seq'][i] == ' ') and (interaction_characteristics['duplex_target_seq'][i] != ' ') and \
               (interaction_characteristics['duplex_ncrna_seq'][i] != ' ') and (interaction_characteristics['context_ncrna_seq'][i] == ' '):
                dmrna.append(interaction_characteristics['duplex_target_seq'][i])
                dncrna.append(interaction_characteristics['duplex_ncrna_seq'][i])
                if(interaction_characteristics['duplex_target_seq'][i] == 'G' and interaction_characteristics['duplex_ncrna_seq'][i] == 'U'):
                    sym.append('.')
                    GU += 1.0 / app
                elif(interaction_characteristics['duplex_ncrna_seq'][i] == 'G' and interaction_characteristics['duplex_target_seq'][i] == 'U'):
                    sym.append('.')
                    GU += 1.0 / app
                else:
                    sym.append('|')
                    if(interaction_characteristics['duplex_target_seq'][i] == 'G' and interaction_characteristics['duplex_ncrna_seq'][i] == 'C'):
                        GC += 1.0 / app
                    elif(interaction_characteristics['duplex_ncrna_seq'][i] == 'G' and interaction_characteristics['duplex_target_seq'][i] == 'C'):
                        GC += 1.0 / app
                    elif(interaction_characteristics['duplex_target_seq'][i] == 'A' and interaction_characteristics['duplex_ncrna_seq'][i] == 'U'):
                        AU += 1.0 / app
                    elif(interaction_characteristics['duplex_ncrna_seq'][i] == 'A' and interaction_characteristics['duplex_target_seq'][i] == 'U'):
                        AU += 1.0 / app

            elif (interaction_characteristics['context_target_seq'][i] != ' ') and (interaction_characteristics['duplex_target_seq'][i] == ' ') and \
                 (interaction_characteristics['duplex_ncrna_seq'][i] == ' ') and (interaction_characteristics['context_ncrna_seq'][i] != ' '):
                dmrna.append(interaction_characteristics['context_target_seq'][i])
                sym.append(' ')
                dncrna.append(interaction_characteristics['context_ncrna_seq'][i])

            elif (interaction_characteristics['context_target_seq'][i] == ' ') and (interaction_characteristics['duplex_target_seq'][i] == ' ') and \
                 (interaction_characteristics['duplex_ncrna_seq'][i] == ' ') and (interaction_characteristics['context_ncrna_seq'][i] != ' '):
                dmrna.append('-')
                sym.append(' ')
                dncrna.append(interaction_characteristics['context_ncrna_seq'][i])

            elif (interaction_characteristics['context_target_seq'][i] != ' ') and (interaction_characteristics['duplex_target_seq'][i] == ' ') and \
                 (interaction_characteristics['duplex_ncrna_seq'][i] == ' ') and (interaction_characteristics['context_ncrna_seq'][i] == ' '):
                dmrna.append(interaction_characteristics['context_target_seq'][i])
                sym.append(' ')
                dncrna.append('-')

        return dmrna, sym, dncrna, GC, AU, GU

    def stats(inter):
        """
            Compute characteristics of interaction length, matches and successive_matches
        """
        stats = {'overlap': len(str(''.join(inter['duplex_target_seq'])).rstrip().lstrip())}
        overlap = len(str(''.join(inter['duplex_target_seq'])).rstrip().lstrip())
        app_succ = []
        app = 0
        tmp = 0
        for i in inter['duplex_target_seq']:
            if i != ' ':
                app += 1
                tmp += 1
            else:
                if tmp != 0:
                    app_succ.append(tmp)
                tmp = 0
        if tmp != 0:
            app_succ.append(tmp)

        stats['app_succ'] = max(app_succ)
        stats['app'] = app

#         return stats
        return overlap, app, max(app_succ)

    def coord(header, pos):
        start = int(header.split("|")[1].split('-')[0])  # sequence start
        stop = int(header.split("|")[1].split('-')[1])  # sequence stop
        strand = header.split("|")[2]

        if "-" in strand:
            stopG = stop - (int(pos[0]) - 1)
            startG = stop - (int(pos[1]) - 1)
        else:
            startG = start + (int(pos[0]) - 1)
            stopG = start + (int(pos[1]) - 1)

        return startG, stopG

    # all the results
    results = []
    results.extend(parse_interaction(input))
    # for each interaction DUMP it into pickle gzip file
    with gzip.open(output, 'ab') as f:
        pickle.dump(results, f, protocol=Common.PICKLE_COMPRESSION_RATE)


def run_intarna(exec_path, outdir, query_fasta, target_fasta, stdout_path, stderr_path):
    """
        Execute intaRNA software
    """
    import os
    import jflow.seqio as seqio
    from subprocess import Popen, PIPE
    import time

    # window folding parameter
    # maybe authorized to set by a user
    param = 100
    reader = seqio.SequenceReader(target_fasta)
    for id, desc, seq, qual in reader:
        tmp_file = str(time.time()) + str(os.getpid()) + ".tmp"
        current_file_path = os.path.join(outdir, tmp_file)
        out_fh = open(current_file_path, 'w')

        if len(seq) > param:
            seqio.writefasta(out_fh, [[id, desc, seq, qual]])
            out_fh.close()
            cmd = [exec_path, "-m", query_fasta, "-t",
                   current_file_path, "-w", str(param), "-o"]
            p = Popen(cmd, stdout=PIPE, stderr=PIPE)
            stdout, stderr = p.communicate()
            # write down the stdout
            with open(stdout_path, "a") as stdoh:
                stdoh.write(stdout.decode("utf-8"))
            # write down the stderr
            with open(stderr_path, "a") as stdeh:
                stdeh.write(stderr.decode("utf-8"))
            os.remove(current_file_path)
        else:
            seqio.writefasta(out_fh, [[id, desc, seq, qual]])
            out_fh.close()
            cmd = [exec_path, "-m", query_fasta, "-t", current_file_path, "-o"]
            p = Popen(cmd, stdout=PIPE, stderr=PIPE)
            stdout, stderr = p.communicate()
            # write down the stdout
            with open(stdout_path, "a") as stdoh:
                stdoh.write(stdout.decode("utf-8"))
            # write down the stderr
            with open(stderr_path, "a") as stdeh:
                stdeh.write(stderr.decode("utf-8"))
            os.remove(current_file_path)


def summarize(sum_file):
    with open(sum_file, 'w') as s:
        s.write('- IntaRNA executed.\n')

class IntaRNA (Component):

    def define_parameters(self, query_fasta, target_fasta):

        # INPUT
        self.add_input_file("query_fasta", "Query sequences fasta format",
                            default=query_fasta, required=True, file_format="fasta")
        self.add_input_file_list(
            "target_fasta", "Target sequences fasta format [list|uniq file]", default=target_fasta, required=True, file_format="fasta")

        # OUTPUT
        self.add_output_file_list("output_intarna", "Raw output of IntaRNA software",
                                  pattern="{basename_woext}_intarna.raw", items=self.target_fasta)
        self.add_output_file_list("output_file", "Object interaction Pickle file gzip",
                                  pattern="{basename_woext}_intarna.pgz", items=self.target_fasta)
        self.add_output_file(
            "summary", "Summary of all actions done in this component", filename="sum_tmp.txt")

        # ERROR
        self.add_output_file("stderr_parse", "Standard error output for parsing",
                             filename="parse_intarna.stderr")
        self.add_output_file_list("stderr_inta", "Standard error output for intaRNA",
                                  pattern="{basename_woext}_intarna.stderr", items=self.target_fasta)

    def process(self):

        intarna = PythonFunction(run_intarna, cmd_format='{EXE} ' + self.get_exec_path(
            "IntaRNA") + ' ' + self.output_directory + ' ' + self.query_fasta + ' {IN} {OUT}')
        intarna = MultiMap(intarna, includes=self.query_fasta, inputs=[
                           self.target_fasta],  outputs=[self.output_intarna, self.stderr_inta])

        parser = PythonFunction(
            parse_intarna, cmd_format="{EXE} {IN} {OUT} 2>> " + self.stderr_parse)
        parser = Map(parser, inputs=[self.output_intarna], outputs=[self.output_file])

        sum_function = PythonFunction(summarize, cmd_format="{EXE} {IN} {OUT}")
        sum_function(outputs=[self.summary])

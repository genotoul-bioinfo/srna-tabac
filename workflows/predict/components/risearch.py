#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro, Anais Painset - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2015 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.1'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import os
from subprocess import Popen, PIPE

from jflow.component import Component
from jflow.abstraction import MultiMap

from weaver.function import ShellFunction, PythonFunction
from weaver.abstraction import Map

from srnatabac.utils.common import Common

def parse_risearch(input, output):
    import re
    import pickle
    import gzip
    from srnatabac.utils.common import Common
    from srnatabac.core.interaction import Interaction, InteractionDetail

    def parse_interaction(inp):
        from srnatabac.core.region import Region
        from srnatabac.core.strand import Strand
        from srnatabac.core.rna_feature import mRNA, ncRNA
        """
            Convert raw output of RIsearch into Interactions Objects
        """
        with open(inp, 'r') as inputfile:
            lines = inputfile.readlines()
        results = []
        iid = 0

        for l in range(0, len(lines)):
            # using tag to detect a new interaction
            if 'vs.' in lines[l]:
                inter = Interaction()
                iid += 1
                inter.id = "%04d" % (iid)
                inter.details.software = Common.SOFTWARE_RISEARCH
                ncRNA_s = lines[l].split('vs. ')[0].split(' ')[1]
                mRNA_s = lines[l].split('vs. ')[1].split(' ')[1]
                # get information about target (split fasta header)
                inter.mrna_id = mRNA_s.lstrip('>').split('_')[0]
                inter.region_id = Region.get_region_from_string(mRNA_s.lstrip('>').split('|')[0].split('_')[1])
                m_start = int(mRNA_s.split('|')[1].split('-')[0])
                m_stop = int(mRNA_s.split('|')[1].split('-')[1])
                m_strand = Strand.get_strand_from_string(mRNA_s.split('|')[2][0])
                m_gene_start = int(mRNA_s.split('|')[3].split('-')[0])
                m_gene_stop = int(mRNA_s.split('|')[3].split('-')[1])
                inter.mrna = mRNA(inter.mrna_id, m_start, m_stop, m_strand, m_gene_start, m_gene_stop)
                # get information about ncRNA (split fasta header)
                inter.ncrna_id = ncRNA_s.split('|')[0].lstrip('>')
                nc_start = int(ncRNA_s.split('|')[1].split('-')[0])
                nc_stop = int(ncRNA_s.split('|')[1].split('-')[1])
                nc_strand = Strand.get_strand_from_string(ncRNA_s.split('|')[2][0])
                inter.ncrna = ncRNA(inter.ncrna_id, nc_start, nc_stop, nc_strand)
                inter.details.energy_software = float(lines[l + 2].replace(" ","").split(':')[1].split('(')[0])
                duplex_ncrna_seq = []   # nt from query // nts involved into duplex
                duplex_profil = []      # profil of interaction
                duplex_target_seq = []  # nt from target // nts involved into duplex
                for i in str(lines[l + 4]).rstrip():  # recuperation nts ncRNA
                    duplex_ncrna_seq.append(i)
                for i in str(lines[l + 5]).rstrip():  # recuperation of interaction profil
                    duplex_profil.append(i)
                for i in str(lines[l + 6]).rstrip():  # recuperation nts mRNA
                    duplex_target_seq.append(i)
                mRNA_coord = lines[l + 7].replace("(3' <-- 5')\n", "").split(' - ')
                mRNA_coord.reverse()
                ncRNA_coord = lines[l + 3].rstrip().split(' - ')
                # interaction characteristics : name, coordinates + sequences
                interaction = {
                    'mRNA': mRNA_s,
                    'ncRNA': ncRNA_s,
                    'mRNA_coord': mRNA_coord,
                    'ncRNA_coord': ncRNA_coord,
                    'duplex_ncrna_seq': duplex_ncrna_seq,
                    'duplex_profil': duplex_profil,
                    'duplex_target_seq': duplex_target_seq
                }
                inter.details.overlap_length, inter.details.match_count, inter.details.match_successive = stats(interaction)
                inter.details.mrna_seq = duplex_target_seq[::-1]
                inter.details.symbolism = duplex_profil[::-1]
                inter.details.ncrna_seq = duplex_ncrna_seq[::-1]
                inter.details.pair_pct_GC, inter.details.pair_pct_AU, inter.details.pair_pct_GU = pairing_composition(
                    interaction, inter.details.match_count)
                inter.details.mrna_begin, inter.details.mrna_end = coord(mRNA_s, mRNA_coord)
                inter.details.ncrna_begin, inter.details.ncrna_end = coord(ncRNA_s, ncRNA_coord)
                # all interactions contained in the inputfile
                results.append(inter)

        return results

    def stats(inter):
        """
            Compute characteristics of interaction length, matches and successive_matches
        """
        overlap = len(inter['duplex_profil'])
        app_succ = []
        app = 0
        tmp = 0
        for i in inter['duplex_profil']:
            if i != ' ':
                app += 1
                tmp += 1
            else:
                if tmp != 0:
                    app_succ.append(tmp)
                tmp = 0
        if tmp != 0:
            app_succ.append(tmp)
        return overlap, app, max(app_succ)

    def coord(header, pos):
        start = int(header.split("|")[1].split('-')[0])  # sequence start
        stop = int(header.split("|")[1].split('-')[1])  # sequence stop
        strand = header.split("|")[2]
        if "-" in strand:
            stopG = stop - (int(pos[0]) - 1)
            startG = stop - (int(pos[1]) - 1)
        else:
            startG = start + (int(pos[0]) - 1)
            stopG = start + (int(pos[1]) - 1)

        return startG, stopG

    def pairing_composition(inter, app):
        """
            Compute composition of pairing
        """
        GC = 0
        AU = 0
        GU = 0
        for i in range(0, len(inter['duplex_profil'])):
            if inter['duplex_profil'][i] == '|':
                if inter['duplex_ncrna_seq'][i] == 'G' and inter['duplex_target_seq'][i] == 'C':
                    GC += 1.0
                elif inter['duplex_ncrna_seq'][i] == 'C' and inter['duplex_target_seq'][i] == 'G':
                    GC += 1.0
                elif inter['duplex_ncrna_seq'][i] == 'A' and inter['duplex_target_seq'][i] == 'U':
                    AU += 1.0
                elif inter['duplex_ncrna_seq'][i] == 'U' and inter['duplex_target_seq'][i] == 'A':
                    AU += 1.0
            elif inter['duplex_profil'][i] == '.':
                GU += 1.0

        return GC / app, AU / app, GU / app
    # all the results
    results = []
    results.extend(parse_interaction(input))
    # for each interaction DUMP it into pickle gzip file
    with gzip.open(output, 'ab') as f:
        pickle.dump(results, f, protocol=Common.PICKLE_COMPRESSION_RATE)

def summarize(sum_file):
    with open(sum_file, 'w') as s:
        s.write('- RISearch executed.\n')

class Risearch (Component):

    def define_parameters(self, query_fasta, target_fasta):

        # INPUT
        self.add_input_file("query_fasta", "Query sequences fasta format (ncRNA)", default=query_fasta, required=True, file_format="fasta")
        self.add_input_file_list("target_fasta", "Target sequences fasta format [list|uniq file]", default=target_fasta, required=True, file_format="fasta")

        # OUTPUT
        self.add_output_file_list("output_risearch", "Raw output of RIsearch software", pattern="{basename_woext}_risearch.raw", items=self.target_fasta)
        self.add_output_file_list("output_file", "Object interaction Pickle file gzip", pattern="{basename_woext}_risearch.pgz", items=self.target_fasta)
        self.add_output_file("summary", "Summary of all actions done in this component", filename="sum_tmp.txt")

        # ERROR
        self.add_output_file_list("stderr_rish", "Standard error output for RIsearch", pattern="{basename_woext}_risearch.stderr", items=self.target_fasta)
        self.add_output_file("stderr_parse", "Standard error output for parsing", filename="parse_risearch.stderr")

    def process(self):
        risearch = ShellFunction(self.get_exec_path("RIsearch") + " -q " + self.query_fasta +
                                 " -t $1 -d 30 -m t04 -l 700 > $2 2>> $3", cmd_format='{EXE} {IN} {OUT}')
        risearch = MultiMap(risearch, inputs=[self.target_fasta], outputs=[self.output_risearch, self.stderr_rish], includes=self.query_fasta)
        
        parser = PythonFunction(parse_risearch, cmd_format="{EXE} {IN} {OUT} 2>> " + self.stderr_parse)
        parser = Map(parser, inputs=[self.output_risearch], outputs=[self.output_file])

        sum_function = PythonFunction(summarize, cmd_format="{EXE} {IN} {OUT}")
        sum_function( outputs=[self.summary] )

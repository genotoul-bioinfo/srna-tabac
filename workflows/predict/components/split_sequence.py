#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2015 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import os
import jflow.seqio as seqio
from jflow.component import Component
from weaver.function import PythonFunction

def split_seq(sequences_file, outdir, min_sequence, nb_seq_by_file, sum_file):
    """ Split a sequences file in sequences files with 'nb_seq_by_file' sequences per file.

    Keyword arguments:
    sequences_file -- file before cut
    outdir -- output directory
    min_sequence -- minimal sequence length derived from minimal matches expected
    nb_seq_by_file -- number of sequences per file
    sum_file -- the summary file
    project_name -- the project name
    """
    import os
    import gzip
    import pickle
    import jflow.seqio as seqio

    # sequence checking
    basename = os.path.basename(sequences_file)
    basename_woext = basename.split(".")[0]
    extensions = ".".join(basename.split(".")[1:])

    current_nb_seq_on_file = 0
    current_file_id = 0
    nb_seq_exclude = 0
    out_fh = None

    # For each sequence
    reader = seqio.SequenceReader(sequences_file)
    for id, desc, seq, qual in reader:
        if len(seq) >= int(min_sequence):
            current_nb_seq_on_file += 1
            # If current file is complete
            if int(current_nb_seq_on_file) > int(nb_seq_by_file) or out_fh is None:
                # Close current file
                if out_fh is not None:
                    out_fh.close()
                # Next output file
                current_nb_seq_on_file = 1
                current_file_id += 1

                current_file_path = os.path.join(
                    outdir, basename_woext + '_' + str(current_file_id) + '.' + extensions)
                if sequences_file.endswith(".gz"):
                    out_fh = seqio.xopen(current_file_path, "w")
                else:
                    out_fh = open(current_file_path, "w")
            # If sequences file is a FASTQ
            if reader.__class__.__name__ == "FastqReader":
                seqio.writefastq(out_fh, [[id, desc, seq, qual]])
            # If sequences file is a FASTA
            elif reader.__class__.__name__ == "FastaReader":
                seqio.writefasta(out_fh, [[id, desc, seq, qual]])
        else:
            nb_seq_exclude += 1
    out_fh.close()

    # summary
    with open(sum_file, 'w') as f:
        f.write('- Split mRNAs sequences:\n')
        f.write('-   Minimal sequence length (related to the matches threshold): {}\n'.format(min_sequence))
        f.write('-   # sequences per file: {}\n'.format(nb_seq_by_file))
        f.write('-   # excluded sequences: {}\n'.format(nb_seq_exclude))

class SplitSequence(Component):
    """ Split a sequences file in sequences files with 'nb_seq_by_file' sequences by file and filter sequences with length < minimal matches required.

    Keyword arguments:
    sequences_file -- file before cut
    nb_seq_by_file -- number of sequences per file before cut
    min_sequence -- minimal sequence length derived from minimal matches expected
    """
    def define_parameters(self, sequences_file, min_sequence, nb_seq_by_file=200):

        # PARAMETERS
        self.add_parameter("nb_seq_by_file", "The number of sequences per file before cut.", default=nb_seq_by_file, type="int", required=True)
        self.add_parameter("min_sequence", "Minimal sequence length derived from minimal matches expected", default=min_sequence, type="int", required=False)

        # INPUT
        self.add_input_file("input_file", "File to split.", default=sequences_file, required=True)

        # OUTPUT
        self.add_output_file_endswith("output_files", "The list of paths for splitted files.", pattern=".fa")
        self.add_output_file("summary", "Summary of all actions done in this component", filename="sum_tmp.txt")

        # ERROR
        self.add_output_file("stderr", "The stderr output file.", filename="splitseq.stderr")

    def process(self):
        split_s = PythonFunction(split_seq, cmd_format='{EXE} {IN} {ARG} {OUT} 2>> ' + self.stderr)
        split_s(inputs=self.input_file, arguments=[self.output_directory, self.min_sequence, self.nb_seq_by_file], outputs=self.summary)

#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro, Anais Painset - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2015 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.1'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import os
from subprocess import Popen, PIPE

from jflow.component import Component
from jflow.abstraction import MultiMap

from weaver.function import ShellFunction, PythonFunction
from weaver.abstraction import Map

from srnatabac.utils.common import Common

def linearize_fasta(input, output):
    list_seq = []
    curr_seq = None

    with open(input, 'r') as fr:
        for l in fr:
            line = l.strip()

            if '>' in line:
                if curr_seq:
                    list_seq.append(curr_seq)
                curr_seq = line + '\n' # add id of sequence
            else:
                curr_seq += line # add sequence
        if curr_seq:
            list_seq.append(curr_seq)

    with open(output, 'w') as fw:
        for seq in list_seq :
            fw.write(seq + "\n")

def parse_rnaplex(query_fasta, input, target_fasta, output):
    import sys
    import pickle
    import gzip
    from srnatabac.utils.common import Common
    from srnatabac.core.interaction import Interaction, InteractionDetail
    import jflow.seqio as seqio

    def load_fasta(fas):
        seq_to_dict = {}
        reader = seqio.SequenceReader(fas)
        for id, desc, seq, qualities in reader:
            seq_to_dict[id] = [id, desc, seq, qualities]
        return seq_to_dict

    def appariement(qo, to):
        iqo = list(qo[::-1].rstrip('.').lstrip('.'))
        ito = list(to.rstrip('.').lstrip('.'))
        i = 0
        compt = 0
        app_succ = []
        while len(iqo) > 0 or len(ito) > 0:
            if iqo[i] == '.' and ito[i] == '.':
                compt = 0
                iqo.pop(i)
                ito.pop(i)
            elif iqo[i] == '(' and ito[i] == ')':
                compt += 1
                iqo.pop(i)
                ito.pop(i)
            elif iqo[i] == '.'and ito[i] == ')':
                iqo.pop(i)
                compt = 0
            elif iqo[i] == '(' and ito[i] == '.':
                ito.pop(i)
                compt = 0
            app_succ.append(compt)

        return max(app_succ)

    def stats(inter):
        structure = inter.split()[0]
        app = structure.count('(')
        soft_energy = float(inter.split()[4].lstrip('(').rstrip(')'))
        mRNA_coord = inter.split()[1].split(',')
        ncRNA_coord = inter.split()[3].split(',')
        t = structure.split('&')[0]  # structure mRNA
        q = structure.split('&')[1]  # structure ncRNA
        # suppress "context"
        if t[0] == '.':
            t = t.lstrip('.')
            mRNA_coord[0] = int(mRNA_coord[0]) + 1
        if t[-1] == '.':
            t = t.rstrip('.')
            mRNA_coord[1] = int(mRNA_coord[1]) - 1
        if q[0] == '.':
            q = q.lstrip('.')
            ncRNA_coord[0] = int(ncRNA_coord[0]) + 1
        if q[-1] == '.':
            q = q.rstrip('.')
            ncRNA_coord[1] = int(ncRNA_coord[1]) - 1

        app_succ = appariement(t, q)

        return {'mRNA_coord': mRNA_coord, 'ncRNA_coord': ncRNA_coord, 'q': q, 't': t}, app_succ, app, soft_energy

    def draw_inter(inter, app):
        """
            Associate parenthesis interaction to align-like interaction
        """
        inter_mRNA = inter['m'].replace('T', 'U')
        struct_mRNA = inter['t']
        inter_ncRNA = inter['s'].replace('T', 'U')
        struct_ncRNA = inter['q']
        imrna = []
        isrna = []
        sym = []
        GU = 0.0
        AU = 0.0
        GC = 0.0
        # accross structures
        i = 0  # mRNA
        j = len(struct_ncRNA) - 1  # sRNA
        while i <= len(struct_mRNA) - 1 and j >= 0:
            if (struct_mRNA[i] == '(') and (struct_ncRNA[j] == (')')):
                imrna.append(inter_mRNA[i])
                isrna.append(inter_ncRNA[j])
                if ((inter_mRNA[i] == 'G') and (inter_ncRNA[j] == 'U')) or ((inter_mRNA[i] == 'U') and (inter_ncRNA[j] == 'G')):
                    sym.append('.')
                    GU += 1.0
                elif ((inter_mRNA[i] == 'A') and (inter_ncRNA[j] == 'U')) or ((inter_mRNA[i] == 'U') and (inter_ncRNA[j] == 'A')):
                    sym.append('|')
                    AU += 1.0
                else:
                    sym.append('|')
                    GC += 1.0
                i += 1
                j -= 1

            elif (struct_mRNA[i] == '(') and (struct_ncRNA[j] == ('.')):
                imrna.append('-')
                sym.append(' ')
                isrna.append(inter_ncRNA[j])
                j -= 1

            elif (struct_mRNA[i] == '.') and (struct_ncRNA[j] == (')')):
                imrna.append(inter_mRNA[i])
                sym.append(' ')
                isrna.append('-')
                i += 1

            elif (struct_mRNA[i] == '.') and (struct_ncRNA[j] == ('.')):
                imrna.append(inter_mRNA[i])
                sym.append(' ')
                isrna.append(inter_ncRNA[j])
                i += 1
                j -= 1

        return imrna, sym, isrna, GC / app, AU / app, GU / app

    def coord(header, pos):

        start = int(header.split("|")[1].split('-')[0])  # sequence start
        stop = int(header.split("|")[1].split('-')[1])  # sequence stop
        strand = header.split("|")[2]

        if "-" in strand:
            stopG = stop - (int(pos[0]) - 1)
            startG = stop - (int(pos[1]) - 1)
        else:
            startG = start + (int(pos[0]) - 1)
            stopG = start + (int(pos[1]) - 1)

        return startG, stopG

    def assoc_seq(inter, query_fasta, target_fasta):
        records_nc = load_fasta(query_fasta)
        records_m = load_fasta(target_fasta)

        seq_mRNA = records_m[inter['mRNA'][1:]][2][
            int(inter['mRNA_coord'][0]) - 1:int(inter['mRNA_coord'][1])]
        seq_ncRNA = records_nc[inter['ncRNA'][1:]][2][
            int(inter['ncRNA_coord'][0]) - 1:int(inter['ncRNA_coord'][1])]

        return {'m': str(seq_mRNA), 's': str(seq_ncRNA)}

    def fast_parse(inp, query_fasta, target_fasta):
        from srnatabac.core.region import Region
        from srnatabac.core.strand import Strand
        from srnatabac.core.rna_feature import mRNA, ncRNA

        with open(inp, 'r') as inputfile:
            lines = inputfile.readlines()
        results = []
        iid = 0
        for l in range(0, len(lines)):
            if '|mRNA' in lines[l]:
                iid += 1
                inter = Interaction()
                inter.id = "%04d" % (iid)
                inter.details.software = Common.SOFTWARE_RNAPLEX
                mRNA_s = lines[l].rstrip()
                ncRNA_s = lines[l + 1].rstrip()
                interaction = {'mRNA': mRNA_s, 'ncRNA': ncRNA_s}
                structure = lines[l + 2]
                inter.mrna_id = mRNA_s.split('_')[0].lstrip('>')
                inter.region_id = Region.get_region_from_string(mRNA_s.split('|')[0].split('_')[1])
                m_start = int(mRNA_s.split('|')[1].split('-')[0])
                m_stop = int(mRNA_s.split('|')[1].split('-')[1])
                m_strand = Strand.get_strand_from_string(mRNA_s.split('|')[2][0])
                m_gene_start = int(mRNA_s.split('|')[3].split('-')[0])
                m_gene_stop = int(mRNA_s.split('|')[3].split('-')[1])
                inter.mrna = mRNA(inter.mrna_id, m_start, m_stop, m_strand, m_gene_start, m_gene_stop)
                inter.ncrna_id = ncRNA_s.split('|')[0].lstrip('>')
                nc_start = int(ncRNA_s.split('|')[1].split('-')[0])
                nc_stop = int(ncRNA_s.split('|')[1].split('-')[1])
                nc_strand = Strand.get_strand_from_string(ncRNA_s.split('|')[2][0])
                inter.ncrna = ncRNA(inter.ncrna_id, nc_start, nc_stop, nc_strand)
                interaction.update(stats(structure)[0])
                inter.details.match_successive, inter.details.match_count, inter.details.energy_software = stats(structure)[1:]
                interaction.update(
                    assoc_seq(interaction, query_fasta, target_fasta))
                inter.details.mrna_seq, inter.details.symbolism, inter.details.ncrna_seq, inter.details.pair_pct_GC, inter.details.pair_pct_AU, inter.details.pair_pct_GU = draw_inter(
                    interaction, inter.details.match_count)
                inter.details.overlap_length = len(inter.details.symbolism)
                inter.details.mrna_begin, inter.details.mrna_end = coord(mRNA_s, interaction['mRNA_coord'])
                inter.details.ncrna_begin, inter.details.ncrna_end = coord(ncRNA_s, interaction['ncRNA_coord'])
                results.append(inter)
        return results
    # all the results
    results = []
    results.extend(fast_parse(input, query_fasta, target_fasta))
    # for each interaction DUMP it into pickle gzip file
    with gzip.open(output, 'ab') as f:
        pickle.dump(results, f, protocol=Common.PICKLE_COMPRESSION_RATE)

def summarize(sum_file):
    with open(sum_file, 'w') as s:
        s.write('- RNAPlex executed.\n')

class Rnaplex (Component):
    def define_parameters(self, query_fasta, target_fasta):
        # INPUT
        self.add_input_file("query_fasta", "Query sequences fasta format", default=query_fasta, required=True, file_format="fasta")
        self.add_input_file_list("target_fasta", "Target sequences fasta format [list|uniq file]", default=target_fasta, required=True, file_format="fasta")
        # OUTPUT
        self.add_output_file("query_fasta_linear", "Linearize query fasta", filename="query_linear_rnaplex.fasta")
        self.add_output_file_list("target_fasta_linear", "Linearize target fasta",pattern="{basename_woext}_linear_rnaplex.fasta", items=self.target_fasta)
        self.add_output_file_list("output_rnaplex", "Raw output of RNAplex software",pattern="{basename_woext}_rnaplex.raw", items=self.target_fasta)
        self.add_output_file_list("output_file", "Object interaction Pickle file gzip", pattern="{basename_woext}_rnaplex.pgz", items=self.target_fasta)
        self.add_output_file("summary", "Summary of all actions done in this component", filename="sum_tmp.txt")
        # ERROR
        self.add_output_file_list("stderr_plex", "Standard error output for RNAplex", pattern="{basename_woext}_rnaplex.stderr", items=self.target_fasta)
        self.add_output_file("stderr_parse", "Standard error output for parsing", filename="parse_rnaplex.stderr")
        self.add_output_file("stderr_linearize_query", "Standard error output for linearize queries", filename="linearize_ncrna.stderr")
        self.add_output_file("stderr_linearize_target", "Standard error output for linearize targets", filename="linearize_mrna.stderr")

    def process(self):
        format_input = PythonFunction(linearize_fasta, cmd_format = "{EXE} {IN} {OUT} 2>> " + self.stderr_linearize_query)
        format_input = Map(format_input, inputs=self.query_fasta, outputs=self.query_fasta_linear)

        format_input2 = PythonFunction(linearize_fasta, cmd_format = "{EXE} {IN} {OUT} 2>> " + self.stderr_linearize_target)
        format_input2 = MultiMap(format_input2, inputs=self.target_fasta, outputs=self.target_fasta_linear)

        rnaplex = ShellFunction(self.get_exec_path("RNAplex") + " -q " + self.query_fasta_linear + " -t $1 >$2 2>> $3", cmd_format='{EXE} {IN} {OUT}')
        rnaplex = MultiMap(rnaplex, inputs=self.target_fasta_linear, outputs=[self.output_rnaplex, self.stderr_plex], includes=self.query_fasta_linear)

        parser = PythonFunction(parse_rnaplex, cmd_format="{EXE} " + self.query_fasta + " {IN} {OUT} 2>> " + self.stderr_parse)
        parser = MultiMap(parser, inputs=[self.output_rnaplex, self.target_fasta_linear], outputs=[self.output_file], includes=self.query_fasta)

        sum_function = PythonFunction(summarize, cmd_format="{EXE} {IN} {OUT}")
        sum_function( outputs=[self.summary] )

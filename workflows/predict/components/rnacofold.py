#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro, Anais Painset - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2015 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.1'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import os
from subprocess import Popen, PIPE

from jflow.component import Component
from jflow.abstraction import MultiMap

from weaver.function import ShellFunction, PythonFunction
from weaver.abstraction import Map

from srnatabac.utils.common import Common

def concat_sequence(query_fasta, target_fasta, output):
    import jflow.seqio as seqio
    readerA = seqio.SequenceReader(query_fasta)
    readerB = seqio.SequenceReader(target_fasta)

    with open(output, 'a') as out:
        for idA, descA, seqA, qualitiesA in readerA:
            for idB, descB, seqB, qualitiesB in readerB:
                out.write(">{}&{}\n{}&{}\n".format(idA, idB, seqA, seqB))

def parse_rnacofold(input, output):
    """
        Convert raw output of intaRNA into Interactions Objects
    """
    import re
    import sys
    import pickle
    import gzip
    from srnatabac.utils.common import Common
    from srnatabac.core.interaction import Interaction, InteractionDetail

    # increase recursion limit ==> palliatif avant
    # developpement systeme pile/file
    sys.setrecursionlimit(10000)
    def dict_interaction(inp):
        from srnatabac.core.region import Region
        from srnatabac.core.strand import Strand
        from srnatabac.core.rna_feature import mRNA, ncRNA

        with open(inp, 'r') as inputfile:
            lines = inputfile.readlines()
        results = []
        iid = 0

        for l in range(0, len(lines)):
            if '|mRNA' in lines[l]:
                iid += 1
                ncRNA_m = lines[l].split('&')[0]
                mRNA_m = lines[l].split('&')[1]
                s = lines[l + 1].split('&')[0]  # sequence nt sRNA
                m = lines[l + 1].split('&')[1].rstrip()  # sequence nt mRNA_m
                # global structure
                struct = lines[l + 2].split(' ')[0]
                soft_energy = lines[l + 2].split(' ')[1:]
                # suppress internal structure and return the structure of
                # interaction
                q = ssearch_internal_struct(struct.split('&')[0])  # structure on sRNA
                t = ssearch_internal_struct(struct.split('&')[1])  # structure on mRNA_m
                # RNAcofold could produce non overlapping results
                # consider only interaction with more than ONE paired
                if q.count("(") > 1:
                    inter = Interaction()
                    inter.id = "%04d" % (iid)
                    inter.details.software = Common.SOFTWARE_RNACOFOLD
                    mRNA_coord = (t.find(')'), t.rfind(')'))  # position on mRNA_m
                    ncRNA_coord = (q.find('('), q.rfind('('))  # position on ncRNA_m
                    # interaction characteristics : name, coordinates +
                    # sequences
                    interaction = {
                        'mRNA_m': mRNA_m,
                        'ncRNA_m': ncRNA_m,
                        'm': m,
                        'q': q,
                        't': t,
                        's': s,
                        'ncRNA_coord': ncRNA_coord,
                        'mRNA_coord': mRNA_coord
                    }

                    inter.mrna_id = mRNA_m.split('_')[0]
                    inter.region_id = Region.get_region_from_string(mRNA_m.split('|')[0].split('_')[1])
                    m_start = int(mRNA_m.split('|')[1].split('-')[0])
                    m_stop = int(mRNA_m.split('|')[1].split('-')[1])
                    m_strand = Strand.get_strand_from_string(mRNA_m.split('|')[2][0])
                    m_gene_start = int(mRNA_m.split('|')[3].split('-')[0])
                    m_gene_stop = int(mRNA_m.split('|')[3].split('-')[1])
                    inter.mrna = mRNA(inter.mrna_id, m_start, m_stop, m_strand, m_gene_start, m_gene_stop)
                    inter.ncrna_id = ncRNA_m.split('|')[0].lstrip('>')
                    nc_start = int(ncRNA_m.split('|')[1].split('-')[0])
                    nc_stop = int(ncRNA_m.split('|')[1].split('-')[1])
                    nc_strand = Strand.get_strand_from_string(ncRNA_m.split('|')[2][0])
                    inter.ncrna = ncRNA(inter.ncrna_id, nc_start, nc_stop, nc_strand)
                    inter.details.energy_software = float(''.join(soft_energy).rstrip(')\n').lstrip('('))
                    inter.details.match_count = int(struct.split('&')[0].count('(')) - int(struct.split('&')[0].count(')'))
                    inter.details.match_successive = appariement(q[ncRNA_coord[0]:ncRNA_coord[1] + 1], t[mRNA_coord[0]:mRNA_coord[1] + 1])
                    inter.details.mrna_seq, inter.details.symbolism, inter.details.ncrna_seq, inter.details.pair_pct_GC, inter.details.pair_pct_AU, inter.details.pair_pct_GU = draw_inter(interaction, inter.details.match_count)
                    inter.details.overlap_length = len(inter.details.symbolism)
                    inter.details.mrna_begin, inter.details.mrna_end = coord(mRNA_m, mRNA_coord)
                    inter.details.ncrna_begin, inter.details.ncrna_end = coord(ncRNA_m, ncRNA_coord)
                    # all interactions contained in the inputfile
                    results.append(inter)

        return results

    def ssearch_internal_struct(rna):
        if re.search(r"\(.*\)", rna):
            tmp = re.sub(r'\((\.*)\)', r'.\1.', rna, 1)
            rna = tmp
            return ssearch_internal_struct(rna)
        else:
            return rna

    def appariement(qo, to):
        # structure of interaction only
        iqo = list(qo[::-1])
        ito = list(to)

        i = 0
        compt = 0
        app_succ = []

        while len(iqo) > 0 or len(ito) > 0:
            if iqo[i] == '(' and ito[i] == ')':
                compt += 1
                iqo.pop(i)
                ito.pop(i)
            elif iqo[i] == '.':
                iqo.pop(i)
                compt = 0
            elif ito[i] == '.':
                ito.pop(i)
                compt = 0
            app_succ.append(compt)

        return max(app_succ)

    def coord(header, pos):

        start = int(header.split("|")[1].split('-')[0])  # sequence start
        stop = int(header.split("|")[1].split('-')[1])  # sequence stop
        strand = header.split("|")[2]

        if "-" in strand:
            stopG = stop - (int(pos[0]))
            startG = stop - (int(pos[1]))
        else:
            startG = start + (int(pos[0]))
            stopG = start + (int(pos[1]))

        return startG, stopG

    def draw_inter(inter, app):
        # list with corresponding indexes of sequences and structures
        inter_mRNA = list(inter['m'][inter['mRNA_coord'][0]:inter['mRNA_coord'][1] + 1])
        struct_mRNA = list(inter['t'][inter['mRNA_coord'][0]:inter['mRNA_coord'][1] + 1])
        inter_ncRNA = list(inter['s'][inter['ncRNA_coord'][0]:inter['ncRNA_coord'][1] + 1])
        struct_ncRNA = list(inter['q'][inter['ncRNA_coord'][0]:inter['ncRNA_coord'][1] + 1])
        #var
        imrna = []
        isrna = []
        sym = []
        GU = 0.0
        AU = 0.0
        GC = 0.0
        # accross structures
        i = 0  # mRNA
        j = len(struct_ncRNA) - 1  # sRNA
        while i <= len(struct_mRNA) and j >= 0:
            if (struct_mRNA[i] == ')') and (struct_ncRNA[j] == ('(')):
                imrna.append(inter_mRNA[i])
                isrna.append(inter_ncRNA[j])
                if ((inter_mRNA[i] == 'G') and (inter_ncRNA[j] == 'U')) or ((inter_mRNA[i] == 'U') and (inter_ncRNA[j] == 'G')):
                    sym.append('.')
                    GU += 1.0
                elif ((inter_mRNA[i] == 'A') and (inter_ncRNA[j] == 'U')) or ((inter_mRNA[i] == 'U') and (inter_ncRNA[j] == 'A')):
                    sym.append('|')
                    AU += 1.0
                else:
                    sym.append('|')
                    GC += 1.0
                i += 1
                j -= 1

            elif (struct_mRNA[i] == ')') and (struct_ncRNA[j] == ('.')):
                imrna.append('-')
                sym.append(' ')
                isrna.append(inter_ncRNA[j])
                j -= 1

            elif (struct_mRNA[i] == '.') and (struct_ncRNA[j] == ('(')):
                imrna.append(inter_mRNA[i])
                sym.append(' ')
                isrna.append('-')
                i += 1

            elif (struct_mRNA[i] == '.') and (struct_ncRNA[j] == ('.')):
                imrna.append(inter_mRNA[i])
                sym.append(' ')
                isrna.append(inter_ncRNA[j])
                i += 1
                j -= 1

        return imrna, sym, isrna, GC / app, AU / app, GU / app

    # all the results
    results = []
    results.extend(dict_interaction(input))
    # for each interaction DUMP it into pickle gzip file
    with gzip.open(output, 'ab') as f:
        pickle.dump(results, f, protocol=Common.PICKLE_COMPRESSION_RATE)

def summarize(sum_file):
    with open(sum_file, 'w') as s:
        s.write('- RNACofold executed.\n')

class Rnacofold (Component):

    def define_parameters(self, query_fasta, target_fasta):

        # INPUT
        self.add_input_file("query_fasta", "Query sequences fasta format", default=query_fasta, required=True, file_format="fasta")
        self.add_input_file_list("target_fasta", "Target sequences fasta format [list|uniq file]", default=target_fasta, required=True, file_format="fasta")

        # OUTPUT
        self.add_output_file_list("data", "Concatenation between actors and targets", pattern="{basename_woext}_rnacofold.data", items=self.target_fasta)
        self.add_output_file_list("output_rnacofold", "Raw output of RNAcofold software", pattern="{basename_woext}_rnacofold.raw", items=self.target_fasta)
        self.add_output_file_list("output_file", "Object interaction Pickle file gzip", pattern="{basename_woext}_rnacofold.pgz", items=self.target_fasta)
        self.add_output_file("summary", "Summary of all actions done in this component", filename="sum_tmp.txt")

        # ERROR
        self.add_output_file_list("stderr_cofold", "Standard error output for RNAcofold", pattern="{basename_woext}_rnacofold.stderr", items=self.target_fasta)
        self.add_output_file("stderr_parse", "Standard error output for parsing", filename="parse_rnacofold.stderr")

    def process(self):

        format_input = PythonFunction(concat_sequence, cmd_format="{EXE} " + self.query_fasta + " {IN} {OUT}")
        format_input = MultiMap(format_input, inputs=[self.target_fasta], outputs=[self.data], includes=self.query_fasta)

        rnacofold = ShellFunction(self.get_exec_path("RNAcofold") + " --noPS <$1 >$2 2>> $3", cmd_format='{EXE} {IN} {OUT}')
        rnacofold = MultiMap(rnacofold, inputs=[self.data], outputs=[self.output_rnacofold, self.stderr_cofold])

        parser = PythonFunction(parse_rnacofold, cmd_format="{EXE} {IN} {OUT} 2>> " + self.stderr_parse)
        parser = Map(parser, inputs=[self.output_rnacofold], outputs=[self.output_file])

        sum_function = PythonFunction(summarize, cmd_format="{EXE} {IN} {OUT}")
        sum_function( outputs=[self.summary] )

#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro, Anais Painset - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2015 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.1'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import os
import pickle
import gzip
from srnatabac.utils.common import Common
from subprocess import Popen, PIPE

from jflow.component import Component

from weaver.function import PythonFunction, ShellFunction
from weaver.abstraction import Map


def filter_structure(matches, successive_matches, list_pred, summary, ncrna_mrna_list, predictions_tmp):
    import pickle
    import gzip
    from srnatabac.utils.common import Common
    from jflow.featureio import GFF3IO, GFF3Record
    from srnatabac.core.interaction import Interaction, InteractionDetail
    from srnatabac.core.strand import Strand
    from srnatabac.core.region import Region
    inputs = []
    with gzip.open(list_pred, 'rb') as f:
        inputs.extend(pickle.load(f))
    # Load interactions
    predictions = []
    for l in inputs:
        with gzip.open(l, 'rb') as f:
            predictions.extend(pickle.load(f))
    # get & save  mrna & ncrna ids sets
    mrna_ids = set()
    ncrna_ids = set()
    for p in predictions:
        mrna_ids.add(p.mrna_id)
        ncrna_ids.add(p.ncrna_id)
    with gzip.open(ncrna_mrna_list, 'wb') as f:
        pickle.dump({"actors": ncrna_ids, "targets": mrna_ids},
                    f, protocol=Common.PICKLE_COMPRESSION_RATE)
    # filter
    # basically no positive energy allowed
    # and beginning of the interaction must be between the start and stop of the considered region
    fpred = [p for p in predictions
             if p.details.match_count >= int(matches)
             and p.details.match_successive >= int(successive_matches)
             and p.details.energy_duplex <= 0
             and p.details.mrna_begin >= p.mrna.start and p.details.mrna_end <= p.mrna.stop]
    # attribute ids for all interactions from all softs
    iid = 1
    for i in fpred:
        #ids
        i.id = "%04d" % (iid)
        iid += 1
    # create the brief summary file
    with open(summary, 'w') as s:
        s.write("- Applied filters:\n")
        s.write("-   Matches threshold: {}. \n".format(matches))
        s.write("-   Successive matches threshold: {}. \n".format(successive_matches))
        s.write("- Interactions & filter threshold:\n")
        s.write("-   Total # of interactions: {}\n".format(len(predictions)))
        s.write("-   # of interactions that crossed the threshold: {}. \n".format(len(fpred)))
        s.write("-   # of interactions excluded: {}. \n".format(len(predictions) - len(fpred)))
    # for each interaction DUMP it into pickle gzip file
    with gzip.open(predictions_tmp, 'ab') as f:
        pickle.dump(fpred, f, protocol=Common.PICKLE_COMPRESSION_RATE)

class FilterPrediction (Component):

    def define_parameters(self, input_inter, matches, successive_matches):
        # INPUTS
        self.add_input_file_list(
            "input_inter", "All interactions predictions raw files from several softwares", default=input_inter, required=True)
        # OUTPUTS
        self.add_output_file("dump_input", "Dumping long list of input",
                             filename="dump_inter_tmp.pgz")
        self.add_output_file("list_ncrna_mrna", "List of regulators and targets",
                             filename="ncrna_mrna.pgz")
        self.add_output_file(
            "predictions_tmp", "Temporary file of filtered interactions without NRJ of duplex", filename="predictions_tmp.pgz")
        self.add_output_file("summary", "Summary of interaction filtered", filename="sum_tmp.txt")
        # ERROR
        self.add_output_file("stderr_filter_structure",
                             "Standard error for filter and compute structure", filename="filter_structure.stderr")
        # PARAMETERS
        self.add_parameter("matches", "Threshold number matches", default=matches, required=False)
        self.add_parameter("successive_matches", "Threshold number successive matches",
                           default=successive_matches, required=False)

    def process(self):
        """ Run process : filter and create pickle file of complete target
        """
        # dump all raw files from different software
        with gzip.open(self.dump_input, 'ab') as f:
            pickle.dump(self.input_inter, f, protocol=Common.PICKLE_COMPRESSION_RATE)

        filter_and_structure = PythonFunction(
            filter_structure, cmd_format="{EXE} {ARG} " + self.dump_input + " {OUT} 2>> " + self.stderr_filter_structure)
        filter_and_structure(includes=[self.input_inter], arguments=[self.matches, self.successive_matches],
                             outputs=[self.summary, self.list_ncrna_mrna, self.predictions_tmp])

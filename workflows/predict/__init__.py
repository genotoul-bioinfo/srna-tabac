#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro, Anais Painset - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2015 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.1'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import os
from jflow.workflow import Workflow
from srnatabac.core.user_information import UserInformation,UserProject
from srnatabac.dao.user_handler import UserHandler
from srnatabac.utils.common import Common

class PredictFasta(Workflow):
    """ Predict module: parallel-tasks between ncRNA list and mRNA list on several prediction softwares """

    def get_description(self):
        """ Description for the prediction module """
        return """Compute interactions between mRNAs and ncRNAs.
            Inputs are two FASTA files: one describing mRNAs, the other the ncRNAs.
            Convenient tool for small unstructured sets or test sets."""

    def _base_parameters(self):
        """ All basic parameters used in predictfasta and predictGFF3 workflow """
        # FILTER CRITERIA ##
        self.add_parameter("matches", "Threshold number matches [default 10]",\
            display_name="Minimum matches in interaction?",required=False, group="Filters", default=10, type="matches_min")
        self.add_parameter("successive_matches", "Threshold number successives matches [default 10]",\
            display_name="Minimum successive matches in interaction?", required=False, group="Filters", default=10, type="succ_matches_min")

        # SOFTWARE CHOICES ##
        self.add_parameter("intarna", "Process intaRNA predictions", display_name=Common.SOFTWARE_INTARNA,\
            required=False, type="bool", group="Softwares")
        self.add_parameter("rnaplex", "Process RNAplex predictions", display_name=Common.SOFTWARE_RNAPLEX,\
            required=False, type="bool", group="Softwares")
        self.add_parameter("risearch", "Process RIsearch predictions", display_name=Common.SOFTWARE_RISEARCH,\
            required=False, type="bool", group="Softwares")
        self.add_parameter("rnacofold", "Process RNAcofold predictions", display_name=Common.SOFTWARE_RNACOFOLD,\
            required=False, type="bool", group="Softwares")
        self.add_parameter("ssearch", "Process Ssearch predictions", display_name=Common.SOFTWARE_SSEARCH,\
            required=False, type="bool", group="Softwares")

        # PARAMETERS
        self.add_parameter("project_name", "Give a name to your project",\
            display_name="Project name", type="str", required=True, group="Project informations")
        self.add_parameter("align_output", "Do you want an alignment-like output?",\
            display_name="Alignment output", type="bool", default=False, group="Project informations")
        self.add_parameter("user_email", "Your email address, you will receive the direct link to your workspace.",\
            display_name="Email", type="email", required=True, group="Project informations")

    def define_parameters(self, function="process"):
        #INPUTS
        self.add_input_file("input_file_one", "[FASTA] ncRNA FASTA file with correct identifiers syntax.",\
            display_name="ncRNA", required=True, file_format="srnatabacfasta", group="Data files", type="browsefile")
        self.add_input_file("input_file_two", "[FASTA] mRNA fasta file with correct identifiers syntax.",\
            display_name="mRNA", required=True, file_format="srnatabacfasta", group="Data files", type="browsefile")

        #PARAMETERS & FILTERS
        self._base_parameters()


    def pre_process(self):
        """ Before processing: register the user """
        user = UserHandler.register_user(self, self.user_email, self.project_name)

    def _control_input_files(self):
        """ All basic controls """
        # if no software were chosen, throw away
        if self.intarna == False and self.risearch == False and self.rnacofold == False\
            and self.rnaplex == False and self.ssearch == False:
            raise Exception('Specify at least one software to run the prediction.')

    def _exec_process(self, project_name, ncrna, mrna, summary_files):
        """ Execute process """

        # split big FASTA input file and trimmed short sequences under the minimal matches required
        split_seq = self.add_component("SplitSequence", [mrna, self.matches, self.get_resource('nb_seq_by_file')])
        summary_files.append(split_seq.summary)

        # folding ncRNA
        fold_ncrna = self.add_component("FoldNcrna", [ncrna])
        summary_files.append(fold_ncrna.summary)

        # run prediction softwares
        pred = []
        if self.intarna == True:
            intarna = self.add_component("IntaRNA", [ncrna, split_seq.output_files])
            pred += intarna.output_file
            summary_files.append(intarna.summary)

        if self.risearch == True:
            risearch = self.add_component("Risearch", [ncrna,  split_seq.output_files])
            pred += risearch.output_file
            summary_files.append(risearch.summary)

        if self.rnacofold == True:
            rnacofold = self.add_component("Rnacofold", [ncrna, split_seq.output_files])
            pred += rnacofold.output_file
            summary_files.append(rnacofold.summary)

        if self.rnaplex == True:
            rnaplex = self.add_component("Rnaplex", [ncrna, split_seq.output_files])
            pred += rnaplex.output_file
            summary_files.append(rnaplex.summary)

        if self.ssearch == True:
            ssearch = self.add_component("Ssearch", [ncrna, split_seq.output_files])
            pred += ssearch.output_file
            summary_files.append(ssearch.summary)

        # process results
        energy_and_filter = self.add_component("FilterPrediction", [pred, self.matches, self.successive_matches])
        summary_files.append(energy_and_filter.summary)

        # add duplex energy to all interactions
        add_energy = self.add_component("AddEnergy", [energy_and_filter.predictions_tmp])
        summary_files.append(add_energy.summary)

        # other outputs
        format_output = self.add_component("FormatOutput", [add_energy.predictions_complete, self.align_output, True, False])
        summary_files.append(format_output.summary)

        # summarize all action
        sum_up = self.add_component("Summarize", [project_name, summary_files])

        # email
        user_info = UserHandler().get_user_by_email(self.user_email)
        if user_info is None: raise Exception('User not found')

        project_info = user_info.get_project_by_workflow_id(self.id)
        socket_opt = self.jflow_config_reader.get_socket_options()
        status_placeholder = '###get_status()###'
        user_url = "http://{}:{}/projects?user={}".format(socket_opt[0], socket_opt[1], user_info.get_user_key().get_key())
        user_msg = "Dear sRNA-Tabac user,<br/><br/>"
        user_msg += "Your project {} (#{}) is {}. It is available for 7 days.<br/>".format(project_name, project_info.get_workflow_id(), status_placeholder)
        if user_info.has_only_one_project(): # warn the user
            user_msg += "Since you only have one project available on your workspace, <u>your workspace will also be deleted in 7 days</u>.<br/>"
            user_msg += "Consider downloading generated files.<br/>"
        user_msg += "You can access to <a href='{}'>your workspace here</a> to see the details.<br/><br/>".format(user_url)
        user_msg += "The sRNA-Tabac team"
        self.set_to_address(self.user_email)
        self.set_subject("[sRNA-Tabac] your project {} (#{}) is {}".format(project_name, project_info.get_workflow_id(), status_placeholder))
        self.set_message(user_msg)


    def process(self):
        """ Run the workflow """
        # CONTROLS
        self._control_input_files()

        # project name
        p_name = self.project_name if self.project_name != None else 'Untitled'

        ##--EXECUTE PROCESS
        self._exec_process(p_name, self.input_file_one, self.input_file_two, [])


class PredictGFF3(PredictFasta):
    """ Predict module: parallel-tasks between ncRNA list and mRNA list on several prediction softwares """

    def get_description(self):
        """ Description for the prediction module """
        return """Compute interactions between mRNAs and ncRNAs.
            Inputs are: a GFF3 file describing actors (ncRNAs) and targets (mRNAs),
            the associated reference genome FASTA file.
            Convenient tool for big, structured data sets."""

    def define_parameters(self, function="process"):
        #INPUTS
        self.add_input_file("input_file_one", "[GFF3] the GFF3 file containing mRNA and ncRNA.",\
            display_name="gff3", required=True, file_format="gff3", group="Data files", type="browsefile")
        self.add_input_file("input_file_two", "[FASTA] the genome reference FASTA file.",\
            display_name="genome reference", required=True, file_format="fasta", group="Data files", type="browsefile")
        # REGION CHOICE ##
        self.add_parameter("predict_5utr", "Select to include 5'UTR region in prediction [default: False]",\
            display_name="Predict 5'UTR", required=False, type="bool", default=False, group="Predict regions")
        self.add_parameter("predict_cds", "Select to include CDS region in prediction [default: True]",\
            display_name="Predict CDS", required=False, type="bool", default=False, group="Predict regions")
        self.add_parameter("predict_3utr", "Select to include 3'UTR region in prediction [default: False]",\
            display_name="Predict 3'UTR", required=False, type="bool", default=False, group="Predict regions")
        # AFTER / BEFORE CHOICE ##
        self.add_parameter("before_5utr", "Based on CDS START, simulate 5'UTR START relative position to virtually create 5'UTR region if 'five_prime_UTR' tag is not found in GFF3 [default: 30]",\
            display_name="Define 5'UTR start", required=False, default=30, group="Simulate UTRs", type="positiveValue")
        self.add_parameter("after_5utr", "Based on CDS START, simulate 5'UTR STOP relative position to virtually create 5'UTR region if 'five_prime_UTR' tag is not found in GFF3 [default: 30]",\
            display_name="Define 5'UTR stop", required=False, default=30, group="Simulate UTRs", type="positiveValue")
        self.add_parameter("before_3utr", "Based on CDS STOP, simulate 3'UTR START relative position to virtually create 3'UTR region if 'three_prime_UTR' tag is not found in GFF3 [default: 30]",\
            display_name="Define 3'UTR start", required=False, default=30, group="Simulate UTRs", type="positiveValue")
        self.add_parameter("after_3utr", "Based on CDS STOP, simulate 3'UTR STOP relative position to virtually create 3'UTR region if 'three_prime_UTR' tag is not found in GFF3 [default: 30]",\
            display_name="Define 3'UTR stop", required=False, default=30, group="Simulate UTRs", type="positiveValue")
        #PARAMETERS & FILTERS - super()
        super(PredictGFF3, self)._base_parameters()

    def process(self):
        """ Run the workflow """
        #summary files
        sum_files=[]

        #--CONTROLS
        super(PredictGFF3, self)._control_input_files()
        if self.predict_5utr == False and self.predict_cds == False and self.predict_3utr == False:
            raise Exception('Specify at least one region to run the prediction.')

        # get sequence - transform gff3 files into ncRNA and mRNA files
        attr = [self.input_file_two, self.input_file_one,\
            self.predict_5utr, self.predict_cds, self.predict_3utr,\
            self.before_5utr, self.after_5utr, self.before_3utr, self.after_3utr]

        get_seq_ncrna = self.add_component("GetSequence", attr, component_prefix='ncrna')
        ncrna = get_seq_ncrna.fasta_file
        sum_files.append(get_seq_ncrna.summary)

        get_seq_mrna = self.add_component("GetSequence", attr, component_prefix='mrna')
        mrna = get_seq_mrna.fasta_file
        sum_files.append(get_seq_mrna.summary)

        #--Execute process
        p_name = self.project_name if self.project_name != None else os.path.splitext(self.input_file_two)[0].split('/')[-1]
        super(PredictGFF3, self)._exec_process(p_name, ncrna, mrna, sum_files)

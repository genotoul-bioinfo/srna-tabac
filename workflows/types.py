#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2015 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import argparse
import datetime

def date(datestr):
    from srnatabac.utils.srnatabac_config_reader import SrnatabacConfigReader
    try:
        return datetime.datetime.strptime(datestr, SrnatabacConfigReader().get_date_format())
    except:
        raise argparse.ArgumentTypeError("'{}' is an invalid date!".format(datestr))

def matches_min(matches):
    if int(matches) < 6:
        raise argparse.ArgumentTypeError("'{}' is too small : generates non-relevant interactions (too short)!".format(matches))
    return matches

def succ_matches_min(successive_matches):
    if int(successive_matches) < 4:
        raise argparse.ArgumentTypeError("'{}' is too small : generates non-relevant interactions (too short)!".format(successive_matches))
    return successive_matches

def positiveValue(value):
    if int(value) < 0:
        raise argparse.ArgumentTypeError("'{}' needs to be positive!".format(value))
    return value

def email(email):
    import re
    email_re = re.compile(
        r"(^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*"  # dot-atom
        r'|^"([\001-\010\013\014\016-\037!#-\[\]-\177]|\\[\001-011\013\014\016-\177])*"' # quoted-string
        r')@(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?$', re.IGNORECASE  # domain
    )
    if not email_re.match(email):
        raise argparse.ArgumentTypeError("Invalid email address.")
    return email

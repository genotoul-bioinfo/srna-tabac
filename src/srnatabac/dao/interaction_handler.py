#
# Copyright (C) 2016 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2016 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

from jflow.workflows_manager import WorkflowsManager
from srnatabac.dao.abstract_handler import AbstractHandler
from srnatabac.dao.user_handler import UserHandler
from srnatabac.core.authkey import UserAuthkey, ProjectAuthkey, CompleteAuthkey
from srnatabac.core.region import Region

class InteractionHandler(AbstractHandler):
    """ Represents a handler for "Interaction" objects.
    Extends srnatabac.dao.AbstractHandler.

    Keyword attributes:
    wfmanager -- the jflow workflow manager
    user_handler -- the user handler
    """

    def __init__(self):
        self.wfmanager = WorkflowsManager()
        self.user_handler = UserHandler()

    def _get_interactions(self, wf_id):
        """ Get all interactions.

        Keyword arguments:
        wf_id -- the workflow id
        """
        return self.get_file_output(
            workflow_id=wf_id,
            component_name='AddEnergy.default',
            filename='predictions.pgz'
        )

    def _get_folded_ncrna(self, wf_id):
        """ Get the folded ncrnas.

        Keyword arguments:
        wf_id -- the workflow id
        """
        return self.get_file_output(
            workflow_id=wf_id,
            component_name='FoldNcrna.default',
            filename='ncrna_dbn.json'
        )

    def _get_indexes(self, wf_id):
        """ Get the index file.

        Keyword arguments:
        wf_id -- the workflow id
        """
        return self.get_file_output(
            workflow_id=wf_id,
            component_name='FormatOutput.default',
            filename='predictions_index.pgz'
        )

    def get_all_interactions(self, complete_authkey):
        """ Get all interactions of a specific project

        Keyword arguments:
        complete_authkey -- the complete authkey
        """
        if not isinstance(complete_authkey, CompleteAuthkey): raise TypeError
        return self._get_interactions(self.user_handler.get_workflow_id_by_authkey(complete_authkey))

    def get_interactions_indexes(self, complete_authkey):
        """ Get all indexes from a project.

        Keyword arguments:
        complete_authkey -- the complete authkey
        """
        if not isinstance(complete_authkey, CompleteAuthkey): raise TypeError
        return self._get_indexes(self.user_handler.get_workflow_id_by_authkey(complete_authkey))

    def get_involved_regions(self, complete_authkey):
        """ Get regions involved in interactions of a specific project

        Keyword arguments:
        complete_authkey -- the complete authkey
        """
        if not isinstance(complete_authkey, CompleteAuthkey): raise TypeError
        res = set()
        for i in self.get_all_interactions(complete_authkey):
            res.add(Region.get_string_value(i.region_id))
            if len(res) == 3:
                break
        return list(res)

    def get_interaction_by_id(self, complete_authkey, interaction_id):
        """ Get an specific interaction, searched by its ID.

        Keyword arguments:
        complete_authkey -- the targeted complete authkey
        interaction_id -- the specific interaction id
        """
        if not isinstance(complete_authkey, CompleteAuthkey): raise TypeError
        predictions = self.get_all_interactions(complete_authkey)
        i = None
        if predictions is not None:
            for p in predictions:
                if p.id == interaction_id:
                    i = p
                    break
        return i

    def get_interaction_ncrna_infos(self, complete_authkey, interaction_id):
        """  Get ncrna informations.

        Keyword arguments:
        complete_authkey -- the complete authkey
        interaction_id -- the interaction id
        """
        if not isinstance(complete_authkey, CompleteAuthkey): raise TypeError
        obj = None
        i = self.get_interaction_by_id(complete_authkey, interaction_id)
        pid = self.user_handler.get_workflow_id_by_authkey(complete_authkey)
        if i != None and pid != None:
            dbn_file_content = self._get_folded_ncrna(pid)
            t = dbn_file_content[i.ncrna_id] # sequence and structure infos
            if t:
                obj = {
                    'ncrna': {
                        'id': i.ncrna.id,
                        'start': i.ncrna.start,
                        'stop': i.ncrna.stop,
                        'strand': i.ncrna.strand
                    },
                    'interaction': {
                        'id': i.id,
                        'software': i.details.software,
                        'ncrna_begin':i.details.ncrna_begin,
                        'ncrna_end':i.details.ncrna_end,
                        'sequence': t['sequence'],
                        'structure':t['structure']
                    }
                }
        return obj

#
# Copyright (C) 2016 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2016 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

from jflow.workflows_manager import WorkflowsManager
from srnatabac.dao.abstract_handler import AbstractHandler

class RNAHandler(AbstractHandler):
    """ Represents a handler for "xRNA" objects.
    Extends srnatabac.dao.AbstractHandler.

    Keyword attributes:
    wfmanager -- the jflow workflow manager
    """
    def __init__(self):
        self.wfmanager = WorkflowsManager()

    def _get_project_actors_and_targets(self, project):
        """ Get the list of actors (ncRNAs) and targets (mRNAs) involved in the interaction.

        Keyword arguments:
        project -- the project id
        """
        component = 'GFF3ToPrediction.default' if self.wfmanager.get_workflow(project).name == 'load' else 'FilterPrediction.default'
        return self.get_file_output(project, component, 'ncrna_mrna.pgz')

    def get_actors(self, project_id):
        """ Get the ncRNAs involved in a given project

        Keyword arguments:
        project_id -- the project id
        """
        return self._get_project_actors_and_targets(project_id)['actors']

    def get_targets(self, project_id):
        """ Get the mRNAs involved in a given project

        Keyword arguments:
        project_id -- the project id
        """
        return self._get_project_actors_and_targets(project_id)['targets']

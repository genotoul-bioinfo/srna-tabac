#
# Copyright (C) 2016 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2016 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import os
import pickle as pickle
import gzip
from srnatabac.utils.srnatabac_config_reader import SrnatabacConfigReader
from srnatabac.core.user_information import UserInformation
from srnatabac.core.authkey import UserAuthkey, ProjectAuthkey, CompleteAuthkey

class UserHandler (object):
    """ Represents a handler for user projects objects.

    Keyword attributes:
    output_dir -- the user specific output directory
    """

    def __init__(self):
        self.output_dir = SrnatabacConfigReader().get_save_directory()

    # def get_all_users_uuid(self):
    def get_all_users_authkeys(self):
        """ Get all users authkeys """
        res = []
        if os.path.exists(self.output_dir):
            res = [ UserAuthkey(f[:-4]) for f in os.listdir(self.output_dir) if os.path.isfile(os.path.join(self.output_dir, f)) ]
        return res

    def get_all_projects(self):
        """ Check exceptionnal uses """
        res = []
        for uk in self.get_all_users_authkeys():
            user_informations = self.get_user_data(uk)
            if user_informations is not None:
                for up in user_informations.get_projects():
                    res.append(up)
        return res

    def _get_user_file_path(self, user_authkey):
        """Get the user file path.

        Keyword argument:
        user_authkey -- the user authkey
        """
        if not isinstance(user_authkey, UserAuthkey): raise TypeError
        res = None
        user_file = os.path.join(self.output_dir, user_authkey.get_key() + ".pgz")
        if os.path.isdir(self.output_dir):
            res = user_file
        return res

    def user_has_data(self, user_authkey):
        """Test if the user has his own file.

        Keyword argument:
        user_authkey -- the user authkey
        """
        if not isinstance(user_authkey, UserAuthkey): raise TypeError
        path = self._get_user_file_path(user_authkey)
        res = None
        if path is not None:
            res = os.path.isfile(path)
        return res

    def user_exists(self, user_authkey):
        """ Test if the user exists

        Keyword argument:
        user_authkey -- the user authkey
        """
        if not isinstance(user_authkey, UserAuthkey): raise TypeError
        return self.user_has_data(user_authkey)

    def _load_user_informations(self, user_authkey):
        """ Load user information from his information file.

        Keyword argument:
        # uid -- the user uuid
        user_authkey -- the user authkey
        """
        if not isinstance(user_authkey, UserAuthkey): raise TypeError
        res = None
        if self.user_has_data(user_authkey) != None:
            res = pickle.load(gzip.open(self._get_user_file_path(user_authkey), 'rb'))
        return res

    def get_user_data(self, user_authkey):
        """ Get the user informations if the user exists, None else

        Keyword argument:
        user_authkey -- the user authkey
        """
        if not isinstance(user_authkey, UserAuthkey): raise TypeError
        res = None
        if self.user_exists(user_authkey):
            res = self._load_user_informations(user_authkey)
        return res

    def get_user_projects(self, user_authkey):
        """ Get the project list for a specific user if the user exists, None else

        Keyword argument:
        user_authkey -- the user authkey
        """
        user_informations = self.get_user_data(user_authkey)
        res = None
        if user_informations is not None and user_informations.has_projects():
            res = user_informations.get_projects()
        return res

    def find_user_project(self, project_authkey):
        """ Search a specific project key, None if not found

        Keyword argument:
        project_authkey -- the project authkey
        """
        if not isinstance(project_authkey, ProjectAuthkey): raise TypeError
        res = None
        for uk in self.get_all_users_authkeys():
            user_informations = self.get_user_data(uk)
            if user_informations is not None and user_informations.has_projects():
                for up in user_informations.get_projects():
                    if up.get_project_key() == project_authkey:
                        res = up
                        break
        return res

    def get_project_by_complete_key(self, complete_authkey):
        if not isinstance(complete_authkey, CompleteAuthkey): raise TypeError
        user_data = self.get_user_data(complete_authkey.get_user_part())
        if user_data is None: raise Exception('User not found')
        project = user_data.get_project_by_id(complete_authkey.get_project_part())
        if project is None: raise Exception('Project not found')
        return project

    def get_user_by_email(self, mail_searched):
        """Search the user by its email.

        Keyword argument:
        mail_searched -- the email searched
        """
        user_found = None
        for user_authkey in self.get_all_users_authkeys():
            if os.path.exists(self.output_dir):
                current_user = self._load_user_informations(user_authkey)
                if current_user is not None and current_user.get_email() == mail_searched:
                    user_found = current_user
                    break
        return user_found

    def get_user_by_workflow_id(self, wf_id):
        """ """
        if not isinstance(wf_id, str) and not isinstance(wf_id, int): raise TypeError
        found = None
        for user_authkey in self.get_all_users_authkeys():
            user_data = self.get_user_data(user_authkey)
            try:
                if user_data is not None and user_data.get_project_by_workflow_id(wf_id) is not None:
                    found = user_data
                    break
            except:
                pass
        return found

    def get_workflow_id_by_authkey(self, complete_authkey):
        """ Get a project id (workflow id) by its authkey (uuid)

        Keyword arguments:
        complete_authkey -- the complete authkey
        """
        if not isinstance(complete_authkey, CompleteAuthkey): raise TypeError
        res = None
        user_data = self.get_user_data(complete_authkey.get_user_part())
        if user_data is not None:
            project = user_data.get_project_by_id(complete_authkey.get_project_part())
            if project is not None:
                res = project.get_workflow_id()
        return res

    def delete_user(self, user_authkey):
        """ Delete a user if has no project left

        Keyword arguments:
        user_authkey -- the user authkey to remove
        """
        if not isinstance(user_authkey, UserAuthkey): raise TypeError
        if not self.user_exists(user_authkey):
            raise Exception('User does not exists')
        #user exists, he has a file, test projects
        if not self.get_user_projects(user_authkey):
            os.remove(self._get_user_file_path(user_authkey))

    def save_user_data(self, user_info):
        """ Save user information into his file.
        If the file does not exists, create it.

        Keyword argument:
        user_info -- the user information
        """
        if not isinstance(user_info, UserInformation): raise TypeError
        #create user dir
        if not os.path.exists(self.output_dir):
            os.mkdir(self.output_dir)
        #override current data
        with gzip.open(self._get_user_file_path(user_info.get_user_key()), 'wb') as f:
            pickle.dump(user_info, f)

    @staticmethod
    def register_user(workflow, user_email, project_name):
        """ Register a new workflow/project for a specific user.
        Return the UserInformation.

        Keyword arguments:
        workflow -- the newly created workflow
        user_email -- the user email
        project_name -- the current submitted project
        """
        from srnatabac.core.user_information import UserInformation
        # get user informations
        user_handler = UserHandler()
        user = user_handler.get_user_by_email(user_email)
        # if user did not exist, create it
        if user == None:
            user = UserInformation(user_email)
        # add a project
        user.add_project(workflow, project_name)
        # save user data
        user_handler.save_user_data(user)
        return user

#
# Copyright (C) 2016 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2016 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import json
import pickle as pickle
import gzip
from jflow.workflows_manager import WorkflowsManager

class AbstractHandler(object):
    """ Represents a abstract handler for "srnatabac.core" objects."""

    # TODO: useful for big PGZ files, use it with streaming-Pickle (sPickle) and Common.PICKLE_COMPRESSION_RATE = 0
#     def yield_pickle_file(self, workflow_id=None, component_name=None, filename=None):
#         from srnatabac.utils.sPickle import sPickle
#         if workflow_id == None:
#             e = Exception('Workflow id not found')
#             yield str(e)
#         elif component_name == None:
#             e = Exception('Component not found')
#             yield str(e)
#         elif filename == None:
#             e = Exception('File ' + filename + ' not found')
#             yield str(e)
#         else:
#             datafile = WorkflowsManager().get_workflow_outputs(workflow_id)[component_name][filename]
#             if datafile.endswith(".pgz"):
#                 for element in sPickle().s_load(gzip.open(datafile, 'rb')):
#                     yield element
#             else:
#                 raise FileNotFoundError('File ' + filename + ' has a unknown extension.')

    def get_file_output(self, workflow_id=None, component_name=None, filename=None):
        """ Open a file in a given workflow, a given component, with a given name and extension.

        Keyword arguments:
        workflow_id -- the workflow id
        component_name -- the component name
        filename -- the filename
        """
        if workflow_id == None:
            e = Exception('Workflow id not found')
            return str(e)
        elif component_name == None:
            e = Exception('Component not found')
            return str(e)
        elif filename == None:
            e = Exception('File {} not found'.format(filename))
            return str(e)
        else:
            datafile = WorkflowsManager().get_workflow_outputs(workflow_id)[component_name][filename]
            if datafile.endswith(".json") or datafile.endswith(".index"):
                content = None
                with open(datafile) as f:
                    content = json.load(f)
                return content
            elif datafile.endswith(".pgz"):
                content = None
                with gzip.open(datafile, 'rb') as f:
                    content = pickle.load(f)
                return content
            else:
                raise IOError('File {} has a unknown extension.'.format(filename))

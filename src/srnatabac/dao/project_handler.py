#
# Copyright (C) 2016 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2016 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.1'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

from jflow.workflows_manager import WorkflowsManager
import jflow.utils as utils
from srnatabac.dao.abstract_handler import AbstractHandler
from srnatabac.dao.user_handler import UserHandler
from srnatabac.core.user_information import UserProject, UserInformation
from srnatabac.core.authkey import UserAuthkey, ProjectAuthkey, CompleteAuthkey
from srnatabac.utils.common import Common

class ProjectHandler(AbstractHandler):
    """ Represents a handler for "projects" (aka. workflow).
    Extends srnatabac.dao.AbstractHandler.

    Keyword attributes:
    wfmanager -- the jflow workflow manager
    user_handler -- the user handler
    """
    def __init__(self):
        self.wfmanager = WorkflowsManager()
        self.user_handler = UserHandler()

    def _format_project_status(self, workflow, pname):
        """ Format the date/string/int attributes of a given project status.

        Keywords arguments:
        workflow -- the targeted project id
        pname -- the project name
        """
        import time
        import datetime
        import jflow.utils as jflow_utils
        from jflow.workflow import Workflow

        # foramt time (start, end, elasped)
        timeformat = "%Y %b %d - %H:%M:%S" #start/end time format
        start_time = (time.strftime(timeformat, time.localtime(workflow.start_time)) if workflow.start_time else "-")
        end_time = (time.strftime(timeformat, time.localtime(workflow.end_time)) if workflow.end_time else "-")
        if workflow.start_time:
            elapsed_time = (str(workflow.end_time - workflow.start_time) if workflow.end_time else str(time.time() - workflow.start_time))
            elapsed_time = str(datetime.timedelta(seconds=int(str(elapsed_time).split(".")[0])))
        else:
            elapsed_time = "-"

        # format wf components status
        components = []
        softs = {
            Common.SOFTWARE_RNAPLEX: False,
            Common.SOFTWARE_RNACOFOLD: False,
            Common.SOFTWARE_RISEARCH: False,
            Common.SOFTWARE_SSEARCH: False,
            Common.SOFTWARE_INTARNA: False
        }

        for i, component in enumerate(workflow.get_components_nameid()):
            status_info = workflow.get_component_status(component)
            components.append({
                "name": component,
                "total": status_info["tasks"],
                "waiting": status_info["waiting"],
                "failed": status_info["failed"],
                "running": status_info["running"],
                "aborted": status_info["aborted"],
                "completed": status_info["completed"]
            })

            #find softwares informations
            if workflow.name == 'predictfasta' or workflow.name == 'predictgff3':
                ctmp = component.lower()
            elif workflow.name == 'load':
                if component == 'GFF3ToPrediction.default':
                    try:
                        ctmp = self.get_file_output(workflow.id, 'GFF3ToPrediction.default', 'softwares.pgz')['softwares']
                    except:
                        ctmp = {}
                else:
                    ctmp = component.lower()
            else:
                raise Exception('Workflow type unknown')

            #save softs
            if Common.SOFTWARE_RNAPLEX.lower() in ctmp:
                softs[Common.SOFTWARE_RNAPLEX] = True
            if Common.SOFTWARE_RNACOFOLD.lower() in ctmp:
                softs[Common.SOFTWARE_RNACOFOLD] = True
            if Common.SOFTWARE_RISEARCH.lower() in ctmp:
                softs[Common.SOFTWARE_RISEARCH] = True
            if Common.SOFTWARE_SSEARCH.lower() in ctmp:
                softs[Common.SOFTWARE_SSEARCH] = True
            if Common.SOFTWARE_INTARNA.lower() in ctmp:
                softs[Common.SOFTWARE_INTARNA] = True

        # multi criteria sorting
        components = Common.multikeysort(components, ['aborted', 'failed', 'running', '-completed'])

        # format wf status
        return {
            "id": jflow_utils.get_nb_string(workflow.id),
            "name": workflow.name,
            "project_name": pname,
            "softwares": softs,
            "metadata": workflow.metadata,
            "errors": workflow.get_errors(),
            "status": workflow.get_status(),
            "elapsed_time": elapsed_time,
            "start_time": start_time,
            "end_time": end_time,
            "components": components
        }

    def get_user_project_status(self, complete_authkey):
        """ Get the status of a given project key, if it is a project key

        Keywords arguments:
        complete_authkey -- the authkey
        """
        if not isinstance(complete_authkey, CompleteAuthkey): raise TypeError
        status = None
        # process
        user_projects = self.user_handler.get_user_projects(complete_authkey.get_user_part())
        if user_projects != None:
            # get linked workflows and search the project id
            for project in user_projects:
                if complete_authkey.get_project_part() == project.get_project_key():
                    wf = self.wfmanager.get_workflow(project.get_workflow_id())
                    status = self._format_project_status(wf, project.get_project_name())
                    break
        return status

    def get_user_projects_status(self, user_authkey):
        """ Get the status of all projects of a user

        Keywords arguments:
        user_authkey -- the user authkey
        """
        from operator import itemgetter
        if not isinstance(user_authkey, UserAuthkey): raise TypeError
        status = []
        user_projects = self.user_handler.get_user_projects(user_authkey)
        if user_projects is not None:
            # dict to bind wfid to project name
            pnames = { p.get_workflow_id() : p.get_project_name() for p in user_projects }
            # wf_id, wf_uuid
            tuple_sort = []
            # get the wf and save the bind between the project uuid and the wf id
            for project in user_projects:
                tuple_sort.append( (project.get_workflow_id(), project.get_project_key().get_key()) )
            # reverse sort by wf id (automatically reverse date), then save the uuid/wfid
            for wf in sorted(tuple_sort, key=itemgetter(0), reverse=True):
                try:
                    curr_wf = self.wfmanager.get_workflow(wf[0])
                except:
                    #delete unbounded project (project in user data but workflow not found)
                    user_data = self.user_handler.get_user_by_workflow_id(wf[0])
                    user_project = user_data.get_project_by_workflow_id(wf[0])
                    user_data.delete_project_by_id(user_project.get_project_key())
                    self.user_handler.save_user_data(user_data)
                    continue
                status.append({
                    'wf_uuid':wf[1], #stringify project uuid
                    'wf_id':wf[0],   #stringify wf id
                    'wf':self._format_project_status(curr_wf, pnames[wf[0]])
                })
        return status

    def delete_project(self, project):
        """ Delete a project, in both user data and workflows

        Keywords arguments:
        project -- a UserProject object
        """
        if not isinstance(project, UserProject): raise TypeError
        deleted = False
        #get user data
        user_data = self.user_handler.get_user_data(project.get_user_key())
        if user_data is None: raise Exception('User not found')
        # then delete the workflow
        self.wfmanager.delete_workflow(int(project.get_workflow_id()))
        #delete project in user information
        deleted = user_data.delete_project_by_id(project.get_project_key())
        # save modification
        self.user_handler.save_user_data(user_data)
        return deleted

    def get_project_description(self, wf_name):
        """ Get the associated workflow description

        Keyword argument:
        wf_name -- the workflow name [PredictFasta/PredictGff3/Load]"""
        avail_wf_list = self.wfmanager.get_available_workflows()[0]
        desc = None
        for wftested in avail_wf_list:
            if wftested.name.lower() == wf_name.lower():
                desc = wftested.get_description()
                break
        return desc

    def check_projects(self):
        """ Check consistency of projects/workflows bindings
        v1.0: add user informations to ease targeting
        """
        _WF_ERROR = 'workflow'
        _PJ_ERROR = 'project'
        errors_on = { _PJ_ERROR: [], _WF_ERROR: [] }
        def _add_e(e_on, e_origin, e_msg):
            errors_on[e_on].append({ e_origin: e_msg })

        #prepare workflows
        try:
            workflows = self.wfmanager.get_workflows(use_cache=True)
            if not workflows: raise Exception('No workflows found')
        except Exception as e:
            utils.display_error_message(str(e))

        #prepare projects and workflows
        projects_check = self.user_handler.get_all_projects()
        workflows_check = { w.id : ''  for w in workflows }

        #check project ids in workflows list
        for p in projects_check:
            if p.get_workflow_id() not in workflows_check:
                _add_e(_PJ_ERROR, p.get_project_key().get_key(), 'Project {} associated workflow "{}" not found'.format(p.get_project_key().get_key(), p.get_workflow_id()))
        #check workflows ids in project list
        for wf, t in workflows_check.items():
            found = False
            for p in projects_check:
                if wf == p.get_workflow_id():
                    found = True
            if not found:
                _add_e(_WF_ERROR, wf, 'Workflow {} associated project not found'.format(wf))
        return errors_on

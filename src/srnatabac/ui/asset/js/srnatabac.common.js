/*******************************************************************************
 * Copyright notice
 *
 * (c) 2014 PF bioinformatique de Toulouse All rights reserved
 *
 * It is distributed under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ******************************************************************************/

/*
 * GENERATE THE DOCUMENTATION WITH JSDOC
 * http://usejsdoc.org
 */

/**
 * Redirects to a specific page, triggered by an event.
 * @param {event} event - The event that contains the destination.
 */
function redirectTo(event) {
	window.location.href = event.data.destination;
}

/*******************************************************************************
 * Copyright notice
 *
 * (c) 2014 PF bioinformatique de Toulouse All rights reserved
 *
 * It is distributed under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ******************************************************************************/

//module, Revealing Module Pattern
datatable = function(){
	//var
	var actorsList = null;
	var targetsList = null;
	var regionsList = null;
	var softwaresList = null;
	var datatableSelector = null;
	//
	var selector = {
		//initial lists
		initial_actors: '#initialActorsList',
		initial_targets: '#initialTargetsList',
		initial_regions: '#initialRegionsList',
		initial_softwares: '#initialSoftwaresList',
		//target filter obj
		target: {
			include_btn: '#includeAllMrnaBtn',
			exclude_btn: '#excludeAllMrnaBtn',
			include_list: '#includedMrnaList',
			exclude_list: '#excludedMrnaList',
			include_item_btn: "#includedMrnaList button[id^=mrna]",
			exclude_item_btn: "#excludedMrnaList button[id^=mrna]",
		},
		//actor filter obj
		actor: {
			include_btn: '#includeAllNcrnaBtn',
			exclude_btn: '#excludeAllNcrnaBtn',
			include_list: '#includedNcrnaList',
			exclude_list: '#excludedNcrnaList',
			include_item_btn: "#includedNcrnaList button[id^=ncrna]",
			exclude_item_btn: "#excludedNcrnaList button[id^=ncrna]",
		},
		//region filter obj
		region: {
			include_btn: '#includeAllRegionBtn',
			exclude_btn: '#excludeAllRegionBtn',
			include_list: '#includedRegionList',
			exclude_list: '#excludedRegionList',
			include_item_btn: "#includedRegionList button[id^=region]",
			exclude_item_btn: "#excludedRegionList button[id^=region]",
		},
		//software filter obj
		software: {
			include_btn: '#includeAllSoftwareBtn',
			exclude_btn: '#excludeAllSoftwareBtn',
			include_list: '#includedSoftwareList',
			exclude_list: '#excludedSoftwareList',
			include_item_btn: "#includedSoftwareList button[id^=software]",
			exclude_item_btn: "#excludedSoftwareList button[id^=software]",
		},
	};

	var evt = {
		// https://datatables.net/reference/event/
		dtcore: {
			columnSizing: 'column-sizing.dt', 				//fired when the column widths are recalculated.
			columnVisibility: 'column-visibility.dt',	//fired when the visibility of a column changes.
			destroy: 'destroy.dt', 										//fired when a table is destroyed.
			draw: 'draw.dt',													//fired once the table has completed a draw.
			error: 'error.dt',												//An error has occurred during DataTables' processing of data.
			init: 'init.dt',													//fired when DataTables has been fully initialised and data loaded.
			length: 'length.dt',											//fired when the page length is changed.
			order: 'order.dt',												//fired when the data contained in the table is ordered.
			page: 'page.dt',													//fired when the table's paging is updated.
			preInit: 'preInit.dt',										//triggered immediately before data load.
			preXhr: 'preXhr.dt',											//fired before an Ajax request is made
			processing: 'processing.dt',							//fired when DataTables is processing data
			search: 'search.dt',											//fired when the table is filtered.
			stateLoaded: 'stateLoaded.dt',						//fired once state has been loaded and applied.
			stateLoadParams: 'stateLoadParams.dt',		//fired when loading state from storage.
			stateSaveParams: 'stateSaveParams.dt',		//fired when saving table state information.
			xhr: 'xhr.dt',														//fired when an Ajax request is completed
		}

	};

	//---------------------
	//--METHODS
	//---------------------
	var dtapi = function(){
		return $( datatableSelector ).dataTable().api();
	};

	var reloadTable = function(){
		dtapi().draw();
	};

	//build initial obj lists
	function abstractStatusBuilder(prefix, sel){
		var as = [];
		var valist = $(sel).val().split(',');
		for(var v in valist){
			as.push({
				id: prefix + v,
				name: valist[v],
				is_included: true
			});
		}
		return as;
	};
	//get a specific id obj
	var abstractGetById = function(idsearched, arr){
		var as = null;
		for(var v in arr){
			if(arr[v].id === idsearched){
				as = arr[v];
			}
		}
		return as;
	};

	//selector for datatable init
	function abstractSelector(arr){
		var as = [];
		for(var v in arr){
			if(arr[v].is_included){
				as.push(arr[v].name);
			}
		}
		return as;
	};

	var getSelectedTargets = function(){
		return abstractSelector(targetsList);
	};
	var getSelectedActors = function(){
		return abstractSelector(actorsList);
	};
	var getSelectedRegions = function(){
		return abstractSelector(regionsList);
	};
	var getSelectedSoftwares = function(){
		return abstractSelector(softwaresList);
	};

	//init filter listener for 'move all' btn and single move btn
	var abstractFilterListener = function(filterObjSelector, name, objList){
		//exclude all btn
		$(filterObjSelector.exclude_btn).on('click', function(){
			for(var i in objList){
				objList[i].is_included = false;
			}
			abstractFilterReload(filterObjSelector.include_list, filterObjSelector.exclude_list, name, objList);
			reloadTable();
		});
		//include all btn
		$(filterObjSelector.include_btn).on('click', function(){
			for(var i in objList){
				objList[i].is_included = true;
			}
			abstractFilterReload(filterObjSelector.include_list, filterObjSelector.exclude_list, name, objList);
			reloadTable();
		});
		//exclude one item btn
		$("body").on("click", filterObjSelector.exclude_item_btn, function() {
			abstractGetById($(this).prop('id'), objList).is_included = true;
			abstractFilterReload(filterObjSelector.include_list, filterObjSelector.exclude_list, name, objList);
			reloadTable();
			return false;//avoid href jump to page top
		});
		//include one item btn
		$("body").on("click", filterObjSelector.include_item_btn, function() {
			abstractGetById($(this).prop('id'), objList).is_included = false;
			abstractFilterReload(filterObjSelector.include_list, filterObjSelector.exclude_list, name, objList);
			reloadTable();
			return false;//avoid href jump to page top
		});
	};

	//reload item list
	function abstractFilterReload(includeSelector, excludeSelector, classType, classList){
		//empty filters list
		$(includeSelector).html('');
		$(excludeSelector).html('');
		//fill them
		for(var s in classList){
			if(classList[s].is_included){
				$(includeSelector).append('<a href="#" class="' + classType + ' list-group-item clearfix">'
					+ classList[s].name
					+ '<span class="pull-right"><button id="' + classList[s].id + '" class="btn btn-danger">'
					+ '<i class="fa fa-minus" aria-hidden="true"></i></button></span></a>');
			}else{
				$(excludeSelector).append('<a href="#" class="' + classType + ' list-group-item clearfix">'
					+ classList[s].name
					+ '<span class="pull-right"><button id="' + classList[s].id + '" class="btn btn-info">'
					+ '<i class="fa fa-plus" aria-hidden="true"></i></button></span></a>');
			}
		}
	};

	var getAjaxUrl = function(){
		return "/get_datatable_data"
			+ "?user=" + userId
			+ "&project=" + projectId
			+ "&actors=" + escape(getSelectedActors())
			+ "&targets=" + escape(getSelectedTargets())
			+ "&regions=" + escape(getSelectedRegions())
			+ "&softwares=" + escape(getSelectedSoftwares())
	};

	//init
	var init = function(uid, pid, dtSelector){
		userId = uid;
		projectId = pid;
		datatableSelector = dtSelector;
		//
		actorsList = abstractStatusBuilder('ncrna', selector.initial_actors);
		targetsList = abstractStatusBuilder('mrna', selector.initial_targets);
		regionsList = abstractStatusBuilder('region', selector.initial_regions);
		softwaresList = abstractStatusBuilder('software', selector.initial_softwares);
		//load list
		abstractFilterReload(selector.target.include_list, selector.target.exclude_list, 'targets', targetsList);
		abstractFilterReload(selector.actor.include_list, selector.actor.exclude_list, 'actors', actorsList);
		abstractFilterReload(selector.region.include_list, selector.region.exclude_list, 'regions', regionsList);
		abstractFilterReload(selector.software.include_list, selector.software.exclude_list, 'softwares', softwaresList);
		//init listener ('move all' btn listener + one item btn listener)
		abstractFilterListener(selector.target, 'targets', targetsList);
		abstractFilterListener(selector.actor, 'actors', actorsList);
		abstractFilterListener(selector.region, 'regions', regionsList);
		abstractFilterListener(selector.software, 'softwares', softwaresList);
		//dt options
		$(datatableSelector).dataTable({
			processing : true,
			serverSide : true,
			ajax : {
				url: getAjaxUrl(),
				type : "POST",
				dataType : "json"
			},
			dom: 'lBfrtip',
			pagingType: 'full_numbers',
			deferRender : true,	// optimize render on init
			buttons: [ 'csv', {
            extend: 'print',
            title: $('h1').text()
        }, {
            text: 'Options',
						className: 'advFilterBtn'
        } ],
			language : { // change the dataTable messages
				emptyTable : "No interaction found",
				info : "Showing _START_ to _END_ of _TOTAL_ entries",
				infoEmpty : "Showing 0 to 0 of 0 entries",
				infoFiltered : "(filtered from _MAX_ total entries)",
				infoPostFix : "",
				thousands : ",",
				lengthMenu : "Show _MENU_ entries",
				loadingRecords : "Data loading...",
				processing : "Data loading...",
				search : "mRNA search:",
				zeroRecords : "No interaction found",
				paginate : {
					first : "First",
					last : "Last",
					next : "Next",
					previous : "Previous"
				},
				aria : {
					sortAscending : ": activate to sort column ascending",
					sortDescending : ": activate to sort column descending"
				}
			},
			renderer : "bootstrap",
			scrollX : true,
			columns : [
				{
					data : 'id',
					name : 'id',
					visible : true,
					searchable : true,
					orderable : true,
					className: "interaction_modal",
					render: function ( data, type, full, meta ) {
						return type === 'display' ? '<a href="#">' + data + '</a>' : data;
					}
				}, {
					data : 'mrna_id',
					name : 'mrna_id',
					visible : true,
					searchable : true,
					orderable : true
				}, {
					data : 'ncrna_id',
					name : 'ncrna_id',
					visible : true,
					searchable : true,
					orderable : true,
					className: "ncrna_view",
					render: function ( data, type, full, meta ) {
						return type === 'display'
							? '<a href="/forna?user=' + userId + '&project=' + projectId + '&interaction=' + full.id + '">' + data + '</a>'
							: data;
						}
				}, {
					data : 'region_id',
					name : 'region_id',
					visible : true,
					searchable : true,
					orderable : true
				}, {
					data : 'details.software',
					name : 'software',
					visible : true,
					searchable : true,
					orderable : true
				}, {
					data : 'details.overlap_length',
					name : 'overlap_length',
					visible : true,
					searchable : false,
					orderable : true
				}, {
					data : 'details.overlap_start',
					name : 'overlap_start',
					visible : false,
					searchable : false,
					orderable : true,
					render : function(data, type, full, meta) {
						return (type === 'display') ? ((data == false) ? 'No' : 'Yes') : data;
					}
				}, {
					data : 'details.match_count',
					name : 'match_count',
					visible : true,
					searchable : false,
					orderable : true
				}, {
					data : 'details.match_successive',
					name : 'match_successive',
					visible : true,
					searchable : false,
					orderable : true
				}, {
					data : 'details.energy_software',
					name : 'energy_software',
					visible : false,
					searchable : false,
					orderable : true
				}, {
					data : 'details.energy_duplex',
					name : 'energy_duplex',
					visible : true,
					searchable : false,
					orderable : true
				}, {
					data : 'details.mrna_begin',
					name : 'mrna_begin',
					visible : true,
					searchable : false,
					orderable : true
				}, {
					data : 'details.mrna_end',
					name : 'mrna_end',
					visible : true,
					searchable : false,
					orderable : true
				}, {
					data : 'details.ncrna_begin',
					name : 'ncrna_begin',
					visible : true,
					searchable : false,
					orderable : true
				}, {
					data : 'details.ncrna_end',
					name : 'ncrna_end',
					visible : true,
					searchable : false,
					orderable : true
				}, {
					data : 'details.pos_to_gene_start',
					name : 'pos_to_gene_start',
					visible : true,
					searchable : false,
					orderable : true
				}, {
					data : 'details.pos_to_gene_stop',
					name : 'pos_to_gene_stop',
					visible : false,
					searchable : false,
					orderable : true
				}, {
					data : 'details.pair_pct_GC',
					name : 'pair_pct_GC',
					visible : false,
					searchable : false,
					orderable : true,
					render : function(data, type, full, meta) {
						return (type === 'display') ? (data * 100).toFixed(2) + "%" : "-%";
					}
				}, {
					data : 'details.pair_pct_AU',
					name : 'pair_pct_AU',
					visible : false,
					searchable : false,
					orderable : true,
					render : function(data, type, full, meta) {
						return (type === 'display') ? (data * 100).toFixed(2) + "%" : "-%";
					}
				}, {
					data : 'details.pair_pct_GU',
					name : 'pair_pct_GU',
					visible : false,
					searchable : false,
					orderable : true,
					render : function(data, type, full, meta) {
						return (type === 'display') ? (data * 100).toFixed(2) + "%" : "-%";
					}
				}
			]
		});

		$(datatableSelector)
			// EVENT: before sending to server, change server param
			.on(evt.dtcore.preXhr, function(e, settings, data) {
				console.log();
				console.log('[dt] ---- start process');
				console.log('[dt] pre-xhr callback: updating url params...');
				settings.ajax.url = getAjaxUrl();
				console.log('[dt] pre-xhr callback: url updated');
			})
			// EVENT: after receiving data from server
			.on(evt.dtcore.xhr, function(e, settings, json) {
				// console.log(json);
				console.log('[dt] xhr callback, data received: filtered records='
					+ json["recordsFiltered"] + ', total records=' + json["recordsTotal"]);

			})
			// EVENT: on the first init
			.on(evt.dtcore.init, function(e, settings, json){
				console.log('[dt] first initialization complete');
				//add icons to datatable buttons plugin
				$('.buttons-csv').html('<span class="ui-button-text">' +
					'<i class="fa fa-download fa-fw" aria-hidden="true"></i>&nbsp;CSV' +
					'</span>');
				$('.buttons-print').html('<span class="ui-button-text">' +
					'<i class="fa fa-print fa-fw" aria-hidden="true"></i>&nbsp;Print' +
					'</span>');
				//add the header button into the datatable, load collapse link for options
				$('.advFilterBtn').html('<span id="collapseAdvancedFiltersBtn" data-toggle="collapse" ' +
					'aria-expanded="false" aria-controls="collapseAdvancedFilters" ' +
					'aria-pressed="false" autocomplete="off" href="#collapseAdvancedFilters">' +
					'<i class="fa fa-cogs fa-fw" aria-hidden="true"></i>&nbsp;Options' +
					'</span>');
			})
			// EVENT: on error during dataTable processing
			.on(evt.dtcore.error, function ( e, settings, techNote, message ) {
		    console.log('[dt] an error has been reported: ', message );
	    })
			// EVENT: after the end of the dt draw
			.on(evt.dtcore.draw, function ( e, settings ) {
				console.log('[dt] draw callback: draw updated');
				console.log('[dt] ---- end process');
			});

		$(datatableSelector + ' tbody').on('click', 'td', function () {
			// cell clicked
			var api = dtapi();
			var cell = api.cell( this );
			var row_data = api.row( cell.index().row ).data();
			// is cell interaction
			if( $( cell.node() ).hasClass('interaction_modal') === true ){
				$.ajax({
						url: "/get_datatable_interaction?user=" + userId + "&project=" + projectId + "&interaction=" + row_data.id,
				    timeout: 40000,
				    success: function(data) {
				    	$( data ).modal('show').ready(function(){
				    		// re-activate tooltips
				    		$("body").tooltip({ selector: '[data-toggle=tooltip]' });
				    	});
				    }
				});
			}
		});

		//init: activate column visibility
		$('#tabOptColumns input[type="checkbox"]').each(function() {
				var isColVisible = $(datatableSelector).DataTable().column(
						parseInt($(this).attr('data-column'))).visible();
				$(this).prop("checked", isColVisible);
			});

		$('#tabOptColumns input[type="checkbox"]').on('click', function() {
			//toogle visibility
			var api = dtapi();
			var colNum = parseInt($(this).attr('data-column'));
			var colVisibility = api.column(colNum).visible();
			api.column(colNum).visible(!colVisibility);
			api.columns.adjust();
		});

		$('#modal-base-interaction .modal-footer-cancel').on('click', function() {
			$('#modal-base-interaction').modal('close');
		});

		$('.dt-button').css('margin-left', '10px');
	};
	//
	return {
		init:init
	};
}();

// database = function(){
//
// 	var dbAutoOpen = null;
// 	var dbName = null;
// 	var db = null;
// 	var dbEvt = {
// 		ready: 'ready',									// db is ready
// 		blocked: 'blocked',							// blokced request
// 		populate: 'populate',						// first client-side init populate request
// 		error: 'error',									// unhandled error
// 		versionchange: 'versionchange',	// another indexedDB database instance with a newer version of the database needs to upgrade the database
// 		promise: {
// 			error: 'error'								// global error handler for promises
// 		},
// 		transaction: {
// 			abort: 'abort',								// when transaction is aborted
// 			complete: 'complete',					// when transaction completed
// 			error: 'error'								// when transaction failed
// 		}
// 	};
// 	var init = function(){
// 		dbAutoOpen = true;
// 		dbName = 'interactions';
// 		db = new Dexie(dbName, {autoOpen: dbAutoOpen});
//     db.version(1).stores({
// 			interaction: "id, mrna_id, ncrna_id, region_id, "
// 				+ "details.mrna_begin, details.mrna_end, details.mrna_seq, "
// 				+ "details.ncrna_begin, details.ncrna_end, details.ncrna_seq, "
// 				+ "details.software, details.symbolism, "
// 				+ "details.energy_software, details.energy_duplex, "
// 				+ "details.overlap_length, details.overlap_start, details.overlap_stop, "
// 				+ "details.pos_to_gene_start, details.pos_to_gene_stop, "
// 				+ "details.match_count, details.match_successive, "
// 				+ "details.pair_pct_AU, details.pair_pct_GC, details.pair_pct_GU, "
// 				+ "mrna.id, mrna.start, mrna.stop, mrna.strand, mrna.sequence, mrna.gene_start, mrna.gene_stop, "
// 				+ "ncrna.id, ncrna.start, ncrna.stop, ncrna.strand, ncrna.sequence",
//       index: "id, mrna_id, ncrna_id, region_id, software",
//     });
//
// 		//listeners
// 		db.on(dbEvt.blocked, function() {
// 	    console.log("[db] upgrading was blocked by another window. " +
// 				"Please close down any other tabs or windows that has this page open");
// 			})
// 			.on(dbEvt.ready, function() {
// 				console.log("[db] db ready");
// 			})
// 			.on(dbEvt.error, function (error) {
// 				console.log("[db] unhandled error");
// 			})
// 			.on(dbEvt.populate, function () {
// 				console.log("[db] client-side db not found, creating...");
// 			})
// 			.on("versionchange", function(event) {
// 				if (!confirm("Another page tries to upgrade the database to version " + event.newVersion + ". Accept?")) {
// 					return false;
// 				}
// 			});
//
// 		// db.index.hook('creating', function (primKey, obj, transaction) {
// 		//   // called whenever an object is being added into the database
// 	  //   // You may do additional database operations using given transaction object.
// 	  //   // You may also modify given obj
// 	  //   // You may set this.onsuccess = function (primKey){}. Called when autoincremented key is known.
// 	  //   // You may set this.onerror = callback if create operation fails.
// 	  //   // If returning any value other than undefined, the returned value will be used as primary key
// 		// });
// 		//
// 		// db.index.hook('updating', function (modifications, primKey, obj, transaction) {
// 		//   // called whenever an existing database object is about to be updated
// 	  //   // You may use transaction to do additional database operations.
// 	  //   // You may not do any modifications on any of the given arguments.
// 	  //   // You may set this.onsuccess = callback when update operation completes.
// 	  //   // You may set this.onerror = callback if update operation fails.
// 	  //   // If you want to make additional modifications, return another modifications object
// 	  //   // containing the additional or overridden modifications to make. Any returned
// 	  //   // object will be merged to the given modifications object.
// 		// });
// 		//
// 		// db.index.hook('reading', function (obj) {
// 		//  // called whenever an object is about to be returned from database
//     // 	// You may return another object or modify existing object.
// 		// });
// 		//
// 		// db.index.hook('deleting', function (primKey, obj, transaction) {
// 		//   // called whenever an object is about to be deleted from the database
// 	  //   // You may do additional database operations using given transaction object.
// 	  //   // You may set this.onsuccess = callback when delete operation completes.
// 	  //   // You may set this.onerror = callback if delete operation fails.
// 	  //   // Any modification to obj is ignored.
// 	  //   // Any return value is ignored.
// 	  //   // throwing exception will make the db operation fail.
// 		// });
//
// 		//before the user leave the page, unload its data
// 		// $(window).on('beforeunload', function(){
// 		//   console.log("Wild user wants to leave results page!");
// 		// });
// 	};
//
// 	var isOpen = function(){
// 		return (dbAutoOpen) ? true : db.isOpen();
// 	};
//
// 	var countStore = function(store){
// 		return db.table(store).count(); //promise
// 	};
//
// 	var deleteDb = function(){
// 		return db.delete(); //promise
// 	};
//
// 	var addInteraction = function(i){
// 		return db.interaction.add(i); //promise
// 	};
//
// 	var getInteraction = function(iid){
// 		return db.interaction.get(iid); //promise
// 	};
//
// 	var getAllIndexed = function() {
// 	  return db.index.toArray();
// 	};
//
// 	return {
// 		init: init,
// 		isOpen: isOpen,
// 		count: countStore,
// 		delete: deleteDb,
// 		addInteraction: addInteraction,
// 		getInteraction: getInteraction,
// 		getAllIndexed: getAllIndexed
// 	};
//
// }();

$(function() {
	//var
	var project_id = window.location.search.split('&project=')[1];
	var user_id = window.location.search.split('&project=')[0].split('user=')[1];
	//tooltips
	$('[data-toggle="tooltip"]').tooltip();

	$('#display_outputs').on('click',function(){
		window.location="/outputs?user=" + user_id + "&project="+ project_id;
	});
	//load modules
	datatable.init(user_id, project_id, '#table-data');
});

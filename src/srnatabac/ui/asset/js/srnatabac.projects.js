/*******************************************************************************
 * Copyright notice
 *
 * (c) 2014 PF bioinformatique de Toulouse All rights reserved
 *
 * It is distributed under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ******************************************************************************/

//module project, Revealing Module Pattern
project = function(){
	//var
  var user_uuid = window.location.search.split('user=')[1];
	var projects = null;
	var selector = {
		modal: '#modal-base-confirmation',
		modal_content: '#confirmation-content',
		modal_body: '#modal-base-confirmation .modal-body',
		modal_submit_btn: '#modal-base-confirmation .modal-footer-submit',
		modal_cancel_btn: '#modal-base-confirmation .modal-footer-cancel'
	};
	var showModal = function(){
		$(selector.modal).modal('show');
	};
	var hideModal = function(){
		$(selector.modal).modal('hide');
	};
	var load = function(newloc){
		window.location=newloc;
	};
	var loadResults = function(projectid){
		load("/result?user=" + user_uuid + "&project=" + projectid);
	};
	var loadOutputs = function(projectid){
		load("/outputs?user=" + user_uuid + "&project=" + projectid);
	};
	//init
  var init = function(){
		//
		$('#delete_failed').prop('disabled', (getFailedOrAbortedProjects().length===0)?'disabled':'');
		// activate tooltips
		$('[data-toggle="tooltip"]').tooltip();
		//listeners
		$('button[id^=delete_wf_]').on('click', function() {
			projects = $(this).prop('id').split('delete_wf_')[1];
			$('#confirmation-content').html('Are you sure to delete this project?\nAll data will be lost.');
			showModal();
		});
		$('#delete_failed').on('click', function() {
			projects = getFailedOrAbortedProjects();
			$(selector.modal_content).html('Are you sure to delete all (' + projects.length + ') failed/aborted project?');
			showModal();
		});
		$('button[id^=results_]').on('click', function(){
			loadResults($(this).prop('id').split('results_')[1]);
		});
		$('button[id^=outputs_]').on('click', function(){
			loadOutputs($(this).prop('id').split('outputs_')[1]);
		});
		// process confirmation modal
		$(selector.modal_cancel_btn).on('click', hideModal);
		//submit
		$(selector.modal_submit_btn).on('click', function() {
			//disable submit button and display animation
			$(selector.modal_submit_btn).prop('disabled', 'disabled');
			$(selector.modal_body).html('<div class="alert alert-info" role="alert"><i class="fa fa-cog fa-spin fa-2x fa-fw"></i>Deleting...</div>');
			$.ajax({
				url: "/delete_projects?user=" + user_uuid + "&projects=" + projects,
				timeout: 20000,
				error: function(xhr, ajaxOptions, thrownError) {
					$(selector.modal_body).html('<div class="alert alert-danger" role="alert">Something wrong happened on the server... Please, try again later.</div>')
				},
				success: function(data) {
					if(typeof data === 'object' && Object.keys(data).length > 0){
						$(selector.modal_body).html('<div class="alert alert-success" role="alert">Success!</div>');
						setTimeout(function(){
							window.location = window.location.search;
						}, 1000);
					}else{
						$(selector.modal_body).html('<div class="alert alert-danger" role="alert">Something wrong happened on the server... Please, try again later.</div>')
					}
				},
				complete: function(jqXHR, textStatus){
					projects = null;
				}
			});
		});
	};
	//
  var getFailedOrAbortedProjects = function(){
		var t = [];
		$('tr').each(function(){
			var cid = $(this).prop('id');
			if(typeof cid==='string' && cid!==''){
				var tr_status = $($(this).children()[1]).text();
				if(tr_status==='aborted' || tr_status==='failed'){
					t.push($($($(this).children()[4]).children()[0]).prop('id').split('delete_wf_')[1]);
				}
			}
		});
		return t;
  };
  return{
		init:init,
		getFailedOrAbortedProjects:getFailedOrAbortedProjects
	}
}();

//init after dom ready
$(function() {
	project.init();
});

/*******************************************************************************
 * Copyright notice
 *
 * (c) 2014 PF bioinformatique de Toulouse All rights reserved
 *
 * It is distributed under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ******************************************************************************/
$(function() {
	// the affix navbar
	$('body').scrollspy({
		target: '.bs-docs-sidebar',
		offset: 150
	});

	// visual helper for cli-mode arguments
	function highlightLine(wfclass){ $(wfclass).addClass('wfhighlight'); }
	function removeHighlighting(wfclass){ $(wfclass).removeClass('wfhighlight'); }

	$('.wfload')
		.on('mouseover', function(){ highlightLine('.wfload'); })
		.on('mouseleave', function(){ removeHighlighting('.wfload'); });

	$('.wfpfasta')
		.on('mouseover', function(){ highlightLine('.wfpfasta'); })
		.on('mouseleave', function(){ removeHighlighting('.wfpfasta'); });

	$('.wfpgff3')
		.on('mouseover', function(){ highlightLine('.wfpgff3'); })
		.on('mouseleave', function(){ removeHighlighting('.wfpgff3'); });

	$('.wfall').on('mouseover', function(){
		removeHighlighting('.wfpgff3');
		removeHighlighting('.wfpfasta');
		removeHighlighting('.wfload');
	});
});

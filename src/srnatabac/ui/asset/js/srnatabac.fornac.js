/*******************************************************************************
 * Copyright notice
 *
 * (c) 2014 PF bioinformatique de Toulouse All rights reserved
 *
 * It is distributed under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ******************************************************************************/
//module fornac, Revealing Module Pattern

var saveModule = function(){
  var doctype = '<?xml version="1.0" standalone="no"?>'
    +'<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">';

  var inlineImages = function(callback) {
    var images = document.querySelectorAll('svg image');
    var left = images.length;
    if (left == 0) {
      callback();
    }
    for (var i = 0; i < images.length; i++) {
      (function(image) {
        if (image.getAttribute('xlink:href')) {
          var href = image.getAttribute('xlink:href').value;
          if (/^http/.test(href) && !(new RegExp('^' + window.location.host).test(href))) {
            throw new Error("Cannot render embedded images linking to external hosts.");
          }
        }
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');
        var img = new Image();
        img.src = image.getAttribute('xlink:href');
        img.onload = function() {
          canvas.width = img.width;
          canvas.height = img.height;
          ctx.drawImage(img, 0, 0);
          image.setAttribute('xlink:href', canvas.toDataURL('image/png'));
          left--;
          if (left == 0) {
            callback();
          }
        }
      })(images[i]);
    }
  };

  var styles = function(dom) {
    var css = "";
    var sheets = document.styleSheets;
    for (var i = 0; i < sheets.length; i++) {
      var rules = sheets[i].cssRules;
      if (rules != null) {
        for (var j = 0; j < rules.length; j++) {
          var rule = rules[j];
          if (typeof(rule.style) != "undefined") {
            css += rule.selectorText + " { " + rule.style.cssText + " }\n";
          }
        }
      }
    };

    var s = document.createElement('style');
    s.setAttribute('type', 'text/css');
    s.innerHTML = "<![CDATA[\n" + css + "\n]]>";

    var defs = document.createElement('defs');
    defs.appendChild(s);
    return defs;
  };

  var svgAsDataUri = function(el, scaleFactor, cb) {
    scaleFactor = scaleFactor || 1;
    inlineImages(function() {
      var outer = document.createElement("div");
      var clone = el.cloneNode(true);
      var width = parseInt(
        clone.getAttribute('width')
          || clone.style.width
          || getComputedStyle(el).getPropertyValue('width')
      );
      var height = parseInt(
        clone.getAttribute('height')
          || clone.style.height
          || getComputedStyle(el).getPropertyValue('height')
      );

      var xmlns = "http://www.w3.org/2000/xmlns/";

      clone.setAttribute("version", "1.1");
      clone.setAttributeNS(xmlns, "xmlns", "http://www.w3.org/2000/svg");
      clone.setAttributeNS(xmlns, "xmlns:xlink", "http://www.w3.org/1999/xlink");
      clone.setAttribute("width", width * scaleFactor);
      clone.setAttribute("height", height * scaleFactor);
      clone.setAttribute("viewBox", "0 0 " + width + " " + height);
      outer.appendChild(clone);

      clone.insertBefore(styles(clone), clone.firstChild);

      var svg = doctype + outer.innerHTML;
      var uri = 'data:image/svg+xml;base64,' + window.btoa(unescape(encodeURIComponent(svg)));
      if (cb) { cb(uri); }
    });
  };

  var saveSvgAsPng = function(el, name, scaleFactor) {
    return svgAsDataUri(document.getElementById(el), scaleFactor, function(uri) {
      var image = new Image();
      image.src = uri;
      image.onload = function() {
        var canvas = document.createElement('canvas');
        canvas.width = image.width;
        canvas.height = image.height;
        var context = canvas.getContext('2d');
        context.drawImage(image, 0, 0);

        var a = document.createElement('a');
        a.download = name;
        a.href = canvas.toDataURL('image/png');
        document.body.appendChild(a);
        a.click();
      }
    });
  };

  var saveSvg = function(container, name){
    var svg_clone = $('#'+container).clone();
    var to_remove = $('[link_type=fake],.brush,.outline_node', svg_clone).toArray();
    var parents = to_remove.map(function(d) {
        return d.parentNode;
    });
    for (var i = 0; i < to_remove.length; i++) {
      parents[i].removeChild(to_remove[i]);
    }
    var svg = svg_clone.get(0);
    //get svg source.
    var serializer = new XMLSerializer();
    var source = serializer.serializeToString(svg);
    //add name spaces.
    if(!source.match(/^<svg[^>]+xmlns="http\:\/\/www\.w3\.org\/2000\/svg"/)){
        source = source.replace(/^<svg/, '<svg xmlns="http://www.w3.org/2000/svg"');
    }
    if(!source.match(/^<svg[^>]+"http\:\/\/www\.w3\.org\/1999\/xlink"/)){
        source = source.replace(/^<svg/, '<svg xmlns:xlink="http://www.w3.org/1999/xlink"');
    }
    //add xml declaration
    source = '<?xml version="1.0" standalone="no"?>\r\n' + source;
    // use FileSave to get a downloadable SVG File
    var file = new Blob([source], {type: "data:image/svg+xml;charset=utf-8"});
    saveAs(file, name);
  };

  return {
    saveSvg: saveSvg,
    saveSvgAsPng: saveSvgAsPng
  };
}();


fornacDisplay = function(){

  var initSize = null;
  var options = null;
  var containerElement = null;
  var container = null;
  var saveMod = null;

  var changeColor = function(event){
    if(typeof event.data.color === 'undefined') {
      throw new Exception('Undefined color event');
    }
    var c = event.data.color;
    if(c !== 'sequence' && c !== 'structure' && c !== 'positions') {
      throw new Exception('Color must be "sequence", "structure" or "positions"');
    }
    container.changeColorScheme(c);
  };

  var requestFullscreen = function(){
    if (screenfull.enabled) {
      screenfull.request($(containerElement)[0]);
    }
  };

  var center = function(){
    container.centerView();
  };

  var toggleNumbering = function(){
    container.displayNumbering(!container.displayParameters.displayNumbering);
  };

  var toggleNodeOutline = function(){
    container.displayNodeOutline(!container.displayParameters.displayNodeOutline);
  };

  var toggleNodeLabel = function(){
    container.displayNodeLabel(!container.displayParameters.displayNodeLabel);
  };

  var toggleAnimation = function(){
    (container.animation) ? container.stopAnimation() : container.startAnimation();
  };

  var getWindowDimensions = function(){
    return {
      width: window.innerWidth
        || document.documentElement.clientWidth
        || document.body.clientWidth,
      height: window.innerHeight
        || document.documentElement.clientHeight
        || document.body.clientHeight
      };
  };

  var init = function(sequence, structure, interactionStart, interactionStop, htmlContainer){
    containerElement = htmlContainer;
    initSize = [$(htmlContainer).width(), 0.55*getWindowDimensions().height];
    options = {
      'sequence': sequence,
      'structure': structure
    };
    saveMod = saveModule;

    //init container for forna
    container = new fornac.FornaContainer(containerElement, {
      'applyForce': true,
      'allowPanningAndZooming': true,
      'displayAllLinks': true,
      'labelInterval': 10,
      'initialSize': initSize,
      //added for sRNA-TaBac
      'interactionStart': interactionStart,
      'interactionStop': interactionStop
      // 'transitionDuration': 300,
    });
    container.stopAnimation();
    container.displayNodeLabel(false);
    var rnaJSON = container.addRNA(options.structure, options);

    //hide fullscreen
    $(document).on(screenfull.raw.fullscreenchange, function(){
      // console.log(screenfull.isFullscreen ? 'Fullscreen [ON]' : 'Fullscreen [OFF]');
      if(!screenfull.isFullscreen){
        container.setSize(initSize);
      }
    }).on(screenfull.raw.fullscreenerror, function (event) {
      console.error('Failed to enable fullscreen', event);
    });

    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
  };

  var screenSize = function(){
    return [screen.width, screen.height];
  };

  var saveSVG = function(){
    saveMod.saveSvg('plotting-area', 'rna.svg');
  };

  var savePNG = function(){
    saveMod.saveSvgAsPng('plotting-area', 'rna.png', 4);
  };

  return {
    init: init,
    changeColor: changeColor,
    requestFullscreen:requestFullscreen,
    center:center,
    toggleNumbering:toggleNumbering,
    toggleNodeOutline:toggleNodeOutline,
    toggleNodeLabel:toggleNodeLabel,
    toggleAnimation:toggleAnimation,
    savePNG:savePNG,
    saveSVG:saveSVG
  };
}();


$(function(){

  fornacDisplay.init(
    $('#ncrna-sequence').val(),
    $('#ncrna-structure').val(),
    $('#interaction-start').val(),
    $('#interaction-stop').val(),
    '#rna_ss'
  );

  //copy text to clkipboard using temp textarea
  var copyTextToClipboard = function(text) {
    var textArea = document.createElement("textarea");
    // Avoid flash of white box if rendered for any reason
    textArea.style.background = 'transparent';
    textArea.value = text;
    document.body.appendChild(textArea);
    textArea.select();
    try {
      var successful = document.execCommand('copy');
    } catch (err) {
      console.log('Oops, unable to copy');
      alert('Oops, unable to copy');
    }
    document.body.removeChild(textArea);
  }

  $('#seqBox').on('click', function() {
    copyTextToClipboard($('#seqBoxText').val());
  });
  $('#structBox').on('click', function() {
    copyTextToClipboard($('#structBoxText').val());
  });

  // fornac events
  $('#centerView').on('click', fornacDisplay.center);
  $('#fullscreenView').on('click',fornacDisplay.requestFullscreen);
  $('#numberingOpt').on('click', fornacDisplay.toggleNumbering);
  $('#nodeLabelOpt').on('click', fornacDisplay.toggleNodeLabel);
  $('#nodeOutlineOpt').on('click', fornacDisplay.toggleNodeOutline);
  $('#animateOpt').on('click', fornacDisplay.toggleAnimation);
  $('#colorSequence').on('click', {color: 'sequence'}, fornacDisplay.changeColor);
  $('#colorStructure').on('click', {color: 'structure'}, fornacDisplay.changeColor);
  $('#colorPosition').on('click', {color: 'positions'}, fornacDisplay.changeColor);
  $('#saveSVG').on('click', fornacDisplay.saveSVG);
  $('#savePNG').on('click', fornacDisplay.savePNG);
});

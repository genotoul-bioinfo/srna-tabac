/*******************************************************************************
 * Copyright notice
 *
 * (c) 2014 PF bioinformatique de Toulouse All rights reserved
 *
 * It is distributed under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ******************************************************************************/

$(function(){

	var project_uuid = window.location.search.split('&project=')[1];
	var user_uuid = window.location.search.split('&project=')[0].split('user=')[1];

	var outputs = $('#outputs').wfoutputs({
		serverURL: "http://" + window.location.hostname + ":" + window.location.port,
		// workflowID: $('#project_id').val(),
		workflowID: $('#project_id').val() + "&user=" + user_uuid + "&project=" + project_uuid,
		logFile: [".stderr", ".stdout", ".log"],
		template: ['<div style="float:right;margin-bottom:14px">',
		           '  <button id="jflow_log_on"  class="btn btn-default btn-xs" type="button"><span class="glyphicon glyphicon-eye-open"></span> View log and error files</button>',
		           '  <button id="jflow_log_off" style="display:none" class="btn btn-default btn-xs" type="button"><span class="glyphicon glyphicon-eye-close"></span> Mask log and error files</button>',
		           '</div>',
		           '<div style="clear:both;">',

		           // Prediction files
		           '  <dl "style=margin-bottom:15px">',
		           '    <div style="clear:both"></div>',
		           '    <dt style="margin-top:15px;background-color: #eee">',
		           '      <span class="glyphicon glyphicon-play" style="color:white;left:-3px"></span>',
		           '      Prediction files',
		           '    </dt>',
		           '  {{each(component_name, files) data}}',
		           '    {{each(file_name, href) files}}',
		           '      {{if file_name.endsWith(".pgz") && !file_name.endsWith("_info.pgz") || file_name.endsWith(".gff3") || file_name.endsWith(".txt") || file_name.endsWith(".csv") && !file_name.endsWith("_tmp.txt")}}',
		           '        <dd class="output-file" style="float:left;margin-left:20px;width:265px;overflow:hidden;white-space: nowrap;text-overflow:ellipsis;">',
		           '          <span class="glyphicon glyphicon-file"></span>',
		           '          <a href="${href.url}" download>${file_name}</a>',
		           '          <span style="color:grey">| ${href.size}</span>',
		           '        </dd>',
		           '      {{/if}}',
		           '    {{/each}}',
		           '  {{/each}}',
		           '    <div style="clear:both"></div>',

		           // Logs and errors
		           '    <dt style="margin-top:15px;background-color: #eee">',
		           '      <span class="glyphicon glyphicon-play" style="color:white;left:-3px"></span>',
		           '      Log and error files',
		           '    </dt>',
		           '  {{each(component_name, files) data}}',
		           '    {{each(file_name, href) files}}',
		           '      {{if file_name.endsWith(".stderr") || file_name.endsWith(".stdout") || file_name.endsWith(".log") }}',
		           '        <dd class="output-file" style="float:left;margin-left:20px;width:265px;overflow:hidden;white-space: nowrap;text-overflow:ellipsis;">',
		           '          <span class="glyphicon glyphicon-file"></span>',
		           '          <a href="${href.url}" download>${file_name}</a>',
		           '          <span style="color:grey">| ${href.size}</span>',
		           '        </dd>',
		           '      {{/if}}',
		           '    {{/each}}',
		           '  {{/each}}',
		           '  </dl>',
		           '</div>'].join('\n')
	});

});

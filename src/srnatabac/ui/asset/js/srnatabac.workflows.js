/*******************************************************************************
 * Copyright notice
 *
 * (c) 2016 PF bioinformatique de Toulouse All rights reserved
 *
 * It is distributed under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ******************************************************************************/

//module workflows, Revealing Module Pattern
workflows = function(){
  var server_url = "http://" + window.location.hostname + ":" + window.location.port;
  var check = null;
  //
  var primes_default = {
    active: 30,
    deactive: 0
  }
  //selectors
	var selector = {
		form: '#setAndRunForm',
    wf_name: '#wfname',
    wf_project_name: '#project_name',
    wf_description: '#wfdescription',
    wf_run_btn: '#wfform_run_btn',
    wf_reset_btn: '#wfform_reset_btn',
    sample: '#collapseSample',
    checkboxes: 'input[type="checkbox"]',
    b5:'#before_5utr',
    a5:'#after_5utr',
    b3:'#before_3utr',
    a3:'#after_3utr',
    cds: '#predict_cds',
    five_prime: '#predict_5utr',
    three_prime: '#predict_3utr'
	};
  var evt = {
    form_uploading: 'uploading.wfform',
    form_loaded: 'loaded.wfform',
    form_ready: 'ready.wfform',
    form_run: 'run.wfform',
    collapse_show: 'show.bs.collapse',
    collapse_hide: 'hide.bs.collapse',
    click: 'click'
  };
  //methods
  var userWorkspace = function(wfid){
    return server_url + '/get_user_workspace?workflow_id=' + wfid;
  };
  var showDescription = function(){
    $(selector.wf_description).show();
  };
  var hideDescription = function(){
    $(selector.wf_description).hide();
  };
  var formIsGff3 = function(){
    return $(selector.b5).length > 0;
  };
  var checkAll = function(){
    $(selector.checkboxes).prop('checked', true);
  };
  var uncheckAll = function(){
    $(selector.checkboxes).prop('checked', false);
  };

  var wantFivePrime = function(){
    return $(selector.five_prime).prop('checked');
  };
  var wantThreePrime = function(){
    return $(selector.three_prime).prop('checked');
  };
  var enablePrimes = function(i,b){
    $(i).prop('disabled', !b);
    $(i).val((b) ? primes_default.active : primes_default.deactive);
  };
  var enableFivePrime = function(b){
    enablePrimes(selector.b5, b);
    enablePrimes(selector.a5, b);
  };
  var enableThreePrime = function(b){
    enablePrimes(selector.b3, b);
    enablePrimes(selector.a3, b);
  };
  var activateFivePrime = function(){
    $(selector.five_prime).prop('checked', true);
    enableFivePrime(true);
  };
  var activateThreePrime = function(){
    $(selector.three_prime).prop('checked', true);
    enableThreePrime(true);
  };
  var deactivateFivePrime = function(){
    $(selector.five_prime).prop('checked', false);
    enableFivePrime(false);
  };
  var deactivateThreePrime = function(){
    $(selector.three_prime).prop('checked', false);
    enableThreePrime(false);
  };
  var deactivatePrimes = function(){
    deactivateThreePrime();
    deactivateFivePrime();
  };


  var init = function(){
    // Generates the load form.
  	$(selector.form).wfform({
  		serverURL: server_url,
  		workflowClass : $(selector.wf_name).val()
  	});
    $(selector.form).on(evt.form_uploading, hideDescription);
    $(selector.form).on(evt.form_loaded, function(){
      showDescription();
      //trigger an event when generated html is ready
      if (check == null) {
        check = setInterval(function () {
          if( $(selector.form).html().indexOf('fieldset') ){
            $(selector.form).trigger(evt.form_ready);
          }
        }, 500);
        //keeper, 20s
        setTimeout(function(){
          //html in form cannot be loaded
          if(check != null){
            clearInterval(check);
            check = null;
            alert('Cannot load the form!');
          }
        }, 15000);
      }

    });

    //example button clicked, collapse show/hide infos
  	$(selector.sample)
  		.on(evt.collapse_show, function(){ //collapse show element
  			checkAll();
        if(formIsGff3()){
          if(wantFivePrime()){
            activateFivePrime();
          }
          if(wantThreePrime()){
            activateThreePrime();
          }
        }
  			$(selector.wf_project_name).val('test ' + $(selector.wf_name).val());
  			$(selector.wf_run_btn).trigger(evt.click);
  		})
  		.on(evt.collapse_hide, function(){ //collapse hide element
        uncheckAll();
  			$(selector.wf_reset_btn).trigger(evt.click);
        if(formIsGff3()){
          $(selector.cds).prop('checked', true);
          deactivatePrimes();
        }
  		});

    $(selector.form)
      .on(evt.form_run, function (event, wf) { // when the server starts to run the workflow
        // redirect to the user workspace after the project is created on the server
    		$.ajax({
    		    url: userWorkspace(wf.id),
    		    success: function(data) {
    		    	$(selector.form).hide( "slide", { direction: "up" }, 500, function(){
                var msg = '<h4>Congratulation! Your project has started!</h4><br/>'
      	    			+ '<p>A mail will be sent to your email address with a link to your workspace when the project will be done.<br/>'
      	    			+ '<b>Keep at least one mail from sRNA-Tabac</b>, it will lead you to your workspace!<br/>'
      	    			+ 'Meanwhile, if you have other available projects, you can take a look at them by following the link sent to you in some previous mails';
      		    	msg += ( data == null ) ? '.<br/></p>' : ', or <a href="' + server_url + '/projects?user=' + data + '">your workspace</a>.<br/></p>';
    		    		$(selector.form).html(msg);
    		    		$(selector.form).show( "slide", { direction: "up" }, 500, function(){
    		    			setTimeout(function() {
    		    				window.location.href = ( data == null ) ? server_url : server_url + '/projects?user=' + data;
    		    			}, 30000 ); // force redirect after 30s
    		    		});
    		    	});
    		    }
    		});
  	  })
      //When html in form is ready, no error here
      .on(evt.form_ready, function(){
        clearInterval(check);
        check = null;
        //do something here for generated html, only for gff3 workflow
        if(formIsGff3()){
          //init
          $(selector.cds).prop('checked', true);
          deactivatePrimes();
          //listeners for 5'/3' changes
          $(selector.five_prime).on(evt.click, function(){
            enableFivePrime($(this).prop('checked'));
          });
          $(selector.three_prime).on(evt.click, function(){
            enableThreePrime($(this).prop('checked'));
          });
        }
      });
  };

  return{
    init:init
  };
}();

//init after dom ready
$(function() {
	workflows.init();
});

#
# Copyright (C) 2016 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2016 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

from srnatabac.core.authkey import UserAuthkey, ProjectAuthkey, CompleteAuthkey
from srnatabac.core.user_information import UserProject, UserInformation
from srnatabac.dao.user_handler import UserHandler
from srnatabac.dao.project_handler import ProjectHandler
from srnatabac.utils.common import Regex
from argparse import ArgumentTypeError

class CleanController(object):
    """ Manage the clean-projects command.

    Keyword attributes:
    user_handler -- the user handler
    project_handler -- the project handler
    """
    def __init__(self):
        self.user_handler = UserHandler()
        self.project_handler = ProjectHandler()

    def _delete_project(self, user_project, force=False):
        if not isinstance(user_project, UserProject): raise TypeError
        res = {
            'deleted_user': False,
            'deleted_project': False
        }
        if force or not force and user_project.is_outdated():
            #delete project
            deleted = self.project_handler.delete_project(user_project)
            if deleted:
                res['deleted_project'] = True
            #delete user if no project left
            user_data = self.user_handler.get_user_data(user_project.get_user_key())
            if not user_data.has_projects():
                self.user_handler.delete_user(user_project.get_user_key())
                res['deleted_user'] = True
        return res

    def clean_all(self, force=False):
        """ Clean all users projects and delete associated workflows """
        user_authkeys = self.user_handler.get_all_users_authkeys()
        res = {
            'users': 0,
            'projects': 0,
            'deleted_users': 0,
            'deleted_projects': 0
        }
        for ua in user_authkeys:
            # foreach users
            tres = self.clean_user(self.user_handler.get_user_data(ua).get_email(), force=force)
            res['users'] += tres['users']
            res['projects'] += tres['projects']
            res['deleted_users'] += tres['deleted_users']
            res['deleted_projects'] += tres['deleted_projects']
        return res

    def clean_user(self, email, force=False):
        """ Clean all user projects """
        if not Regex.email().match(email):
            raise ArgumentTypeError("'{}' is not a valid mail address".format(email))
        user_data = self.user_handler.get_user_by_email(email)
        if user_data is None:
            raise Exception("User not found")
        res = {
            'users': 1,
            'projects': 0,
            'deleted_users': 0,
            'deleted_projects': 0
        }
        if not user_data.has_projects(): #projects list empty
            self.user_handler.delete_user( user_data.get_user_key() )
            res['deleted_users'] = 1
        else: #projects list not empty, build complete key and delete
            for p in user_data.get_projects():
                deleted = self._delete_project(p, force=force)
                res['projects'] += 1
                if deleted['deleted_project']:
                    res['deleted_projects'] += 1
                if deleted['deleted_user']:
                    res['deleted_users'] += 1
                    break
        return res

    def clean_workflow(self, wfid, force=False):
        """ Clean a specific workflow """
        user_data = self.user_handler.get_user_by_workflow_id(wfid)
        if user_data is None:
            raise Exception('Workflow not found')
        user_project = user_data.get_project_by_workflow_id(wfid)
        return self._delete_project(user_project, force=force)

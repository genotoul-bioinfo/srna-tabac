#
# Copyright (C) 2016 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2016 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.1'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import json
from srnatabac.utils.srnatabac_json_encoder import SRNATabacJSONEncoder

from srnatabac.core.interaction import Interaction, InteractionDetail
from srnatabac.core.region import Region
from srnatabac.core.authkey import UserAuthkey, ProjectAuthkey, CompleteAuthkey
from srnatabac.core.user_information import UserProject, UserInformation

from srnatabac.dao.project_handler import ProjectHandler
from srnatabac.dao.interaction_handler import InteractionHandler

class DatatableController(object):
    """ The dataTable controller.

    @see: https://datatables.net/manual/server-side

    Keyword attributes:
    actual_project -- the actual project id
    all_predictions -- the list of all predictions
    predictions_index -- the list of all predictions index
    mrna_index -- the string containing all mrna (coma separated string)
    ncrna_index -- the string containing all ncrna (coma separated string)
    region_index -- the string containing all regions (coma separated string)
    software_index -- the string containing all softwares (coma separated string)
    matching_index_ids -- the prediction indexes matching [mrna/ncrna/regions]_index
    matching_predictions -- the prediction matching [mrna/ncrna/regions]_index
    """
    def __init__(self):
        self._interaction_handler = InteractionHandler()
        self.actual_project = None
        self.all_predictions = None
        # index
        self.predictions_index = None
        self.mrna_index = None
        self.ncrna_index = None
        self.region_index = None
        self.software_index = None
        self.matching_index_ids = []
        # all prediction matching the mrna/ncrna/region items
        self.matching_predictions = []
        self.already_sent_ids = []

    def _are_keys_allowed(self, **kwargs):
        """ Test the datatable sent kwargs keys

        Keyword arguments:
        kwargs -- the datatable sent kwargs
        """

        # all datatable allowed keys
        # unique keys ex: 'draw':'2', or 'start':'0'
        allowed_dt_unique_keys = ['draw', 'length', 'start']
        # multiple keys ex: 'columns[0][name]':'foo' and 'columns[35][search][value]':''
        allowed_dt_multiple_keys = ['columns', 'search', 'order']
        # custom keys ex: 'regions':'5UTR,CDS,3UTR', or 'targets':'lmo2278,lmo1054'
        allowed_dt_custom_keys = ['user', 'targets', 'regions', 'actors', 'softwares', 'project', 'groupby', 'group_one']

        def _multiple_keys_test(key):
            key_found = False
            for m in allowed_dt_multiple_keys:
                if m in key:
                    key_found = True
                    break
            return key_found

        for k in list(kwargs.keys()):
            if k in allowed_dt_custom_keys:
                continue
            if k in allowed_dt_unique_keys:
                continue
            if _multiple_keys_test(k) == True:
                continue
            raise Exception('Datatable key "{}" not authorized.'.format(k))

    def _is_group_allowed(self, group_name):
        """ Test the group name

        Keyword arguments:
        group_name -- the group the datatable can handle
        """
        allowed_groups = ['mrna_id', 'ncrna_id', 'region_id', 'software']
        return group_name in allowed_groups

    def _get_user_workflow_id(self, complete_authkey):
        """ Get the user project id if the authkey is valid

        Keyword arguments:
        complete_authkey -- the project authkey
        """
        up = ProjectHandler().get_user_project_status(complete_authkey)
        if up is None:
            raise Exception('Project not found!')
        return up['id']

    def _search_matching_index_ids(self):
        """ Search a specific region/target/actor written into the index file """
        matching_prediction_ids = []
        for prediction in self.predictions_index:
            if prediction["ncrna"] not in self.ncrna_index:
                continue
            if prediction["mrna"] not in self.mrna_index:
                continue
            if prediction["region"] not in self.region_index:
                continue
            if prediction["software"] not in self.software_index:
                continue
            matching_prediction_ids.append(prediction['id'])
        return matching_prediction_ids


    def _build_group_insert(self, group_req, group_one, *data):
        """ Build the group line command for the dataTable.

        Keyword arguments:
        group_req -- the group request
        group_one -- the level one group request
        data -- the data to iterate
        """
        if not self._is_group_allowed(group_req):
            raise Exception('Group "{}" not allowed.').format(group_req)

        sliced_data = []
        insert_data_cmd = []
        previous_pred = {}

        # let the big 'if' on group_req names be
        # it allows a much faster calculation
        # mrna_id
        if group_req == 'mrna_id':
            for current_index, current_pred in enumerate(data):
                if not previous_pred or previous_pred and previous_pred.mrna_id != current_pred.mrna_id:
                    insert_data_cmd.append({
                        'index':current_index,
                        'group_name': current_pred.mrna_id,
                        'group_count':len([t for t in data if t.mrna_id == current_pred.mrna_id])
                    })
                previous_pred = current_pred
                if group_one!=None and current_pred.mrna_id.lower() == group_one.lower():
                    sliced_data.append(current_pred)

        # ncrna_id
        elif group_req == 'ncrna_id':
            for current_index, current_pred in enumerate(data):
                if not previous_pred or previous_pred and previous_pred.ncrna_id != current_pred.ncrna_id:
                    insert_data_cmd.append({
                        'index':current_index,
                        'group_name': current_pred.ncrna_id,
                        'group_count':len([t for t in data if t.ncrna_id == current_pred.ncrna_id])
                    })
                previous_pred = current_pred
                if group_one!=None and current_pred.ncrna_id.lower() == group_one.lower():
                    sliced_data.append(current_pred)

        # region_id
        elif group_req == 'region_id':
            for current_index, current_pred in enumerate(data):
                if not previous_pred or previous_pred and Region.get_string_value(previous_pred.region_id) != Region.get_string_value(current_pred.region_id):
                    insert_data_cmd.append({
                        'index':current_index,
                        'group_name': Region.get_string_value(current_pred.region_id),
                        'group_count':len([t for t in data if Region.get_string_value(t.region_id) == Region.get_string_value(current_pred.region_id)])
                    })
                previous_pred = current_pred
                if group_one!=None and Region.get_string_value(current_pred.region_id).lower() == group_one.lower():
                    sliced_data.append(current_pred)

        # software
        elif group_req == 'software':
            for current_index, current_pred in enumerate(data):
                if not previous_pred or previous_pred and previous_pred.details.software != current_pred.details.software:
                    insert_data_cmd.append({
                        'index':current_index,
                        'group_name': current_pred.details.software,
                        'group_count':len([t for t in data if t.details.software == current_pred.details.software])
                    })
                previous_pred = current_pred
                if group_one!=None and current_pred.details.software.lower() == group_one.lower():
                    sliced_data.append(current_pred)
        return insert_data_cmd, sliced_data



    def get_datatable_content(self, **kwargs):
        """ Get all predictions data for the dataTable.
        Main method, do grouping, sorting, filtering, slicing, and coffeeing.

        Keyword arguments:
        kwargs -- the kwargs send from client
        """
        # error message for dt user
        error_msg = ""
        # data that will be sent to the datatable
        final_data = []
        # data previsualization (recordTotal & recordFiltered)
        all_data = []
        # is group command active?
        groupby_active = False

        # test keys
        try:
            self._are_keys_allowed(**kwargs)
        except Exception as e:
            error_msg = "Don't play with js, please."

        if error_msg == "":
            # project is valid?
            wf_id = None
            user_authkey = UserAuthkey(kwargs['user'])
            project_authkey = ProjectAuthkey(kwargs['project'])
            complete_authkey = CompleteAuthkey(user_authkey, project_authkey)
            try:
                wf_id = self._get_user_workflow_id(complete_authkey)
            except Exception as e:
                error_msg = "{}".format(e)

            # -- load all predictions and indexes data
            interaction_handler = InteractionHandler()
            if self.actual_project != project_authkey:
                self.all_predictions = interaction_handler.get_all_interactions(complete_authkey) # take some time...
                self.predictions_index = interaction_handler.get_interactions_indexes(complete_authkey)
                self.actual_project = project_authkey
            if self.all_predictions is None:
                self.all_predictions = interaction_handler.get_all_interactions(complete_authkey) # take some time...
            if self.predictions_index is None or self.actual_project != project_authkey:
                self.predictions_index = interaction_handler.get_interactions_indexes(complete_authkey)


            if wf_id != None and error_msg == "":

                # frist datatable draw
                if int(kwargs['draw']) == 1:
                    self.matching_predictions=self.all_predictions

                    # send back the fist ten predictions
                    # need the json.loads(json.dumps()) nested because of Region and Strand Enum types
                    final_data = json.loads(json.dumps(self.matching_predictions[0:10], cls=SRNATabacJSONEncoder, separators=(',',':')))
                    # additional informations for datatable lines
                    # self._add_additional_tr_data(None, *final_data)

                else:
                    # is group active?
                    groupby_active = ('groupby' in list(kwargs.keys()) and kwargs['groupby']!='')

                    # get index file
                    if self.mrna_index == None or self.mrna_index != kwargs['targets']:
                        self.mrna_index = kwargs['targets']
                    if self.ncrna_index == None or self.ncrna_index != kwargs['actors']:
                        self.ncrna_index = kwargs['actors']
                    if self.region_index == None or self.region_index != kwargs['regions']:
                        self.region_index = kwargs['regions']
                    if self.software_index == None or self.software_index != kwargs['softwares']:
                        self.software_index = kwargs['softwares']

                    self.matching_index_ids = []
                    for prediction in self.predictions_index:
                        if prediction["ncrna"] not in self.ncrna_index:
                            continue
                        if prediction["mrna"] not in self.mrna_index:
                            continue
                        if prediction["region"] not in self.region_index:
                            continue
                        if prediction["software"] not in self.software_index:
                            continue
                        self.matching_index_ids.append(prediction['id'])
                    self.matching_predictions = [ x for x in self.all_predictions if x.id in self.matching_index_ids ]

                    ## -- SEARCH data
                    if kwargs['search[value]']!='':
                        self.matching_predictions = [ d for d in self.matching_predictions if kwargs['search[value]'] in d.mrna_id ]

                    ## -- SORT data
                    all_data = self._sort(self.matching_predictions, **kwargs)

                    ## -- GROUP BY / SLICE data
                    # GROUP BY
                    sliced_data = []
                    if groupby_active==True:
                        group_lvl_one = kwargs['group_one'] if 'group_one' in list(kwargs.keys()) else None
                        # sliced_data will only be the group asked (one group) not all groups
                        insert_data_cmd, sliced_data = self._build_group_insert(kwargs['groupby'], group_lvl_one, *all_data)

                    else: # SLICE data
                        sliced_data = all_data[int(kwargs['start']):int(kwargs['start']) + int(kwargs['length'])]

                    # need the json.loads(json.dumps()) nested because of Region and Strand Enum types
                    final_data = json.loads(json.dumps(sliced_data, cls=SRNATabacJSONEncoder, separators=(',',':')))
                    # final_data = json.loads(json.dumps(preparing_final_data, cls=SRNATabacJSONEncoder, separators=(',',':')))
                    # additional informations for datatable lines
                    # self._add_additional_tr_data(kwargs['groupby'] if groupby_active==True else None, *final_data)
                    # self._add_additional_tr_data(None, *final_data)

        for d in final_data:
            if d is not None:
                d['DT_RowId'] = "row_" + str(d['id'])

        # callback
        to_client = {
            #see: https://datatables.net/manual/server-side > Returned data > draw
            "draw": int(str(kwargs['draw'])), # secure callback
            "recordsTotal": len(self.all_predictions),
            "recordsFiltered": len(self.matching_predictions),
            "data": final_data
            # "data": { 'src':final_data, 'in_src':[0,1,2] }
        }
        # return error to client
        if error_msg:
            to_client["error"]=error_msg
        # elif groupby_active==True: # return group
        #     to_client["group_request"] = kwargs['groupby']
        #     to_client["group_one"] = kwargs['group_one'] if 'group_one' in list(kwargs.keys()) else 'none',
        #     to_client["groups"] = insert_data_cmd
        return to_client

    # def _add_additional_tr_data(self, grpby=None, *all_data):
    #     """ Add additionnal data for dataTable, meta-data useful for ordering, tr recognition, and so-on.
    #
    #     See: https://datatables.net/manual/server-side
    #     section "Returned data" > 2nd table
    #
    #     Keyword arguments:
    #     grpby -- the group by request (default None)
    #     all_data -- the list of all interactions
    #     """
    #     for d in all_data:
    #         d['DT_RowId'] = "row_" + str(d['id'])
    #         if grpby!=None:
    #             d['DT_RowAttr'] = {'style': 'display:none;'}
    #             if grpby == 'mrna_id':
    #                 d['DT_RowData'] = {'data-group': d['mrna_id']}
    #                 d['DT_RowClass'] = 'group-item ' + d['mrna_id']
    #             elif grpby == 'ncrna_id':
    #                 d['DT_RowData'] = {'data-group': d['ncrna_id']}
    #                 d['DT_RowClass'] = 'group-item ' + d['ncrna_id']
    #             elif grpby == 'region_id':
    #                 d['DT_RowData'] = {'data-group': d['region_id']}
    #                 d['DT_RowClass'] = 'group-item ' + d['region_id']
    #             elif grpby == 'software':
    #                 d['DT_RowData'] = {'data-group': d['details']['software']}
    #                 d['DT_RowClass'] = 'group-item ' + d['details']['software']

    def _search(self, search_value_cmd, *all_data):
        """ The Search part of datatable command.

        Keyword arguments:
        search_value_cmd -- the command sent by the datatable search
        all_data -- the list of interactions data to filter
        """
        searched_values = [x.strip() for x in search_value_cmd.split(' ') if x.strip() != '']
        return [data for data in all_data for search in searched_values if search in data]

    def _sort(self, all_data, **kwargs):
        """ The Sort part of datatable command.

        Keyword arguments:
        all_data -- the list of interactions data to sort
        kwargs -- the kwargs dict
        """
        sort_commands = []
        groupby_activated = False

        # by default, if 'group by' is not active, the first column clicked has priority 0, second has priority 1, ...
        # with the 'group by' active, we shift the priority, so the column grouped by priority is 0 (the highest)
        if 'groupby' in list(kwargs.keys()) and kwargs['groupby']!='':
            groupby_activated = True
            sort_commands.append({
                'priority': str(0),
                'column':kwargs['groupby'],
                'direction':'asc'
            })

        for k in list(kwargs.keys()):
            if 'order[' in k and '][column]' in k:
                k_prio = kwargs['order[' + k[6:7] + '][column]'] #dataTable send the column sorting priority via these indexes
                sort_commands.append({
                    'priority':k[6:7] if not groupby_activated else str(int(k[6:7])+1), # shifting priority when group by is active
                    'column':kwargs['columns[' + k_prio + '][name]'],
                    'direction':kwargs['order[' + k[6:7] + '][dir]'] # 'asc' or 'desc'
                })

        # single column sorting
        if len(sort_commands) == 1:
            # command has valid column name (check interaction and interaction.details)
            column_name = sort_commands[0]['column']
            prediction_rev = (sort_commands[0]['direction']=='desc') # is reverse sorting ?
            if hasattr(InteractionDetail(), column_name):
                prediction_lambda = lambda prediction: getattr(prediction.details, column_name)
            elif hasattr(Interaction(), column_name):
                prediction_lambda = lambda prediction: getattr(prediction, column_name)
            else:
                raise AttributeError('Column name "' + column_name + '" not found for sorting. ')

        elif len(sort_commands) > 1: # multi-sorting

            def _col_to_tuple(sort_cmd_order, prediction):
                """ Build the prediction attributes tuple corresponding to the sorting_command """
                attribute_list = []
                for sc in sort_cmd_order:
                    if hasattr(InteractionDetail(), sc['column']):
                        attribute_list.append(getattr(prediction.details, sc['column']))
                    elif hasattr(Interaction(), sc['column']):
                        attribute_list.append(getattr(prediction, sc['column']))
                    else:
                        raise AttributeError('Column name "' + column_name + '" not found for sorting. ')
                return tuple(attribute_list)

            prediction_lambda = lambda prediction: _col_to_tuple(sort_commands, prediction)
            prediction_rev = False
        else:
            # default sorting (and error message send to client, not implemented yet)
            prediction_lambda = lambda prediction: prediction.id
            prediction_rev = False

        return sorted(all_data, key=prediction_lambda, reverse=prediction_rev)

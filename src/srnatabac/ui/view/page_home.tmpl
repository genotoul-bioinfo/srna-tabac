{#
	# Copyright (C) 2015 INRA
	#
	# This program is free software: you can redistribute it and/or modify
	# it under the terms of the GNU General Public License as published by
	# the Free Software Foundation, either version 3 of the License, or
	# (at your option) any later version.
	#
	# This program is distributed in the hope that it will be useful,
	# but WITHOUT ANY WARRANTY; without even the implied warranty of
	# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	# GNU General Public License for more details.
	#
	# You should have received a copy of the GNU General Public License
	# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#}



{% extends "page_template.tmpl" %}
{% set page_header_title = 'sRNA-TaBac | Home' %}
{% set page_id = 'index' %}

{% block page_more_css %}
<link href="css/font-awesome.min.css" rel="stylesheet" />
<link href="css/srnatabac.home.css" rel="stylesheet" />
{% endblock page_more_css %}

{% block page_body_content %}

<div class="jumbotron">
	<div class="media">
		<div class="media-body">
			<img class="media-object pull-left" width="120" height="120"src="img/dna.png" />
			<h1 class="media-heading">sRNA-TaBac<br />
				<small>visualize targets predictions for bacterial genomes</small>
			</h1>
		</div>
	</div>
	<br/><br/>
	<div>
		<p>
			<b>sRNA-TaBac</b> is a web visualization interface for target predictions.
		</p>
		<p>
			It allows you to load feature files, run prediction softwares and and visualize generated data through a friendly interface.
			This tool will help you to explore all your data from one or several software.
		</p>
	</div>
	<br/>
	<p style="text-align: center">
		<a class="btn btn-lg btn-default" href="/manual#quickStart" role="button">Learn more</a>
		<a class="btn btn-lg btn-primary" href="/predictGff3" role="button">Get	started</a>
	</p>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
		<h2>Key features</h2><hr/>
		<ul class="features-header">
			<li>
					<div class="pull-left"><i class="fa fa-tasks" aria-hidden="true"></i></div>
					<h3>Submit <small>a new project within seconds</small></h3>
		      <p>sRNA-TaBac comes with light and configurable forms to submit new projects</p>
		  </li>
			<li>
					<div class="pull-left"><i class="fa fa-eye" aria-hidden="true"></i></div>
					<h3>Visualize <small>generated data</small></h3>
		      <p>Take a look at the interactive folded RNA and detailled interactions</p>
		  </li>
			<li>
					<div class="pull-left"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></div>
					<h3>Interact <small>with your data</small></h3>
		      <p>Easily sort, filter, select, compare interactions in few clicks</p>
		  </li>
			<li>
					<div class="pull-left"><i class="fa fa-floppy-o" aria-hidden="true"></i></div>
					<h3>Download <small>your data</small></h3>
		      <p>Export your data in CSV, print them directly or download generated files</p>
		  </li>
			<li>
					<div class="pull-left"><i class="fa fa-cogs" aria-hidden="true"></i></div>
					<h3>Manage <small>your workspace</small></h3>
		      <p>sRNA-TaBac allows you to handle several projects in a simple workspace</p>
		  </li>
			<li>
					<div class="pull-left"><i class="fa fa-upload" aria-hidden="true"></i></div>
					<h3>Load <small>a dataset easily</small></h3>
		      <p>We design a specific <i>interaction GFF3 file format</i> to easily retrieve informations from a previously submitted project. It will allow you to gain some time</p>
		  </li>
		</ul>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
		<img src='/img/srnatabac_workspace.png' class='img-rounded docimg' width='100%'/>
		<img src='/img/srnatabac_table.png' class='img-rounded docimg' width='100%'/>
		<img src='/img/srnatabac_forna.png' class='img-rounded docimg' width='100%'/>
	</div>
</div>

<p style="text-align: center">
	<a class="btn btn-lg btn-default" href="/manual#quickStart" role="button">Learn more</a>
	<a class="btn btn-lg btn-primary" href="/predictGff3" role="button">Get	started</a>
</p>
{% endblock page_body_content %}

{#
	# Copyright (C) 2015 INRA
	#
	# This program is free software: you can redistribute it and/or modify
	# it under the terms of the GNU General Public License as published by
	# the Free Software Foundation, either version 3 of the License, or
	# (at your option) any later version.
	#
	# This program is distributed in the hope that it will be useful,
	# but WITHOUT ANY WARRANTY; without even the implied warranty of
	# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	# GNU General Public License for more details.
	#
	# You should have received a copy of the GNU General Public License
	# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#}

{% extends "page_template.tmpl" %}
{% set page_header_title = 'sRNA-TaBac | My projects' %}
{% set page_id = 'projects' %}

{% block page_more_css %}
<link href="css/jflow.min.css" rel="stylesheet" />
<link href="css/font-awesome.min.css" rel="stylesheet" />
<link href="css/srnatabac.projects.css" rel="stylesheet" />
{% endblock page_more_css %}

{% block page_more_js %}
<script src="js/jflow.min.js"></script>
<script src="js/srnatabac.projects.js"></script>
{% endblock page_more_js %}

{% block page_body_content %}
<div class="row">
	<div class="page-header">
		<h1>
			My projects <small>summary page</small>
		</h1>
	</div>
	<div id="projects_list" class="row">

	{% if workflows_list|length != 0 %}
		<table class="table table-hover table-striped table-fixed">
			<thead>
				<tr>
					<th>Project name</th>
					<th>Status</th>
					<th>Execution time</th>
					<th>Softwares</th>
					<th>Actions</th>
					<th>Result</th>
				</tr>
			</thead>
			<tbody id='table-body'>
				{% for wf_infos in workflows_list %}
					{% set wf = wf_infos['wf'] %}
					{% set wfuuid = wf_infos['wf_uuid'] %}
				<tr id="{{wfuuid}}" class="see_results">
					<td><b>{{wf.project_name}}</b><br/>{{wf.id}} - {{ 'Load' if wf.name == 'load' else 'Predict' }}</td>
					{% if wf.status == "completed" %}
						{% set td_color = 'label-success' %}
					{% elif wf.status == "failed" %}
						{% set td_color = 'label-danger' %}
					{% elif wf.status == "aborted" %}
						{% set td_color = 'label-danger' %}
					{% elif wf.status == "started"%}
						{% set td_color = 'label-info' %}
					{% else %}
						{% set td_color = 'label-default' %}
					{% endif %}
					<td><span class="label {{td_color}}">{{wf.status}}</span></td>
					<td>{{wf.start_time}}<br/>{{wf.end_time}}</td>
					<td>
						{% for s in wf['softwares'] %}
							{% if wf['softwares'][s] %}
						<span class="badge alert-success">{{s}}</span>
							{% endif %}
						{% endfor %}
					</td>
					<td>
						<button type="button" id="delete_wf_{{wfuuid}}!{{wf.id}}"
							class="btn btn-default btn-sm" data-toggle="tooltip"
							data-placement="top" title="Delete Project"
							{% if wf.status!='completed' and wf.status!='failed' and wf.status!='aborted' %} disabled {% endif %}>
							<i class="fa fa-fw fa-trash-o"></i>
						</button>
					</td>
					{% if wf.name == 'load' or wf.name == 'predictfasta' or wf.name == 'predictgff3' %}
					<td>
						<div class="btn-group" role="group" aria-label="interactWithResults">
							<button type="button" id="results_{{wfuuid}}"
								class="btn btn-default btn-sm" data-toggle="tooltip"
								data-placement="top" title="See results"
								{% if wf.status!='completed' %} disabled {% endif %}>
								<i class="fa fa-fw fa-eye"></i>
							</button>
							<button type="button" id="outputs_{{wfuuid}}"
								class="btn btn-default btn-sm" data-toggle="tooltip"
								data-placement="top" title="See outputs"
								{% if wf.status!='completed' %} disabled {% endif %}>
								<i class="fa fa-fw fa-download"></i>
							</button>
						</div>
					</td>
					{% else %}
					<td></td>
					{% endif %}
				</tr>
				<tr class="pbarinvisible"></tr>
				<tr class="pbartr">
					{% set pgrbar_val = 0 %}
					{% set comp = wf.components %}
					{% set nb_total_comp = comp|length %}
					{% set ratio_comp = 100/nb_total_comp if nb_total_comp > 0 else 0 %}
					<td colspan="6" class="pbartd">
						<div id="pbar_{{wf.id}}" class="pbardiv progress">
							{% set nb_empty_pgrbar = 0 %}
							{% for c in comp  %}
								{% set cempty = False %}
								{% if c.failed>=1 %}
									{% set prgbar = 'progress-bar-danger' %}
								{% elif c.aborted>=1 %}
										{% set prgbar = 'progress-bar-warning' %}
								{% elif c.running>=1 %}
									{% set prgbar = 'progress-bar-info' %}
								{% elif c.completed==c.total %}
									{% set prgbar = 'progress-bar-success' %}
								{% else %}
									{% set nb_empty_pgrbar = nb_empty_pgrbar + 1 %}
									{% set cempty = True %}
								{% endif %}
								{% if not cempty %}
	            <div class="progress-bar {{prgbar}}" role="progressbar"
								style="{% if comp|first %}min-width: 1em; {% endif %}width: {{ratio_comp}}%;">
	                <span class="sr-only"></span>
	            </div>
								{% endif %}
							{% endfor %}

							{% if nb_empty_pgrbar %}
								{% for n in range(nb_empty_pgrbar)  %}
							<div class="progress-bar progress-bar-custom" role="progressbar"
								style="width: {{ratio_comp}}%;">
	                <span class="sr-only"></span>
	            </div>
								{% endfor %}
							{% endif %}
	          </div> <!-- progress bar -->
					</td>
				</tr>
			{% endfor %}
			</tbody>
		</table>
	{% else %}
		<p>
		No project found!
		</p>
	{% endif %}
	</div><!-- table -->
	<div style="border-top: 1px solid #EEEEEE; padding-top:15px;">
		<button id="delete_failed" type="button" class="btn btn-default" style="float:right;margin-right:55px;">
			<i class="fa fa-fw fa-trash-o"></i>&nbsp; Delete failed projects
		</button>
	</div>
	<div id="modal"></div>
</div>
{% endblock page_body_content %}

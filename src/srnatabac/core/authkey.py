#
# Copyright (C) 2016 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2016 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

class Authkey(object):
    """ Represents the entity which controls the user/project binding. """
    def __init__(self, pseudo_key=None):
        if pseudo_key is not None and not isinstance(pseudo_key, str):
            raise TypeError
        self._key = Authkey.new_authkey() if pseudo_key is None or pseudo_key == '' else pseudo_key

    def __eq__(self, other):
        raise NotImplementedError

    def __ne__(self, other):
        raise NotImplementedError

    def get_key(self):
        """ Get the key """
        return self._key

    @staticmethod
    def new_authkey():
        """ Create a new authkey
        see: https://docs.python.org/3/library/uuid.html
        """
        from uuid import uuid4
        return str(uuid4())

class UserAuthkey(Authkey):
    """ UUID4 for user """

    def __eq__(self, other):
        if other is not None and not isinstance(other, UserAuthkey): raise TypeError
        return self.get_key() == other.get_key() if other is not None else False

    def __ne__(self, other):
        if other is not None and not isinstance(other, UserAuthkey): raise TypeError
        return self.get_key() != other.get_key() if other is not None else True


class ProjectAuthkey(Authkey):
    """ UUID4 for project """

    def __eq__(self, other):
        if other is not None and not isinstance(other, ProjectAuthkey): raise TypeError
        return self.get_key() == other.get_key() if other is not None else False

    def __ne__(self, other):
        if other is not None and not isinstance(other, ProjectAuthkey): raise TypeError
        return self.get_key() != other.get_key() if other is not None else True

class CompleteAuthkey(object):
    """ Represents the combinaison between a user authkey and a project authkey

    xxxxxxxxxxxxx yyyyyyyyyyyyyyyy
    ---userkey--- ---projectkey---
    ---------completekey----------

    Keyword attributes:
    _user_authkey -- the user authkey
    _project_authkey -- the project authkey
    """

    def __init__(self, user_authkey=None, project_authkey=None):
        if user_authkey is None or not isinstance(user_authkey, UserAuthkey):
            raise TypeError
        if project_authkey is None or not isinstance(project_authkey, ProjectAuthkey):
            raise TypeError
        self._user_authkey = user_authkey
        self._project_authkey = project_authkey

    def __eq__(self, other):
        if not isinstance(other, CompleteAuthkey): raise TypeError
        return self.get_user_part() == other.get_user_part()\
            and self.get_project_part() == other.get_project_part()

    def __ne__(self, other):
        if not isinstance(other, CompleteAuthkey): raise TypeError
        return self.get_user_part() != other.get_user_part()\
            or self.get_project_part() != other.get_project_part()

    def get_user_part(self):
        """ Get the user part """
        return self._user_authkey

    def get_project_part(self):
        """ Get the project part """
        return self._project_authkey

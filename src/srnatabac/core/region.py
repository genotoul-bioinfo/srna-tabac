#
# Copyright (C) 2016 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2016 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

from enum import Enum, unique

@unique
class Region(Enum):
    """Region enumeration.

    --------------------------------
    /!\ DO NOT EXTEND THIS CLASS /!\
    --------------------------------

    @See -- http://stackoverflow.com/questions/36932/how-can-i-represent-an-enum-in-python
    @See -- https://www.python.org/dev/peps/pep-0435/
    @See -- https://docs.python.org/3.4/library/enum.html
    """

    """
    MAKE THESE PARAMETERS IN ALPHABETIC/NUMERIC ASCENDING ORDER FOR AUTOMATIC AND MEANINGFUL SORTING !!!
    @see -- "__gt__", "__lt__", "__ge__" and "__le__" at the end of this class
    """
    region_3UTR = 1
    region_5UTR = 2
    region_CDS = 3


    @staticmethod
    def get_region_from_string(str_region):
        """ Get a region object from a given string.

        Keyword arguments:
        str_region -- a gff3 region formatted string. Case-insensitive.
        Can be 'five_prime_utr', '5utr', 'three_prime_utr', '3utr', 'cds'.
        """
        if str_region is None or str_region == '':
            raise ValueError('String cannot be nul')
        lower_region = str_region.lower()
        if lower_region == 'five_prime_utr' or lower_region == '5utr':
            tmp = Region.region_5UTR
        elif lower_region == 'three_prime_utr' or lower_region == '3utr':
            tmp = Region.region_3UTR
        elif lower_region == 'cds':
            tmp = Region.region_CDS
        else:
            raise AttributeError
        return tmp

    @staticmethod
    def get_all_string_values():
        """ Get all upper-formatted string region values."""
        return ["5UTR", "CDS", "3UTR"];

    @staticmethod
    def get_string_value(region):
        """ Get a upper-formatted string for a given region.

        Keyword arguments:
        region -- a Region object.
        """
        if not isinstance(region, Region):
            raise TypeError('Need a Type Region in parameter.')
        reg_tmp = ""
        if region == Region.region_5UTR:
            reg_tmp += "5UTR"
        elif region == Region.region_3UTR:
            reg_tmp += "3UTR"
        else:
            reg_tmp += "CDS"
        return reg_tmp

    @staticmethod
    def get_all_gff3_values():
        """ Get a list of all gff3 region string values."""
        return ["five_prime_UTR", "CDS", "three_prime_UTR"];

    @staticmethod
    def get_gff3_value(region):
        """ Get a gff3 string of a given region.
        Can only be 'five_prime_UTR', 'CDS' or 'three_prime_UTR'.

        Keyword arguments:
        region -- a Region object.
        """
        if not isinstance(region, Region):
            raise TypeError('Need a Type "Region" in parameter.')
        reg_tmp = ""
        if region == Region.region_5UTR:
            reg_tmp += "five_prime_UTR"
        elif region == Region.region_3UTR:
            reg_tmp += "three_prime_UTR"
        else:
            reg_tmp += "CDS"
        return reg_tmp

    ## --------------------------------------------------------------
    # Define these methods for automatic sorting

    def __ge__(self, other):
        if self.__class__ is other.__class__:
            return self._value_ >= other._value_
        raise TypeError('Cannot compare region to {}'.format(other.__class__.__name__))

    def __gt__(self, other):
        if self.__class__ is other.__class__:
            return self._value_ > other._value_
        raise TypeError('Cannot compare region to {}'.format(other.__class__.__name__))

    def __le__(self, other):
        if self.__class__ is other.__class__:
            return self._value_ <= other._value_
        raise TypeError('Cannot compare region to {}'.format(other.__class__.__name__))

    def __lt__(self, other):
        if self.__class__ is other.__class__:
            return self._value_ < other._value_
        raise TypeError('Cannot compare region to {}'.format(other.__class__.__name__))

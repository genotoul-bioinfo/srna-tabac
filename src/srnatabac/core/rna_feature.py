#
# Copyright (C) 2016 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro, Anais Painset - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2016 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.1'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

from srnatabac.core.strand import Strand


class ncRNA(object):
    """ Represents a ncRNA.

    Keyword attributes:
    id -- the unique id
    start -- the start position
    stop -- the stop position
    strand -- the strand, @see srnatabac.core.Strand
    _sequence -- the sequence of the mRNA (default None)
    """

    def __init__(self, idt, start, stop, strand):
        if not isinstance(strand, Strand):
            raise TypeError('Need a Strand object')
        if start > stop:
            raise ValueError('start > stop')
        self.id = idt
        self.start = start
        self.stop = stop
        self.strand = strand
        self.sequence = None

    def __str__(self):
        return '{} {} {} {} {}'.format(self.id, self.start, self.stop, Strand.get_string_value(self.strand), self.sequence)

    def __len__(self):
        return self.stop - self.start


class mRNA(ncRNA):
    """ Represents a mRNA.

    Keyword attributes:
    gene_start -- the gene start position associated to target (default None)
    gene_stop -- the gene stop position associated to target (default None)
    """

    def __init__(self, idt, start, stop, strand, gene_start=None, gene_stop=None):
        if gene_start is None and gene_stop is not None or gene_stop is None and gene_start is not None:
            raise AttributeError
        if gene_stop is not None and gene_start is not None and gene_start > gene_stop:
            raise AttributeError('start > stop')
        ncRNA.__init__(self, idt, start, stop, strand)
        self.gene_start = gene_start
        self.gene_stop = gene_stop

    def __str__(self):
        return '{} {} {}'.format(ncRNA.__str__(self), self.gene_start, self.gene_stop)

    def get_gene_length(self):
        return self.gene_stop - self.gene_start

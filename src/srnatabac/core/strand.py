#
# Copyright (C) 2016 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2016 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

from enum import Enum, unique

@unique
class Strand(Enum):
    """Strand enumeration.

    --------------------------------
    /!\ DO NOT EXTEND THIS CLASS /!\
    --------------------------------

    @See -- http://stackoverflow.com/questions/36932/how-can-i-represent-an-enum-in-python
    @See -- https://www.python.org/dev/peps/pep-0435/
    @See -- https://docs.python.org/3.4/library/enum.html
    """

    """
    MAKE THESE PARAMETERS IN ALPHABETIC/NUMERIC ASCENDING ORDER FOR AUTOMATIC AND MEANINGFUL SORTING !!!
    @see -- "__gt__", "__lt__", "__ge__" and "__le__" at the end of this class
    """
    minus = 1
    plus = 2

    @staticmethod
    def get_strand_from_string(str_strand):
        """ Get a Strand object from a given string.

        Keyword arguments:
        str_strand -- a string. Case-insensitive.
        Can be '+', 'plus', 'minus' or '-'.
        """
        if str_strand is None or str_strand == '':
            raise ValueError('String cannot be nul')
        if str_strand.lower() == 'plus' or str_strand == '+':
            tmp = Strand.plus
        elif str_strand.lower() == 'minus' or str_strand == '-':
            tmp = Strand.minus
        else:
            raise AttributeError
        return tmp

    @staticmethod
    def get_all_string_values():
        """ Get all string strand values. """
        return ["+", "-"];

    @staticmethod
    def get_string_value(strand):
        """ Get a formatted string for a given strand.

        Keyword arguments:
        strand -- a Strand object.
        """
        if not isinstance(strand, Strand):
            raise TypeError('Need a Type Strand in parameter.')
        st_tmp = ""
        if strand == Strand.plus:
            st_tmp += "+"
        else:
            st_tmp += "-"
        return st_tmp

    ## --------------------------------------------------------------
    # Define these methods for automatic sorting
    def __ge__(self, other):
        if self.__class__ is other.__class__:
            return self._value_ >= other._value_
        return NotImplemented

    def __gt__(self, other):
        if self.__class__ is other.__class__:
            return self._value_ > other._value_
        return NotImplemented

    def __le__(self, other):
        if self.__class__ is other.__class__:
            return self._value_ <= other._value_
        return NotImplemented

    def __lt__(self, other):
        if self.__class__ is other.__class__:
            return self._value_ < other._value_
        return NotImplemented

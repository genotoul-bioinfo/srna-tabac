#
# Copyright (C) 2016 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro, Anais Painset - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2016 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.1'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

from srnatabac.core.strand import Strand
from srnatabac.core.region import Region
from srnatabac.core.rna_feature import mRNA, ncRNA

from jflow.featureio import GFF3Record

class InteractionDetail:
    """Represents the details of an interaction object.

    Keyword attributes:
    mrna_begin -- the mrna begin position for target (default 0)
    mrna_end -- the mrna end position for target (default 0)
    mrna_seq -- the sequence involved in interaction for target (default [])

    ncrna_begin -- the ncrna begin position for actor (default 0)
    ncrna_end -- the ncrna end position for actor (default 0)
    ncrna_seq -- the sequence involved in interaction for actor (default [])

    software -- the software name used to produce the interaction (default None)
    symbolism -- the notation used when producing alignment-like format (default [])
        ex: ['|', '|', '|', '.', ' ', ' ', '|', '|']

    energy_software -- the software energy prediction (default 0)
    energy_duplex -- the energy base prediction, produced by 'rnaeval' (default 0)

    overlap_length -- the overlap length (default 0)
    overlap_start -- is the start codon overlapping? (default False)
    overlap_stop -- is the stop codon overlapping? (default False)

    pos_to_gene_start -- the position to gene start codon (default 0)
    pos_to_gene_stop -- the position to gene stop codon (default 0)

    match_count -- the total count of matches in the interaction (default 0)
    match_successive -- the maximum count of successive matches in the interaction (default 0)

    pair_pct_AU -- the rate of A-U pairs into the interaction (default 0)
    pair_pct_GC -- the rate of G-C pairs into the interaction (default 0)
    pair_pct_GU -- the rate of G-U pairs into the interaction (default 0)
    """

    def __init__(self):
        self.mrna_begin = 0
        self.mrna_end = 0
        self.mrna_seq = []

        self.ncrna_begin = 0
        self.ncrna_end = 0
        self.ncrna_seq = []

        self.software = None        # software name
        self.symbolism = []

        self.energy_software = 0    # software energy prediction
        self.energy_duplex = 0      # energy base

        self.overlap_length = 0     # overlap length
        self.overlap_start = False  # overlapping start codon
        self.overlap_stop = False   # overlapping stop codon

        self.pos_to_gene_start = 0  # position to gene start codon
        self.pos_to_gene_stop = 0   # position to gene stop codon

        self.match_count = 0
        self.match_successive = 0

        self.pair_pct_AU = 0
        self.pair_pct_GC = 0
        self.pair_pct_GU = 0

    def __len__(self):
        return len(self.mrna_seq)

class Interaction:
    """Represents an interaction between a mRNA and a ncRNA.

    Keyword attributes:
    id -- the interaction unique id (default None)
    mrna_id -- the mrna unique id, same as mrna.id (default None)
    ncrna_id -- the ncrna unique id, same as ncrna.id (default None)
    region_id -- the region unique id, @see srnatabac.core.Region (default None)
    details -- the interaction details object, @see InteractionDetail class in this file (default InteractionDetail())
    mrna -- the mrna object of the interaction, @see srnatabac.core.rna_feature (default None)
    ncrna -- the ncrna object of the interaction, @see srnatabac.core.rna_feature (default None)
    """

    def __init__(self):
        self.id = None          # interaction id
        self.mrna_id = None     # target id
        self.ncrna_id = None    # actor id
        self.region_id = None   # region location ('5UTR', 'CDS' or '3UTR')

        # info about interaction
        self.details = InteractionDetail()
        self.mrna = None        # mRNA object involved in the interaction
        self.ncrna = None       # ncRNA object involved in the interaction

    def __len__(self):
        return len(self.details)

    def get_dbn(self):
        """ Get the duplex dot-bracket notation.

        sequence <mRNA & ncRNA>
        struct <mRNA & ncRNA>
        """
        seq = []
        dot_bracket = []
        for i in range(self.details.overlap_length):
            if (self.details.symbolism[i] != ' ') and (self.details.mrna_seq[i] != '-'):
                dot_bracket.append('(')
                seq.append(self.details.mrna_seq[i])
            elif (self.details.symbolism[i] == ' ') and (self.details.mrna_seq[i] != '-'):
                dot_bracket.append('.')
                seq.append(self.details.mrna_seq[i])
        rev_ncrna = self.details.ncrna_seq[::-1]
        rev_sym = self.details.symbolism[::-1]

        dot_bracket.append('&')
        seq.append('&')
        for i in range(self.details.overlap_length):
            if (rev_sym[i] != ' ') and (rev_ncrna[i] != '-'):
                dot_bracket.append(')')
                seq.append(rev_ncrna[i])
            elif (rev_sym[i] == ' ') and (rev_ncrna[i] != '-'):
                dot_bracket.append('.')
                seq.append(rev_ncrna[i])

        return ''.join(seq), ''.join(dot_bracket)

    def export_index(self):
        """ Print the self-index object."""
        return {
            "id": self.id,
            "ncrna": self.ncrna_id,
            "mrna": self.mrna_id,
            "region": Region.get_string_value(self.region_id),
            "software": self.details.software
        }

    def get_alignment_like(self):
        """ Print in 'alignment-like' format."""
        out = "\n"
        out += "@{0} (Software name : {1} / Software energy = {2:.2f} kcal/mol)\n".format(self.id, self.details.software, self.details.energy_software)
        out += "{0}_{1}|{2}-{3}|{4}|{5}-{6}|mRNA\n".format(
            self.mrna_id, Region.get_string_value(self.region_id), self.mrna.start, self.mrna.stop, Strand.get_string_value(self.mrna.strand), self.mrna.gene_start, self.mrna.gene_stop)
        out += "{0}|{1}-{2}|{3}|ncRNA\n".format(self.ncrna_id, self.ncrna.start, self.ncrna.stop, Strand.get_string_value(self.ncrna.strand))
        out += "\nOverlap={0}\tMatches={1}\tSuccessive_matches={2}\tEnergy={3:.2f} kcal/mol\n".format(
            self.details.overlap_length, self.details.match_count, self.details.match_successive, self.details.energy_duplex)
        out += "GC={0:.2f}\t\tAU={1:.2f}\t\tGU={2:.2f}\n\n".format(self.details.pair_pct_GC, self.details.pair_pct_AU, self.details.pair_pct_GU)
        out += "Start(#)={0}\tStop(*)={1}\n\n".format(self.details.pos_to_gene_start, self.details.pos_to_gene_stop)
        out += self.get_alignment_like_schema()
        out += "\n\n"
        return out

    def get_alignment_like_schema(self):
        """ Build the alignment-like interaction schema."""
        def _get_gene_offset(seq, pos):
            """ Calculate the induct offset by insertion into mRNA sequence.

            Keyword arguments:
            seq -- the mRNA sequence
            pos -- the position (mainly the 'gene start' or the 'gene stop')
            """
            offset = 0
            c = 0
            for a in seq:
                if pos != c:
                    if a == "-":
                        offset += 1
                    else:
                        c += 1
            return offset
        # get String obj len
        mrna_b = len(str(self.details.mrna_begin))
        mrna_e = len(str(self.details.mrna_end))
        ncrna_b = len(str(self.details.ncrna_begin))
        ncrna_e = len(str(self.details.ncrna_end))
        # create global blank
        b3 = " " * 3
        b5 = " " * 5
        # blank offset begin / end (for line begin / end)
        mrna_offset_begin = "" if mrna_b > ncrna_b else " " * (ncrna_b - mrna_b)
        ncrna_offset_begin = " " * (mrna_b - ncrna_b) if mrna_b > ncrna_b else ""
        sym_offset_begin = " " * ncrna_b if len(mrna_offset_begin) > 0 else " " * mrna_b
        mrna_offset_end = "" if mrna_e > ncrna_e else " " * (ncrna_e - mrna_e)
        ncrna_offset_end = " " * (mrna_e - ncrna_e) if mrna_e > ncrna_e else ""
        sym_offset_end = " " * ncrna_e if len(mrna_offset_end) > 0 else " " * mrna_e
        out = ""
        t = (len(self.details.mrna_seq) - self.details.mrna_seq.count("-"))
        if self.details.pos_to_gene_start != "/" and self.details.pos_to_gene_start > 0 and self.details.pos_to_gene_start <= t:
            dec = _get_gene_offset(self.details.mrna_seq, self.details.pos_to_gene_start)
            out += b5 + b3 + sym_offset_begin + b3 + "5'" + " " * (self.details.pos_to_gene_start - 1 + dec) + "#"
            out += " " * (len(self.details.mrna_seq) - self.details.pos_to_gene_start - dec) + "3'" + b3 + sym_offset_end + "\n"
        elif self.details.pos_to_gene_stop != "/" and self.details.pos_to_gene_stop > 0 and self.details.pos_to_gene_stop <= t:
            dec = _get_gene_offset(self.details.mrna_seq, self.details.pos_to_gene_stop)
            out += b5 + b3 + sym_offset_begin + b3 + "5'" + " " * (self.details.pos_to_gene_stop + dec) + "*"
            out += " " * (len(self.details.mrna_seq) - self.details.pos_to_gene_stop - dec) + "3'" + b3 + sym_offset_end + "\n"
        else:
            out += b5 + b3 + sym_offset_begin + b3 + "5'" + " " * len(self.details.mrna_seq) + "3'" + b3 + sym_offset_end + "\n"
        out += " mRNA" + b3 + mrna_offset_begin + str(self.details.mrna_begin)  + b5 + ''.join(self.details.mrna_seq) + b5 + str(self.details.mrna_end) + mrna_offset_end + "\n"
        out += b5 + b3 + sym_offset_begin + b5 + ''.join(self.details.symbolism) + b5 + sym_offset_end + "\n"
        out += "ncRNA" + b3 + ncrna_offset_begin + str(self.details.ncrna_begin) + b5  + ''.join(self.details.ncrna_seq) + b5 + str(self.details.ncrna_end) + ncrna_offset_end + "\n"
        out += b5 + b3 + sym_offset_begin + b3 + "3'" + " " * len(self.details.ncrna_seq) + "5'" + b3 + sym_offset_end + "\n"
        return out

    def __contains__(self, m):
        """ 'in' keyword definition for an interaction.

        Keyword arguments:
        m -- the key searched
        """
        b = False
        if self.details != None and self.details.software != None and m.lower() in self.details.software.lower():
            b = True
        elif self.id != None and m.lower() in self.id.lower():
            b = True
        elif self.mrna_id != None and m.lower() in self.mrna_id.lower():
            b = True
        elif self.ncrna_id != None and m.lower() in self.ncrna_id.lower():
            b = True
        elif self.region_id != None and m.lower() in Region.get_string_value(self.region_id).lower():
            b = True
        return b

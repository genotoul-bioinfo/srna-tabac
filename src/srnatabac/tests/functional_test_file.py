#
# Copyright (C) 2016 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2016 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'beta'

import unittest
import os

from srnatabac.utils.utils_for_tests import GFF3File, PGZInteractionFile, TestUtilsWorkflow, TestUtilsGFF3Workflow

class TestSrnatabacUtilsFunctionalTestFile(unittest.TestCase):
    """ Test utils tests """

    def setUp(self):
        """ Before each test """
        self._predictgff3 = TestUtilsGFF3Workflow()

    def test_utils_test_utils_test(self):
        # #change defaults args
        data_dir = TestUtilsWorkflow.get_test_data_dir()
        #info
        self._predictgff3.set_project_name('test_gff3')
        self._predictgff3.set_input_gff3(os.path.join(data_dir, 'listeria_small_sample.gff3'))
        self._predictgff3.set_input_genome_ref(os.path.join(data_dir, 'listeria_genome_ref.fasta'))
        self._predictgff3.set_user_email('regis.ongaro@toulouse.inra.fr')
        #soft
        self._predictgff3.enable_rnaplex()
        #region
        self._predictgff3.enable_3utr(60, 60)
        self._predictgff3.enable_cds()
        tmp_args = self._predictgff3.call()
        print(tmp_args)

    def tearDown(self):
        """ After each test """
        del self._predictgff3
        # del self._predictfasta
        # del self._load

#launch tests
if __name__ == '__main__':
    #see -- https://docs.python.org/3/library/unittest.html
    unittest.main()

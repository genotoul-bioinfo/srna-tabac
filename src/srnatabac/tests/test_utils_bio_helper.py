#
# Copyright (C) 2016 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2016 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.1'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import unittest
from srnatabac.utils.bio_helper import BioHelper
class TestSrnatabacUtilsBioHelper(unittest.TestCase):
    """ BioHelper tests """

    def setUp(self):
        """ Before each test """
        self._sequence = 'AATCGATCGATCGATGCGTA'

    def test_utils_bio_helper_complement(self):
        complement_expected = 'TTAGCTAGCTAGCTACGCAT'
        self.assertEqual(BioHelper.complement(self._sequence), complement_expected)

    def test_utils_bio_helper_complement_wrong_type(self):
        with self.assertRaises(AttributeError):
            BioHelper.complement(1)
            BioHelper.complement(None)
            BioHelper.complement([])
            BioHelper.complement({})

    def test_utils_bio_helper_reverse_complement(self):
        reverse_complement_expected = 'TACGCATCGATCGATCGATT'
        self.assertEqual(BioHelper.reverse_complement(self._sequence), reverse_complement_expected)

    def test_utils_bio_helper_reverse_complement_wrong_type(self):
        with self.assertRaises(AttributeError):
            BioHelper.reverse_complement(1)
            BioHelper.reverse_complement(-1.3)
            BioHelper.reverse_complement(None)
            BioHelper.reverse_complement([])
            BioHelper.reverse_complement({})

    def test_utils_bio_helper_extract_wrong_type(self):
        with self.assertRaises(AttributeError):
            BioHelper.reverse_complement(1)
            BioHelper.reverse_complement(-1.3)
            BioHelper.reverse_complement(None)
            BioHelper.reverse_complement([])
            BioHelper.reverse_complement({})

            BioHelper.reverse_complement('', 'e')
            BioHelper.reverse_complement('', None)
            BioHelper.reverse_complement('', -1.3)
            BioHelper.reverse_complement('', [])
            BioHelper.reverse_complement('', {})

            BioHelper.reverse_complement('', 0, 'e')
            BioHelper.reverse_complement('', 0, None)
            BioHelper.reverse_complement('', 0, [])
            BioHelper.reverse_complement('', 0, -1.3)
            BioHelper.reverse_complement('', 0, {})

            BioHelper.reverse_complement('', 0, 1, 'e')
            BioHelper.reverse_complement('', 0, 1, None)
            BioHelper.reverse_complement('', 0, 1, [])
            BioHelper.reverse_complement('', 0, 1, -1.3)
            BioHelper.reverse_complement('', 0, 1, {})

            BioHelper.reverse_complement('', 0, 1, '+', 'e')
            BioHelper.reverse_complement('', 0, 1, '+', None)
            BioHelper.reverse_complement('', 0, 1, '+', [])
            BioHelper.reverse_complement('', 0, 1, '+', -1.3)
            BioHelper.reverse_complement('', 0, 1, '+', {})

    def test_utils_bio_helper_extract_wrong_direction(self):
        with self.assertRaises(AttributeError):
            BioHelper.extract_from(self._sequence, 2, 1, '+')
            BioHelper.extract_from(self._sequence, 0, 0, '+')

    def test_utils_bio_helper_extract(self):
        self.assertEqual(BioHelper.extract_from(self._sequence, 1, 1, '+'), 'A')
        self.assertEqual(BioHelper.extract_from(self._sequence, 3, 3, '+'), 'T')
        self.assertEqual(BioHelper.extract_from(self._sequence, 1, 5, '+'), 'AATCG')
        #reverse complement
        self.assertEqual(BioHelper.extract_from(self._sequence, 1, 1, '-'), 'T')
        self.assertEqual(BioHelper.extract_from(self._sequence, 3, 3, '-'), 'A')
        self.assertEqual(BioHelper.extract_from(self._sequence, 1, 5, '-'), 'CGATT')

    def test_utils_bio_helper_circular_extract(self):
        #AATCG ATCGA TCGAT GCGTA
        self.assertEqual(BioHelper.extract_from(self._sequence, 16, 25, '+', circular=True), 'GCGTAAATCG')
        self.assertEqual(BioHelper.extract_from(self._sequence, 19, 21, '+', circular=True), 'TAA')
        #reverse complement
        self.assertEqual(BioHelper.extract_from(self._sequence, 16, 25, '-', circular=True), 'CGATTTACGC')
        self.assertEqual(BioHelper.extract_from(self._sequence, 19, 21, '-', circular=True), 'TTA')

    def test_utils_bio_helper_uracil_wrong(self):
        with self.assertRaises(AttributeError):
            BioHelper.to_uracil(None)
            BioHelper.to_uracil(1)
            BioHelper.to_uracil(-1.3)
            BioHelper.to_uracil([])
            BioHelper.to_uracil({})

    def test_utils_bio_helper_uracil_good(self):
        self.assertEqual(BioHelper.to_uracil(self._sequence), 'AAUCGAUCGAUCGAUGCGUA')

    def test_utils_bio_helper_thymine_wrong(self):
        with self.assertRaises(AttributeError):
            BioHelper.to_thymine(None)
            BioHelper.to_thymine(1)
            BioHelper.to_thymine(-1.3)
            BioHelper.to_thymine([])
            BioHelper.to_thymine({})

    def test_utils_bio_helper_thymine_good(self):
        u = BioHelper.to_uracil(self._sequence)
        self.assertEqual(BioHelper.to_thymine(u), self._sequence)

    def test_utils_bio_helper_extract_uracil_wrong(self):
        with self.assertRaises(TypeError):
            BioHelper.extract_from(self._sequence, 1, 1, '+', to_uracil=None)
            BioHelper.extract_from(self._sequence, 1, 1, '+', to_uracil='e')
            BioHelper.extract_from(self._sequence, 1, 1, '+', to_uracil=[])
            BioHelper.extract_from(self._sequence, 1, 1, '+', to_uracil=-1.3)
            BioHelper.extract_from(self._sequence, 1, 1, '+', to_uracil=1)
            BioHelper.extract_from(self._sequence, 1, 1, '+', to_uracil={})
        BioHelper.extract_from(self._sequence, 1, 1, '+', to_uracil=True)

    def test_utils_bio_helper_circular_extract_uracil(self):
        #AATCG ATCGA TCGAT GCGTA
        self.assertEqual(BioHelper.extract_from(self._sequence, 16, 25, '+', circular=True, to_uracil=True), 'GCGUAAAUCG')
        self.assertEqual(BioHelper.extract_from(self._sequence, 19, 21, '+', circular=True, to_uracil=True), 'UAA')
        #reverse complement
        self.assertEqual(BioHelper.extract_from(self._sequence, 16, 25, '-', circular=True, to_uracil=True), 'CGAUUUACGC')
        self.assertEqual(BioHelper.extract_from(self._sequence, 19, 21, '-', circular=True, to_uracil=True), 'UUA')

    def tearDown(self):
        """ After each test """
        del self._sequence

#launch tests
if __name__ == '__main__':
    #see -- https://docs.python.org/3/library/unittest.html
    unittest.main()

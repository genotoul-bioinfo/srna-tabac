#
# Copyright (C) 2016 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2016 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import unittest
from srnatabac.core.strand import Strand

class TestSrnatabacCoreStrand(unittest.TestCase):
    """ Strand tests """

    def test_core_strand_create(self):
        self.assertIsNotNone(Strand.plus)
        self.assertIsNotNone(Strand.minus)

    def test_core_strand_wrong_attribute(self):
        with self.assertRaises(AttributeError):
            Strand.unknown_attribute

    def test_core_strand_compare_eq(self):
        p = Strand.plus
        m = Strand.minus
        self.assertEqual(p, Strand.plus)
        self.assertEqual(m, Strand.minus)
        self.assertNotEqual(p, Strand.minus)
        self.assertNotEqual(m, Strand.plus)

    def test_core_strand_all_str(self):
        a = Strand.get_all_string_values()
        self.assertEqual(len(a), 2)
        self.assertIn('+', a)
        self.assertIn('-', a)
        self.assertNotIn('*', a)

    def test_core_strand_create_from_str(self):
        s1 = Strand.get_strand_from_string('+')
        s2 = Strand.get_strand_from_string('plus')
        self.assertEqual(s1, s2)
        self.assertIsNotNone(s1)
        self.assertEqual(s1, Strand.plus)
        s1 = Strand.get_strand_from_string('-')
        s2 = Strand.get_strand_from_string('minus')
        self.assertEqual(s1, s2)
        self.assertIsNotNone(s1)
        self.assertEqual(s1, Strand.minus)

    def test_core_strand_create_from_wrong_str(self):
        with self.assertRaises(AttributeError):
            Strand.get_strand_from_string('foo')
            Strand.get_strand_from_string({})
            Strand.get_region_from_string([])
            Strand.get_region_from_string(1)
            Strand.get_region_from_string(-1.3)
            Strand.get_region_from_string(None)

    def test_core_strand_get_str(self):
        self.assertEqual('+', Strand.get_string_value(Strand.plus))
        self.assertEqual('-', Strand.get_string_value(Strand.minus))

#launch tests
if __name__ == '__main__':
    #see -- https://docs.python.org/3/library/unittest.html
    # to trigger only these tests
    # cd /path/to/srnatabac/src
    # python3.4 -m unittest srnatabac/tests/test_core.py -v
    unittest.main()

#
# Copyright (C) 2016 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2016 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import unittest
from srnatabac.core.rna_feature import ncRNA,mRNA
from srnatabac.core.strand import Strand
class TestSrnatabacCoreRNAFeature(unittest.TestCase):
    """ RNA Feature tests """

    def setUp(self):
        """ Before each test """
        self._ncrna = ncRNA(1, 0, 1, Strand.plus)
        self._mrna = mRNA(1, 0, 1, Strand.plus)

    def test_core_ncrna_create(self):
        self.assertIsNotNone(self._ncrna)
        self.assertIsInstance(self._ncrna, ncRNA)
        self.assertIsNone(self._ncrna.sequence)

    def test_core_mrna_create(self):
        self.assertIsNotNone(self._mrna)
        self.assertIsInstance(self._mrna, mRNA)
        self.assertIsNone(self._mrna.sequence)
        self.assertIsNone(self._mrna.gene_start)
        self.assertIsNone(self._mrna.gene_stop)

    def test_core_mrna_create_with_wrong_gene_info(self):
        with self.assertRaises(AttributeError):
            mRNA(1, 0, 1, Strand.plus, gene_stop=1)
            mRNA(1, 0, 1, Strand.plus, gene_start=1)
            mRNA(1, 0, 1, Strand.plus, gene_start=1, gene_stop=0)

    def test_core_rna_create_without_args(self):
        with self.assertRaises(TypeError):
            ncRNA()
            mRNA()

    def test_core_ncrna_create_without_strand(self):
        with self.assertRaises(TypeError):
            ncRNA(1)
            ncRNA(1, 0)
            ncRNA(1, 0, 1)
            ncRNA(1, 0, 1, '-')
            ncRNA(1, 0, 1, 1)
            ncRNA(1, 0, 1, [])
            ncRNA(1, 0, 1, {})

    def test_core_ncrna_len(self):
        self.assertEqual(len(self._ncrna), 1)
        self._ncrna.start = 2
        self._ncrna.stop = 5
        self.assertEqual(len(self._ncrna), 3)

    def test_core_mrna_gene_len(self):
        self._mrna.gene_start = 1
        self._mrna.gene_stop = 5
        self.assertEqual(len(self._mrna), 1)
        self.assertEqual(self._mrna.get_gene_length(), 4)

    def test_core_ncrna_create_wrong_start_stop(self):
        with self.assertRaises(ValueError):
            ncRNA(1, 1, 0, Strand.plus)
            mRNA(1, 1, 0, Strand.plus)

    def tearDown(self):
        """ After each test """
        del self._ncrna

#launch tests
if __name__ == '__main__':
    #see -- https://docs.python.org/3/library/unittest.html
    unittest.main()

#
# Copyright (C) 2016 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2016 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import unittest
import unittest.mock

from srnatabac.dao.rna_handler import RNAHandler
#see -- https://docs.python.org/3/library/unittest.html
class TestSrnatabacDaoRnaHandler(unittest.TestCase):

    def setUp(self):
        """ Before each test """
        self._rna = RNAHandler()

    def test_dao_toto(self):
        self.assertEqual(0, 0)

    def tearDown(self):
        """ After each test """
        del self._rna

#launch tests
if __name__ == '__main__':
    unittest.main()

#
# Copyright (C) 2016 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2016 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import unittest
from srnatabac.core.authkey import Authkey, UserAuthkey, ProjectAuthkey, CompleteAuthkey
class TestSrnatabacCoreAuthkey(unittest.TestCase):
    """ Authkey tests """

    def setUp(self):
        """ Before each test """
        self._authkey = Authkey()

    def test_core_authkey_uuid_create(self):
        self.assertIsNotNone(self._authkey)
        self.assertIsInstance(self._authkey, Authkey)
        a = Authkey.new_authkey()
        self.assertIsNotNone(a)
        self.assertIsInstance(a, str)

    def test_core_authkey_create(self):
        self.assertIsNotNone(self._authkey)
        self.assertIsInstance(self._authkey, Authkey)
        self.assertIsInstance(self._authkey.get_key(), str)

    def test_core_authkey_create_with_name(self):
        same_name = 'foooo'
        self._authkey = Authkey(same_name)
        self.assertIsNotNone(self._authkey)
        self.assertIsInstance(self._authkey, Authkey)
        self.assertIsInstance(self._authkey.get_key(), str)
        self.assertEqual(self._authkey.get_key(), same_name)

    def test_core_authkey_create_with_name_none(self):
        self._authkey = Authkey(None)
        self.assertIsNotNone(self._authkey)
        self.assertIsInstance(self._authkey, Authkey)
        self.assertNotEqual(self._authkey.get_key(), None)
        self.assertNotEqual(self._authkey.get_key(), '')
        self._authkey = Authkey('')
        self.assertIsNotNone(self._authkey)
        self.assertIsInstance(self._authkey, Authkey)
        self.assertNotEqual(self._authkey.get_key(), None)
        self.assertNotEqual(self._authkey.get_key(), '')

    def test_core_authkey_create_with_wrong_params(self):

        self.assertRaises(TypeError, Authkey().__init__, -1.5)
        self.assertRaises(TypeError, Authkey().__init__, -1)
        self.assertRaises(TypeError, Authkey().__init__, 1.5)
        self.assertRaises(TypeError, Authkey().__init__, 13)
        self.assertRaises(TypeError, Authkey().__init__, {})
        self.assertRaises(TypeError, Authkey().__init__, [])

    def test_core_authkey_other_compare(self):
        same_name = 'foooo'
        self._authkey = Authkey(same_name)
        other_authkey = Authkey(same_name)
        self.assertEqual(self._authkey.get_key(), other_authkey.get_key())
        with self.assertRaises(NotImplementedError):
            self._authkey == other_authkey
            other_authkey == self._authkey
            self._authkey != other_authkey
            other_authkey != self._authkey

    def test_core_authkey_create_static(self):
        a = Authkey.new_authkey()
        self.assertNotEqual(a, None)
        self.assertNotEqual(a, '')
        self.assertIsInstance(a, str)

    def tearDown(self):
        """ After each test """
        del self._authkey

class TestSrnatabacCoreUserAuthkey(unittest.TestCase):
    """ UserAuthkey tests """

    def setUp(self):
        """ Before each test """
        self._user_authkey = UserAuthkey()

    def test_core_userauthkey_create(self):
        self.assertIsNotNone(self._user_authkey)
        self.assertIsInstance(self._user_authkey, UserAuthkey)
        self.assertIsInstance(self._user_authkey.get_key(), str)

    def test_core_userauthkey_create_with_name(self):
        same_name = 'foooo'
        self._user_authkey = UserAuthkey(same_name)
        self.assertIsNotNone(self._user_authkey)
        self.assertIsInstance(self._user_authkey, UserAuthkey)
        self.assertIsInstance(self._user_authkey.get_key(), str)
        self.assertEqual(self._user_authkey.get_key(), same_name)

    def test_core_userauthkey_create_with_name_none(self):
        self._user_authkey = UserAuthkey(None)
        self.assertIsNotNone(self._user_authkey)
        self.assertIsInstance(self._user_authkey, UserAuthkey)
        self.assertNotEqual(self._user_authkey.get_key(), None)
        self.assertNotEqual(self._user_authkey.get_key(), '')
        self._user_authkey = UserAuthkey('')
        self.assertIsNotNone(self._user_authkey)
        self.assertIsInstance(self._user_authkey, UserAuthkey)
        self.assertNotEqual(self._user_authkey.get_key(), None)
        self.assertNotEqual(self._user_authkey.get_key(), '')

    def test_core_userauthkey_create_with_wrong_params(self):
        with self.assertRaises(TypeError):
            UserAuthkey(-1.5)
            UserAuthkey(-1)
            UserAuthkey(1.5)
            UserAuthkey(1)
            UserAuthkey({})
            UserAuthkey([])

    def test_core_userauthkey_same(self):
        same_name = 'foooo'
        self._user_authkey = UserAuthkey(same_name)
        other_authkey = UserAuthkey(same_name)
        self.assertIsInstance(self._user_authkey, UserAuthkey)
        self.assertIsInstance(other_authkey, UserAuthkey)
        self.assertEqual(self._user_authkey.get_key(), other_authkey.get_key())
        self.assertEqual(self._user_authkey, other_authkey)
        self.assertTrue(self._user_authkey == other_authkey)
        self.assertFalse(self._user_authkey != other_authkey)

    def test_core_userauthkey_not_the_same(self):
        other_authkey = UserAuthkey('foooo')
        self.assertIsInstance(self._user_authkey, UserAuthkey)
        self.assertIsInstance(other_authkey, UserAuthkey)
        self.assertNotEqual(self._user_authkey.get_key(), other_authkey.get_key())
        self.assertNotEqual(self._user_authkey, other_authkey)
        self.assertTrue(self._user_authkey != other_authkey)
        self.assertFalse(self._user_authkey == other_authkey)

    def test_core_userauthkey_compare_to_bad_type_other(self):
        with self.assertRaises(TypeError):
            self._user_authkey == 'foooo'
            self._user_authkey == 15
            self._user_authkey == []
            self._user_authkey == {}
            self._user_authkey == ProjectAuthkey()
            self._user_authkey == CompleteAuthkey(self._user_authkey, ProjectAuthkey())

    def test_core_userauthkey_compare_to_none(self):
        self.assertTrue(self._user_authkey != None)
        self.assertFalse(self._user_authkey == None)

    def tearDown(self):
        """ After each test """
        del self._user_authkey

class TestSrnatabacCoreProjectAuthkey(unittest.TestCase):
    """ ProjectAuthkey tests, same as UserAuthkey """

    def setUp(self):
        """ Before each test """
        self._project_authkey = ProjectAuthkey()

    def test_core_projectauthkey_create(self):
        self.assertIsNotNone(self._project_authkey)
        self.assertIsInstance(self._project_authkey, ProjectAuthkey)
        self.assertIsInstance(self._project_authkey.get_key(), str)

    def test_core_projectauthkey_create_with_name(self):
        same_name = 'foooo'
        self._project_authkey = ProjectAuthkey(same_name)
        self.assertIsNotNone(self._project_authkey)
        self.assertIsInstance(self._project_authkey, ProjectAuthkey)
        self.assertIsInstance(self._project_authkey.get_key(), str)
        self.assertEqual(self._project_authkey.get_key(), same_name)

    def test_core_projectauthkey_create_with_name_none(self):
        self._project_authkey = ProjectAuthkey(None)
        self.assertIsNotNone(self._project_authkey)
        self.assertIsInstance(self._project_authkey, ProjectAuthkey)
        self.assertNotEqual(self._project_authkey.get_key(), None)
        self.assertNotEqual(self._project_authkey.get_key(), '')
        self._project_authkey = ProjectAuthkey('')
        self.assertIsNotNone(self._project_authkey)
        self.assertIsInstance(self._project_authkey, ProjectAuthkey)
        self.assertNotEqual(self._project_authkey.get_key(), None)
        self.assertNotEqual(self._project_authkey.get_key(), '')

    def test_core_projectauthkey_create_with_wrong_params(self):
        with self.assertRaises(TypeError):
            ProjectAuthkey(-1.5)
            ProjectAuthkey(-1)
            ProjectAuthkey(1.5)
            ProjectAuthkey(1)
            ProjectAuthkey({})
            ProjectAuthkey([])

    def test_core_projectauthkey_same(self):
        same_name = 'foooo'
        self._project_authkey = ProjectAuthkey(same_name)
        other_authkey = ProjectAuthkey(same_name)
        self.assertIsInstance(self._project_authkey, ProjectAuthkey)
        self.assertIsInstance(other_authkey, ProjectAuthkey)
        self.assertEqual(self._project_authkey.get_key(), other_authkey.get_key())
        self.assertEqual(self._project_authkey, other_authkey)
        self.assertTrue(self._project_authkey == other_authkey)
        self.assertFalse(self._project_authkey != other_authkey)

    def test_core_projectauthkey_not_the_same(self):
        other_authkey = ProjectAuthkey('foooo')
        self.assertIsInstance(self._project_authkey, ProjectAuthkey)
        self.assertIsInstance(other_authkey, ProjectAuthkey)
        self.assertNotEqual(self._project_authkey.get_key(), other_authkey.get_key())
        self.assertNotEqual(self._project_authkey, other_authkey)
        self.assertTrue(self._project_authkey != other_authkey)
        self.assertFalse(self._project_authkey == other_authkey)

    def test_core_projectauthkey_compare_to_bad_type_other(self):
        with self.assertRaises(TypeError):
            self._project_authkey == 'foooo'
            self._project_authkey == 15
            self._project_authkey == []
            self._project_authkey == {}
            self._project_authkey == UserAuthkey()
            self._project_authkey == CompleteAuthkey(UserAuthkey(), self._project_authkey)

    def test_core_projectauthkey_compare_to_none(self):
        self.assertTrue(self._project_authkey != None)
        self.assertFalse(self._project_authkey == None)

    def tearDown(self):
        """ After each test """
        del self._project_authkey

class TestSrnatabacCoreCompleteAuthkey(unittest.TestCase):
    """ CompleteAuthkey tests """

    def setUp(self):
        """ Before each test """
        self._complete_authkey = CompleteAuthkey(
            UserAuthkey(), ProjectAuthkey()
        )

    def test_core_completeauthkey_create(self):
        self.assertIsNotNone(self._complete_authkey)
        self.assertIsInstance(self._complete_authkey, CompleteAuthkey)

    def test_core_completeauthkey_create_without_args(self):
        with self.assertRaises(TypeError):
            CompleteAuthkey()

    def test_core_completeauthkey_create_reversed_args(self):
        with self.assertRaises(TypeError):
            CompleteAuthkey(ProjectAuthkey(), UserAuthkey())

    def test_core_completeauthkey_create_with_wrong_args(self):
        with self.assertRaises(TypeError):
            CompleteAuthkey(UserAuthkey(), None)
            CompleteAuthkey(None, ProjectAuthkey())
            CompleteAuthkey(None, None)
            CompleteAuthkey(-1.5, ProjectAuthkey())
            CompleteAuthkey(-1, ProjectAuthkey())
            CompleteAuthkey(1.5, ProjectAuthkey())
            CompleteAuthkey(1, ProjectAuthkey())
            CompleteAuthkey([], ProjectAuthkey())
            CompleteAuthkey({}, ProjectAuthkey())
            CompleteAuthkey(UserAuthkey(), -1.5)
            CompleteAuthkey(UserAuthkey(), -1)
            CompleteAuthkey(UserAuthkey(), 1.5)
            CompleteAuthkey(UserAuthkey(), 1)
            CompleteAuthkey(UserAuthkey(), [])
            CompleteAuthkey(UserAuthkey(), {})

    def test_core_completeauthkey_attributes(self):
        ua = self._complete_authkey.get_user_part()
        pa = self._complete_authkey.get_project_part()
        self.assertIsInstance(ua, UserAuthkey)
        self.assertIsInstance(pa, ProjectAuthkey)

    def test_core_completeauthkey_same(self):
        uk = 'userkey'
        pk = 'projectkey'
        self._complete_authkey = CompleteAuthkey(
            UserAuthkey(uk), ProjectAuthkey(pk)
        )
        other_authkey = CompleteAuthkey(
            UserAuthkey(uk), ProjectAuthkey(pk)
        )
        self.assertIsInstance(self._complete_authkey, CompleteAuthkey)
        self.assertIsInstance(other_authkey, CompleteAuthkey)
        self.assertEqual(self._complete_authkey, other_authkey)
        self.assertTrue(self._complete_authkey == other_authkey)
        self.assertFalse(self._complete_authkey != other_authkey)

    def test_core_completeauthkey_not_the_same(self):
        other_authkey = CompleteAuthkey(
            UserAuthkey(), ProjectAuthkey()
        )
        self.assertIsInstance(self._complete_authkey, CompleteAuthkey)
        self.assertIsInstance(other_authkey, CompleteAuthkey)
        self.assertNotEqual(self._complete_authkey, other_authkey)
        self.assertTrue(self._complete_authkey != other_authkey)
        self.assertFalse(self._complete_authkey == other_authkey)

    def test_core_completeauthkey_compare_to_bad_type_other(self):
        with self.assertRaises(TypeError):
            self._complete_authkey == 'foooo'
            self._complete_authkey == 15
            self._complete_authkey == []
            self._complete_authkey == {}
            self._complete_authkey == UserAuthkey()
            self._complete_authkey == ProjectAuthkey()
            self._complete_authkey == CompleteAuthkey(self._complete_authkey.get_user_part(), ProjectAuthkey())
            self._complete_authkey == CompleteAuthkey(UserAuthkey(), self._complete_authkey.get_project_part())

    def tearDown(self):
        """ After each test """
        del self._complete_authkey

#launch tests
if __name__ == '__main__':
    #see -- https://docs.python.org/3/library/unittest.html
    # to trigger only these tests
    # cd /path/to/srnatabac/src
    # python3.4 -m unittest srnatabac/tests/test_core.py -v
    unittest.main()

#
# Copyright (C) 2016 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2016 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import unittest
from srnatabac.core.region import Region

class TestSrnatabacCoreRegion(unittest.TestCase):
    """ Region tests """

    def test_core_region_create(self):
        self.assertIsNotNone(Region.region_3UTR)
        self.assertIsNotNone(Region.region_CDS)
        self.assertIsNotNone(Region.region_5UTR)

    def test_core_region_wrong_attribute(self):
        with self.assertRaises(AttributeError):
            Region.unknown_attribute

    def test_core_region_compare_eq(self):
        five = Region.region_5UTR
        three = Region.region_3UTR
        cds = Region.region_CDS
        self.assertEqual(five, Region.region_5UTR)
        self.assertEqual(three, Region.region_3UTR)
        self.assertEqual(cds, Region.region_CDS)
        self.assertNotEqual(five, Region.region_3UTR)
        self.assertNotEqual(five, Region.region_CDS)
        self.assertNotEqual(three, Region.region_5UTR)
        self.assertNotEqual(three, Region.region_CDS)
        self.assertNotEqual(cds, Region.region_5UTR)
        self.assertNotEqual(cds, Region.region_3UTR)

    def test_core_region_all_str(self):
        a = Region.get_all_string_values()
        self.assertEqual(len(a), 3)
        self.assertIn('5UTR', a)
        self.assertIn('3UTR', a)
        self.assertIn('CDS', a)

    def test_core_region_all_gff3_str(self):
        a = Region.get_all_gff3_values()
        self.assertEqual(len(a), 3)
        self.assertIn('five_prime_UTR', a)
        self.assertIn('three_prime_UTR', a)
        self.assertIn('CDS', a)

    def test_core_region_create_from_str(self):
        s1 = Region.get_region_from_string('five_prime_UTR')
        s2 = Region.get_region_from_string('5UTR')
        self.assertEqual(s1, s2)
        self.assertIsNotNone(s1)
        self.assertEqual(s1, Region.region_5UTR)
        s1 = Region.get_region_from_string('three_prime_UTR')
        s2 = Region.get_region_from_string('3UTR')
        self.assertEqual(s1, s2)
        self.assertIsNotNone(s1)
        self.assertEqual(s1, Region.region_3UTR)
        s1 = Region.get_region_from_string('CDS')
        self.assertIsNotNone(s1)
        self.assertEqual(s1, Region.region_CDS)

    def test_core_region_create_from_wrong_str(self):
        with self.assertRaises(AttributeError):
            Region.get_region_from_string('foo')
            Region.get_region_from_string({})
            Region.get_region_from_string([])
            Region.get_region_from_string(1)
            Region.get_region_from_string(-1.3)
            Region.get_region_from_string(None)

    def test_core_region_get_str(self):
        self.assertEqual('5UTR', Region.get_string_value(Region.region_5UTR))
        self.assertEqual('3UTR', Region.get_string_value(Region.region_3UTR))
        self.assertEqual('CDS', Region.get_string_value(Region.region_CDS))

    def tes_core_region_get_gff3_value(self):
        self.assertEqual('five_prime_UTR', Region.get_gff3_value(Region.region_5UTR))
        self.assertEqual('three_prime_UTR', Region.get_gff3_value(Region.region_3UTR))
        self.assertEqual('CDS', Region.get_gff3_value(Region.region_CDS))

#launch tests
if __name__ == '__main__':
    #see -- https://docs.python.org/3/library/unittest.html
    # to trigger only these tests
    # cd /path/to/srnatabac/src
    # python3.4 -m unittest srnatabac/tests/test_core.py -v
    unittest.main()

#
# Copyright (C) 2016 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2016 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

from json import JSONEncoder
from srnatabac.utils.srnatabac_config_reader import SrnatabacConfigReader
import datetime


class SRNATabacJSONEncoder (JSONEncoder):

    def default(self, obj):
        from srnatabac.core.interaction import Interaction,InteractionDetail
        from srnatabac.core.rna_feature import ncRNA, mRNA
        from srnatabac.core.region import Region
        from srnatabac.core.strand import Strand
        from srnatabac.core.user_information import UserInformation, UserProject

        if isinstance(obj, (datetime.date, datetime.datetime)):
            return obj.strftime(SrnatabacConfigReader().get_date_format())
        elif isinstance(obj, Interaction):
            return obj.__dict__
        elif isinstance(obj, InteractionDetail):
            return obj.__dict__
        elif isinstance(obj, ncRNA):
            return obj.__dict__
        elif isinstance(obj, mRNA):
            return obj.__dict__
        elif isinstance(obj, UserInformation):
            return obj.__dict__
        elif isinstance(obj, UserProject):
            return obj.__dict__
        elif isinstance(obj, Region):
            return Region.get_string_value(obj)
        elif isinstance(obj, Strand):
            return Strand.get_string_value(obj)
        else:
            return JSONEncoder.default(self, obj)

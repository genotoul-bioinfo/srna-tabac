#
# Copyright (C) 2016 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2016 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'


import os
import os.path
import subprocess
from functools import wraps

from srnatabac.core.interaction import Interaction, InteractionDetail
from srnatabac.core.region import Region
from srnatabac.core.strand import Strand

# from jflow.featurelibio.gff3 import GFF3Record
# import jflow
import jflow.parameter as jflow_parameter
from jflow.workflows_manager import WorkflowsManager
from srnatabac.utils.srnatabac_config_reader import SrnatabacConfigReader
from srnatabac.utils.gff3_feature_manager import GFF3FeatureManager, GFF3Reader

class AbstractFile:
    """ Abstract file format, testing purpose only """
    def __init__(self, file_path):
        if not os.path.exists(file_path):
            raise FileNotFoundError
        self._file = file_path
        self._features = None

    def get_features(self):
        return self._features

    def get_feature_by_id(self, pid):
        raise NotImplementedError

    def has_same_record(self, other_record):
        return self.get_features_by_id(other_record.id) is not None

    def has_same_record(self, other_record):
        """ The purpose here is to cmp contents """
        raise NotImplementedError

    def _is_same_file_content(self, other_file):
        """ bidirectionnal check """
        are_same = True
        for f in self.get_features():
            if not other_file.has_same_record(f):
                are_same = False
                break
        for f in other_file.get_features():
            if not self.has_same_record(f):
                are_same = False
                break
        return are_same

    def __eq__(self, other):
        """ The purpose here is to cmp contents """
        raise NotImplementedError


class GFF3File(AbstractFile):
    """ GFF3 file format given in input for GFF3Predict workflow """
    def __init_(self, gff3_path):
        super(GFF3File, self).__init__(gff3_path)
        #get directives and records
        #test features and directives
        directives = []
        self._features = []
        valid_types = ['ncRNA', 'mRNA', 'CDS', 'five_prime_UTR', 'three_prime_UTR', 'gene']
        gff3_reader = GFF3Reader(gff3_path, valid_types)
        nb_seq = 0
        try:
            for (rec_type, rec_content) in gff3_reader:
                if rec_type == 'feature':
                    self._features.append(rec_content)
                    # only check the first 10 sequences
                    if nb_seq < 10:
                        rec_content.seq_id
                        rec_content.source
                        rec_content.type
                        rec_content.start
                        rec_content.end
                        rec_content.score
                        rec_content.strand
                        rec_content.phase
                        rec_content.attributes
                        nb_seq += 1
                elif rec_type == 'directive':
                    directives.append(rec_content)
        except:
            raise jflow.InvalidFormatError("The provided file '{}' is not a gff3 file!".format(gff3_path))
        finally:
            gff3_reader.close()

        if '##gff-version 3' not in directives:
            raise Exception("The provided file '{}' is not a gff3 file!\nMiss directive '##gff-version 3' at the beginning".format(gff3_path))
        #manager
        self._record_manager = GFF3FeatureManager(self._features)

    def get_feature_by_id(self, pid):
        return self._record_manager.get_record_by_id(other_record.attributes['ID'])

    def __eq__(self, other_gff3_file):
        """ Bidirectionnal check """
        if not isinstance(other_gff3_file, GFF3File):
            raise TypeError
        return self._is_same_file_content(other_gff3_file)

class PGZInteractionFile(AbstractFile):
    """ PGZ file format produced in output by all workflows """
    def __init_(self, pgz_path):
        super(PGZInteractionFile, self).__init__(pgz_path)
        self._features = []
        with gzip.open(pgz_path, 'rb') as f:
            self._features.extend(pickle.load(f))

    def get_feature_by_id(self, pid):
        if not isinstance(pid, int):
            raise TypeError
        found = None
        for r in self.get_features():
            if r.id == pid:
                found = r
                break
        return found

    def __eq__(self, other_pgz):
        """ Bidirectionnal check """
        if not isinstance(other_pgz, PGZInteractionFile):
            raise TypeError
        return self._is_same_file_content(other_pgz)

class TestUtilsWorkflow:
    """ Copy and modify parameters from a Workflow to execute it in cli """

    def __init__(self, wfname):
        self._workflow = self._get_workflow(wfname)
        self._srna_cli_path = os.path.join(os.path.abspath('.'), 'bin', 'srnatabac.py')
        self._output_dir = os.path.abspath(os.path.join(SrnatabacConfigReader().get_work_directory(), wfname))
        self._parameters = self.get_wf_parameters()

    def _get_workflow(self, wfname):
        """ Get a workflow by name """
        wf_found = None
        for wf in WorkflowsManager().get_available_workflows()[0]:
            if wf.get_name() == wfname:
                wf_found = wf
                break
        if wf_found is None:
            raise AttributeError('Workflow "{}" not found'.format(wfname))
        return wf_found

    def get_parameters(self):
        """ Get the modified parameters types """
        return self._parameters

    def get_parameter(self, pname):
        """ Get the modified parameters types """
        return self._parameters[pname]

    def set_parameter(self, pname, pval):
        """ Set a parameter """
        self._parameters[pname]['value'] = pval

    def set_user_email(self, email):
        self.set_parameter('user-email', email)

    @staticmethod
    def get_test_data_dir():
        """ Get the static data directory for tests purpose """
        return os.path.join(os.path.abspath('.'), 'workflows', 'predict', 'data', 'for_tests')

    def get_last_output_path(self):
        """ Get the path to the output directory of a workflow """
        a = None
        if os.path.exists(self._output_dir):
            a = { int(x[2:]):x for x in os.listdir(self._output_dir) if os.path.isdir(os.path.join(self._output_dir, x)) }
        return None if not a else os.path.join(self._output_dir, a[max(a)])

    def _get_default_parameters(self, func=None):
        """ Get parameters of based on a parameter dict and a filter function """
        return {
            p.name.replace('_', '-'): {
                'type': p.get_type(),
                'value': p.default
            } for p in ( filter(func, self._workflow.get_parameters()) if func is not None else self._workflow.get_parameters())
        }

    def get_wf_parameters(self):
        """ Get the workflow parameters types """
        return self._get_default_parameters()

    def get_wf_required_parameters(self):
        """ Get ONLY required parameters from the workflow """
        return self._get_default_parameters( lambda param: param.required )

    def controls(self):
        """ Control parameters before calling the workflow """
        raise NotImplementedError

    # def preprocess(control_func):
    #     def decorator(func):
    #         @wraps(func)
    #         def wrapper(*args, **kwargs):
    #             try:
    #                 control_func()
    #             except NotImplementedError:
    #                 pass
    #             return func(*args, **kwargs)
    #         return wrapper
    #     return decorator

    @preprocess(controls)
    def call(self):
        res = []
        try:
            self.controls()
        except NotImplementedError:
            pass
        required_params = list(self.get_wf_required_parameters().keys())
        for pname, pdesc in self._parameters.items():
            if pname in required_params and pdesc['value'] is None:
                raise Exception('Parameter "{}" needs to be initialized'.format(pname))
            if pdesc['type'] == 'bool':
                if pdesc['value'] == True:
                    res.append('--{}'.format(pname))
            else:
                res.append('--{}={}'.format(pname, pdesc['value']))
        print( ' '.join([ 'python', self._srna_cli_path, self._workflow.name ] + res) )
        return subprocess.call([ 'python', self._srna_cli_path, self._workflow.name ] + res)

class TestUtilsGFF3Workflow(TestUtilsWorkflow):
    """ Add specificity to call for GFF3 predict """

    def __init__(self):
        super(TestUtilsGFF3Workflow, self).__init__('predictgff3')

    #softs
    def _enable_soft(self, sname): self._parameters[sname]['value'] = True
    def _disable_soft(self, sname): self._parameters[sname]['value'] = False
    def enable_intarna(self): self._enable_soft('intarna')
    def disable_intarna(self): self._disable_soft('intarna')
    def enable_ssearch(self): self._enable_soft('ssearch')
    def disable_ssearch(self): self._disable_soft('ssearch')
    def enable_risearch(self): self._enable_soft('risearch')
    def disable_risearch(self): self._disable_soft('risearch')
    def enable_rnacofold(self): self._enable_soft('rnacofold')
    def disable_rnacofold(self): self._disable_soft('rnacofold')
    def enable_rnaplex(self): self._enable_soft('rnaplex')
    def disable_rnaplex(self): self._disable_soft('rnaplex')

    #region
    def _enable_region(self, rname, b=None, a=None):
        if b is None or not isinstance(b, int):
            raise AttributeError
        if a is None or not isinstance(a, int):
            raise AttributeError
        self._parameters['predict-{}'.format(rname)]['value'] = True
        try:
            self._parameters['before-{}'.format(rname)]['value'] = b
            self._parameters['after-{}'.format(rname)]['value'] = a
        except KeyError:
            self._parameters['before-{}'.format(rname)] = {'value': b, 'type': 'int'}
            self._parameters['after-{}'.format(rname)] = {'value': a, 'type': 'int'}

    def enable_5utr(self, b5=None, a5=None):
        self._enable_region('5utr', b5, a5)
    def enable_3utr(self, b3=None, a3=None):
        self._enable_region('3utr', b3, a3)

    def _disable_region(self, rname):
        self._parameters['predict-{}'.format(rname)]['value'] = False
        del self._parameters['before-{}'.format(rname)]
        del self._parameters['after-{}'.format(rname)]

    def disable_5utr(self):
        self._disable_region('5utr')
    def disable_3utr(self):
        self._disable_region('3utr')

    def enable_cds(self):
        self._parameters['predict-cds']['value'] = True
    def disable_cds(self):
        self._parameters['predict-cds']['value'] = False

    def set_input_gff3(self, fname):
        self._parameters['input-file-one']['value'] = fname
    def set_input_genome_ref(self, fname):
        self._parameters['input-file-two']['value'] = fname
    def set_project_name(self, pname):
        self._parameters['project-name']['value'] = pname

    def controls(self):
        #need to have at least on region selected
        want_3utr = self._parameters['predict-3utr']['value']
        want_5utr = self._parameters['predict-5utr']['value']
        want_cds = self._parameters['predict-cds']['value']
        if not want_cds and not want_5utr and not want_3utr:
            raise Exception('Need at least to lookup one region among 5UTR/CDS/3UTR')
        #deleted useless param
        if not want_5utr:
            del self._parameters['before-5utr']
            del self._parameters['after-5utr']
        if not want_3utr:
            del self._parameters['before-3utr']
            del self._parameters['after-3utr']
        #need at least one software selected
        s = {}
        s['inta'] = self._parameters['intarna']['value']
        s['rise'] = self._parameters['risearch']['value']
        s['ssea'] = self._parameters['ssearch']['value']
        s['rnac'] = self._parameters['rnacofold']['value']
        s['rnap'] = self._parameters['rnaplex']['value']
        if True not in list(s.values()):
            raise Exception('Need at least to use one software')

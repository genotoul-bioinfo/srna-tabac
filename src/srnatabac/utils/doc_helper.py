#
# Copyright (C) 2016 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2016 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'
class DocHelper(object):

    @staticmethod
    def get_softwares():
        return [{
                'type': 'software',
                'name': 'Python',
                'version': {
                    'current': '>= 3.2',
                    'supported': '>= 3.2'
                },
                'link': 'https://www.python.org/',
                'description': 'Programming language',
            }, {
                'type': 'software',
                'name': 'Cherrypy',
                'version': {
                    'current': '>= 3',
                    'supported': '>= 3'
                },
                'link': 'http://www.cherrypy.org/',
                'description': 'Python web framework',
            }, {
                'type': 'software',
                'name': 'Jinja2',
                'version': {
                    'current': '2',
                    'supported': '2'
                },
                'link': 'http://jinja.pocoo.org/',
                'description': 'Python template engine',
            }, {
                'type': 'software',
                'name': 'Makeflow',
                'version': {
                    'current': '5.4.3',
                    'supported': '5.4.3'
                },
                'link': 'http://www.cse.nd.edu/~ccl/software/makeflow/',
                'description': 'Workflow engine for executing large complex workflows on clusters, clouds, and grids',
            }, {
                'type': 'predictor',
                'name': 'IntaRNA',
                'call_name':'IntaRNA',
                'version': {
                    'current': '1.2.5',
                    'supported': '1.2.5'
                },
                'link': 'http://www.bioinf.uni-freiburg.de/Software/#IntaRNA-download',
                'description': 'Target prediction including accessibility of interaction sites',
                'args': [{
                    'name': '-m',
                    'default': 'fasta file',
                    'description': 'use fasta file of binding sequences'
                },{
                    'name': '-t',
                    'default': 'fasta file',
                    'description': 'use fasta file of target sequences'
                },{
                    'name': '-o',
                    'default': '',
                    'description': 'detailed output'
                },{
                    'name': '-w',
                    'default': '100',
                    'description': '[only when sequences are > 100] window size for computation of ED values'
                }]
            }, {
                'type': 'predictor',
                'name': 'RIsearch',
                'call_name':'RIsearch',
                'version': {
                    'current': '1.1',
                    'supported': '1.1'
                },
                'link': 'http://rth.dk/resources/risearch/',
                'description': 'Compute interaction with a simplified nearest-neighbor energy model',
                'args': [{
                    'name': '-q',
                    'default': 'fasta file',
                    'description': 'fasta file containing query sequence(s)'
                },{
                    'name': '-t',
                    'default': 'fasta file',
                    'description': 'fasta file containing target sequence(s)'
                },{
                    'name': '-d',
                    'default': '30',
                    'description': 'per-nucleotide extension penalty given in dacal/mol'
                },{
                    'name': '-m',
                    'default': 't04',
                    'description': 'matrix to use, t99 or t04'
                },{
                    'name': '-l',
                    'default': '700',
                    'description': ' max trace back length'
                }]
            }, {
                'type': 'predictor',
                'name': 'Ssearch',
                'call_name':'ssearch36',
                'version': {
                    'current': '36.3.8',
                    'supported': '>= 36.3.6'
                },
                'link': 'http://faculty.virginia.edu/wrpearson/fasta/fasta36/',
                'description': 'Compute interaction based on matrix-hybridation',
                'args':[{
                    'name': '-1',
                    'default': '',
                    'description': 'Sort by "init1" score'
                },{
                    'name': '-3',
                    'default': '',
                    'description': 'use only forward frame translations'
                },{
                    'name': '-f',
                    'default': '',
                    'description': 'gap-open penalty'
                },{
                    'name': '-g',
                    'default': '',
                    'description': 'gap-extension penalty'
                },{
                    'name': '-w',
                    'default': '500',
                    'description': 'line width for similarity score, sequence alignment, output'
                },{
                    'name': '-W',
                    'default': '0',
                    'description': 'context length for alignment that provide additional sequence context'
                },{
                    'name': '-s',
                    'default': 'matrix file',
                    'description': 'substitution matrix'
                },{
                    'name': '-E',
                    'default': '10000',
                    'description': 'expectation value upper limit for score and alignment display'
                },{
                    'name': '-T',
                    'default': '1',
                    'description': '[only on sge system] number of threads or workers to use'
                }]
            }, {
                'type': 'package',
                'name': 'Package ViennaRNA',
                'version': {
                    'current': '2.2.4',
                    'supported': '>= 2.2.0'
                },
                'link': 'http://www.tbi.univie.ac.at/RNA/',
                'description': 'Several libraries and programs for the prediction and comparison of RNA secondary structures',
                'inner_softwares': [{
                        'name':'RNAcofold',
                        'call_name':'RNAcofold',
                        'description': 'Calculate secondary structures of two RNAs with dimerization',
                        'type': 'predictor',
                        'args':[{
                            'name': '--noPS',
                            'default': '',
                            'description': 'Do not produce postscript output'
                        }]
                    }, {
                        'name':'RNAplex',
                        'call_name':'RNAplex',
                        'description': 'Compute interaction with hybridation model',
                        'type': 'predictor',
                        'args':[{
                            'name': '-q',
                            'default': 'fasta file',
                            'description': 'file containing the query sequence'
                        },{
                            'name': '-t',
                            'default': 'fasta file',
                            'description': 'file containing the target sequence'
                        }]
                    }, {
                        'name':'RNAeval',
                        'call_name':'RNAeval',
                        'description': 'Calculate energy of RNA sequences with given secondary structure',
                        'type': 'software'
                    }, {
                        'name':'RNAfold',
                        'call_name':'RNAfold',
                        'description': 'Calculates minimum free energy (mfe) structure for RNA',
                        'type': 'software'
                }]
            }
        ]

    @staticmethod
    def get_workflows_arguments():
        # all wf
        workflows_common_args = [{
            'name': '--project-name',
            'required': True,
            'type': 'str',
            'default': 'None',
            'description': 'The name of the project'
        },{
            'name': '--user-email',
            'required': True,
            'type': 'str',
            'default': 'None',
            'description': 'A valid email address'
        },{
            'name': '--align-output',
            'required': False,
            'type': 'bool',
            'default': False,
            'description': 'Do you wish an alignment-like file output?'
        }]

        # only predict wf
        workflows_predict_common_args = [{
            'name': '--matches',
            'required': True,
            'type': 'int',
            'default': 10,
            'description': 'Filter: the minimum length of matches wanted'
        },{
            'name': '--successive-matches',
            'required': True,
            'type': 'int',
            'default': 10,
            'description': 'Filter: the minimum length of successive matches wanted'
        },{
            'name': '--intarna',
            'required': False,
            'type': 'bool',
            'default': False,
            'description': 'Do you want to use IntaRNA?'
        },{
            'name': '--risearch',
            'required': False,
            'type': 'bool',
            'default': False,
            'description': 'Do you want to use RIsearch?'
        },{
            'name': '--rnaplex',
            'required': False,
            'type': 'bool',
            'default': False,
            'description': 'Do you want to use RNAplex?'
        },{
            'name': '--rnacofold',
            'required': False,
            'type': 'bool',
            'default': False,
            'description': 'Do you want to use RNAcofold?'
        },{
            'name': '--ssearch',
            'required': False,
            'type': 'bool',
            'default': False,
            'description': 'Do you want to use Ssearch?'
        }]

        load_workflow_args = [{
            'name': '--gff3',
            'required': True,
            'type': 'str',
            'default': 'None',
            'description': 'The path to the gff3 file'
        },{
            'name': '--reference-genome',
            'required': True,
            'type': 'str',
            'default': 'None',
            'description': 'The path to the genome reference file'
        }]
        load_workflow_args.extend(workflows_common_args)

        predict_fasta_workflow_args = [{
            'name': '--input-file-one',
            'required': True,
            'type': 'str',
            'default': 'None',
            'description': 'The path to the first Fasta file (ncRNA/queries)'
        },{
            'name': '--input-file-two',
            'required': True,
            'type': 'str',
            'default': 'None',
            'description': 'The path to the second Fasta file (mRNA/targets)'
        }]
        predict_fasta_workflow_args.extend(workflows_common_args)
        predict_fasta_workflow_args.extend(workflows_predict_common_args)

        predict_gff3_workflow_args = [{
            'name': '--input-file-one',
            'required': True,
            'type': 'str',
            'default': 'None',
            'description': 'The path to the GFF3 file'
        },{
            'name': '--input-file-two',
            'required': True,
            'type': 'str',
            'default': 'None',
            'description': 'The path to the genome reference Fasta file'
        },{
            'name': '--before-5utr',
            'required': True,
            'type': 'int',
            'default': 30,
            'description': 'Only if "five_prime_UTR" is not found in the GFF3: number of base to consider before CDS start to simulate a 5UTR region'
        },{
            'name': '--after-5utr',
            'required': True,
            'type': 'int',
            'default': 30,
            'description': 'Only if "five_prime_UTR" is not found in the GFF3: number of base to consider after CDS start to simulate a 5UTR region'
        },{
            'name': '--before-3utr',
            'required': True,
            'type': 'int',
            'default': 30,
            'description': 'Only if "three_prime_UTR" is not found in the GFF3: number of base to consider before CDS start to simulate a 3UTR region'
        },{
            'name': '--after-3utr',
            'required': True,
            'type': 'int',
            'default': 30,
            'description': 'Only if "three_prime_UTR" is not found in the GFF3: number of base to consider after CDS start to simulate a 3UTR region'
        }]
        predict_gff3_workflow_args.extend(workflows_common_args)
        predict_gff3_workflow_args.extend(workflows_predict_common_args)

        # all workflows
        # return [
        #     { 'name': 'predictfasta', 'args': predict_fasta_workflow_args },
        #     { 'name': 'predictgff3', 'args': predict_gff3_workflow_args },
        #     { 'name': 'load', 'args': load_workflow_args },
        # ]
        return {
            'predictfasta': predict_fasta_workflow_args,
            'predictgff3': predict_gff3_workflow_args,
            'load': load_workflow_args
        }

#
# Copyright (C) 2016 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2016 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.1'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

from jflow.featureio import GFF3Record
from srnatabac.core.exceptions.gff3_exceptions import GFF3WrongFormatError, GFF3PositionError

class GFF3Reader:
    """ GFF3 reader that will hide unwanted feature types """

    def __init__(self, file_path, valid_types=None):
        if not isinstance(valid_types, list):
            raise TypeError
        self._path = file_path
        self._handle = open(file_path, 'r')
        self._line = 0
        self._types = valid_types

    def __del__(self):
        self.close()

    def __iter__(self):
        for line in self._handle:
            line = line.rstrip()
            self._line += 1
            # blank line
            if line.strip() == '':
                continue
            # comment or directive line
            if line.startswith('#'):
                # all others directives
                if line.startswith('##'):
                    yield 'directive', line
                continue
            # try feature line
            try:
                gff_record = GFF3Record.fromGff(line)
            except:
                raise IOError("The line {} in '{}' cannot be parsed by {}.\nLine content: {}".format(
                    self._line, self._path, self.__class__.__name__, line))
            else:
                # only returns line when it matches certain types
                if gff_record.type in self._types:
                    yield 'feature', gff_record

    def close(self):
        if hasattr(self, '_handle') and self._handle is not None:
            self._handle.close()
            self._handle = None
            self._line = None
            self._types = None

class GFF3FeatureManager:
    """ A manager for GFF3Record (feature) list object
    It is much more efficient to initialize this wrapper with small GFF3Record list
    It should include a 'region'-type tag to allow you to retrieve 'is_circular' information

    Keyword attribute
    _records -- a GFF3Record list
    """

    def __init__(self, records):
        if not isinstance(records, list):
            raise TypeError
        if not records:
            raise ValueError('Cannot be an empty GFF3 records list')
        if not isinstance(records[0], GFF3Record):
            raise TypeError
        self._records = records

    def get_records(self):
        """ Getter """
        return self._records

    def _attr(self, rec, attr_name):
        """ Safely search attribute value from a GFF3 record

        Keyword argument:
        rec -- a GFF3 record
        attr_name -- the attribute name searched
        """
        if not isinstance(rec, GFF3Record):
            raise TypeError
        if not isinstance(attr_name, str) or attr_name == '':
            raise TypeError
        res = []
        try:
            res = rec.attributes[attr_name].split('%2C')
        except KeyError:
            pass
        return res

    def get_record_by_locus(self, locus_name):
        """ Get a record by the attribute 'locus_tag'
        Returns the first found

        Keyword argument:
        locus_name -- the searched locus name
        """
        if not isinstance(locus_name, str) or locus_name == '':
            raise TypeError
        found = None
        for r in self._records:
            try:
                if r.attributes['locus_tag'] == locus_name:
                    found = r
                    break
            except KeyError:
                pass
        return found

    def get_record_by_id(self, a_id):
        """ Get a record by the attribute 'ID'
        Returns the first found

        Keyword argument:
        a_id -- the searched ID attribute
        """
        if not isinstance(a_id, str) or a_id == '':
            raise TypeError
        found = None
        for r in self._records:
            try:
                if r.attributes['ID'] == a_id:
                    found = r
                    break
            except KeyError:
                pass
        return found

    def get_children(self, rec):
        """ Get the children list of a record

        Keyword argument:
        rec -- a GFF3 record
        """
        if not isinstance(rec, GFF3Record):
            raise TypeError
        res = []
        for r in self._records:
            try:
                if rec.attributes['ID'] in self._attr(r, 'Parent'):
                    res.append(r)
            except KeyError:
                pass
        return res

    def get_descendants(self, rec):
        """ Get all the descendants structure of a record
        @see print_descendants_tree()

        Keyword argument:
        rec -- a GFF3 record
        """
        if not isinstance(rec, GFF3Record):
            raise TypeError
        children = self.get_children(rec)
        res = { rec: None }
        if len(children) > 0:
            res = { rec: [] }
            for c in children:
                res[rec].append(self.get_descendants(c))
        return res

    def get_parents(self, rec):
        """ Get the parent of a record

        Keyword argument:
        rec -- a GFF3 record
        """
        if not isinstance(rec, GFF3Record):
            raise TypeError
        res = []
        for r in self._records:
            try:
                if r.attributes['ID'] in self._attr(rec, 'Parent'):
                    res.append(r)
            except KeyError:
                pass
        return res

    def get_ancestors(self, rec):
        """ Get the ancestor/root of a record

        Keyword argument:
        rec -- a GFF3 record
        """
        if not isinstance(rec, GFF3Record):
            raise TypeError
        parents = self.get_parents(rec)
        res = { rec: None }
        if len(parents) > 0:
            res = { rec: [] }
            for p in parents:
                res[rec].append(self.get_ancestors(p))
        return res

    def get_gene(self, rec):
        """ Get a the related gene-type record

        Keyword argument:
        rec -- the GFF3 record
        """
        if not isinstance(rec, GFF3Record):
            raise TypeError
        res = None
        if rec.type != 'gene':
            parents = self.get_parents(rec)
            for p in parents:
                if p.type == 'gene':
                    res = p
                    break
                res = self.get_gene(p)
        else:
            res = rec
        return res

    def get_locus_name(self, rec):
        """ Get the locus name of a record
        The locus_tag is only written in 'gene'-type records
        Might be useful to get the locus name for every descendant

        Keyword argument:
        rec -- a GFF3 record
        """
        if not isinstance(rec, GFF3Record):
            raise TypeError
        try:
            locus_name = self.get_gene(rec).attributes['locus_tag']
        except KeyError:
            locus_name = None
        return locus_name

    def get_records_of_type(self, record_type):
        """ Get a subset of records list

        Keyword argument:
        record_type -- a GFF3Record list
        """
        return [r for r in self._records if r.type == record_type]

    def has_type(self, record_type=None):
        """ Search for the existance of a specific type

        Keyword argument:
        record_type -- a GFF3Record list
        """
        res = False
        for r in self._records:
            if r.type == record_type:
                res = True
                break
        return res

    def print_descendants_tree(self, rec):
        """ Print the descendant structure based on a record

        Keyword argument:
        rec -- a GFF3 record
        """
        print('Descendants tree:')
        def _recursive_print(recs, tab=0):
            for r, children in recs.items():
                intro = '{}[{}]'.format('  ' * tab, r.type)
                print("{:<20} ({},{}) {} {}".format(intro, r.start, r.end, r.seq_id, r.attributes['ID'] if 'ID' in r.attributes else ''))
                if children is not None:
                    for c in children:
                        _recursive_print(c, tab+1)
        _recursive_print(self.get_descendants(rec))

    def print_ancestors_tree(self, rec):
        """ Print the ancestors structure based on a record

        Keyword argument:
        rec -- a GFF3 record
        """
        print('Ancestors tree:')
        def _recursive_print(recs, tab=0):
            for r, parents in recs.items():
                if parents is not None:
                    for p in parents:
                        _recursive_print(p, tab+1)
                intro = '{}[{}]'.format('  ' * tab, r.type)
                print("{:<20} ({},{}) {} {}".format(intro, r.start, r.end, r.seq_id, r.attributes['ID'] if 'ID' in r.attributes else ''))
        _recursive_print(self.get_ancestors(rec))

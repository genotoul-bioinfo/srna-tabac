#
# Copyright (C) 2016 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2016 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import jflow
import jflow.utils as utils
import os.path
from jinja2 import Environment, FileSystemLoader
import re

class Common(object):
    """ Common utils methods."""

    """ Predictors softwares """
    SOFTWARE_RNAPLEX = 'RNAplex'
    SOFTWARE_RNACOFOLD = 'RNAcofold'
    SOFTWARE_RISEARCH = 'RIsearch'
    SOFTWARE_SSEARCH = 'Ssearch'
    SOFTWARE_INTARNA = 'IntaRNA'

    @staticmethod
    def is_software_authorized(soft_to_test):
        return not not [x for x in map(str.lower, [Common.SOFTWARE_INTARNA, Common.SOFTWARE_RNAPLEX, Common.SOFTWARE_RNACOFOLD, Common.SOFTWARE_RISEARCH, Common.SOFTWARE_SSEARCH]) if soft_to_test.lower() == x]

    @staticmethod
    def multikeysort(items, columns):
        """ multikey sort, only for python3

        @see: http://stackoverflow.com/questions/1143671/python-sorting-list-of-dictionaries-by-multiple-keys
        @usage:
            b = [{u'TOT_PTS_Misc': u'Utley, Alex', u'Total_Points': 96.0},
                {u'TOT_PTS_Misc': u'Russo, Brandon', u'Total_Points': 96.0},
                {u'TOT_PTS_Misc': u'Chappell, Justin', u'Total_Points': 96.0}]

            a = multikeysort(b, ['-Total_Points', 'TOT_PTS_Misc'])

            will sort b by 'Total_Points' desc, 'TOT_PTS_Misc' asc, then put it in a.
        """
        from operator import itemgetter as i
        from functools import cmp_to_key
        def _comparer(left, right):
            _cmp = lambda a, b : (a > b) - (a < b)
            comparer_iter = (
                _cmp(fn(left), fn(right)) * mult
                for fn, mult in comparers
            )
            return next((result for result in comparer_iter if result), 0)
        comparers = [
            ((i(col[1:].strip()), -1) if col.startswith('-') else (i(col.strip()), 1))
            for col in columns
        ]
        return sorted(items, key=cmp_to_key(_comparer))


    """ Pickle compression rate, put it in cfg file

    @see: https://docs.python.org/2/library/pickle.html#data-stream-format

    Extract:
    Protocol version 0 is the original ASCII protocol and is backwards compatible with earlier versions of Python.
    Protocol version 1 is the old binary format which is also compatible with earlier versions of Python.
    Protocol version 2 was introduced in Python 2.3. It provides much more efficient pickling of new-style classes.
    If protocol is specified as a negative value or HIGHEST_PROTOCOL, the highest protocol version available will be used.
    """
    PICKLE_COMPRESSION_RATE = -1

    """ The template view folder."""
    TMPL_REP_PATH = "../ui/view/"

    @staticmethod
    def get_template(template, func = None):
        """Get the jinja template by a given path.
        Keywords arguments:
        template -- the template path
        """
        jinja_env = Environment(loader=FileSystemLoader(os.path.join(os.path.dirname(os.path.abspath(__file__)), Common.TMPL_REP_PATH)))
        if func != None:
            jinja_env.filters[func.__name__] = func
        return jinja_env.get_template( template )

class Regex (object):
    """ Common regex precompiled methods."""

    @staticmethod
    def email():
        return re.compile(
            r"(^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*"  # dot-atom
            r'|^"([\001-\010\013\014\016-\037!#-\[\]-\177]|\\[\001-011\013\014\016-\177])*"' # quoted-string
            r')@(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?$', re.IGNORECASE  # domain
        )

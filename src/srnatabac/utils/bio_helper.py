#
# Copyright (C) 2016 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2016 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.1'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'


class BioHelper(object):
    """ Static biological methods """

    @staticmethod
    def reverse_complement(sequence):
        """ Fast reverse complement DNA method

        Keyword arguments:
        sequence -- the sequence to reverse complement
        """
        return BioHelper.complement(sequence)[::-1]

    @staticmethod
    def complement(sequence):
        """ Really fast way to make the sequence complement

        Keyword arguments:
        sequence -- the sequence to complement
        """
        return sequence.translate(str.maketrans('ATCGUNWSMKRYBVDHatcgunwsmkrybvdh', 'TAGCANWSKMYRVBHDtagcanwskmyrvbhd'))

    @staticmethod
    def to_uracil(sequence):
        """ Turn every 'T' into 'U' in the given sequence """
        return sequence.translate(str.maketrans('Tt', 'Uu'))

    @staticmethod
    def to_thymine(sequence):
        """ Turn every 'U' into 'T' in the given sequence """
        return sequence.translate(str.maketrans('Uu', 'Tt'))

    @staticmethod
    def extract_from(base_sequence, start, stop, strand=None, circular=False, to_uracil=False):
        """ Extract a sub-sequence from start to stop
        /!\ start and stop positions ARE INCLUDED /!\

              1 2 3 4 5 6 7 8
        seq = A-T-C-G-A-T-C-G

        BioHelper.extract_from(seq, 1, 4, '+') => returns 'ATCG'
        BioHelper.extract_from(seq, 1, 4, '-') => returns 'CGAT'
        BioHelper.extract_from(seq, 1, 1, '+') => returns 'A'
        BioHelper.extract_from(seq, 1, 1, '-') => returns 'T'

        Keyword arguments:
        base_sequence -- the base sequence which from the sub-sequence will be extracted
        start -- the start position for extraction
        strand -- the strand, must be '-' or '+'
        stop -- the stop position for extraction
        circular -- is the sequence circular?
        to_uracil -- change 'T' to 'U' at the end of the process?
        """
        if not isinstance(base_sequence, str):
            raise TypeError
        if not isinstance(to_uracil, bool):
            raise TypeError
        if not isinstance(circular, bool):
            raise TypeError
        if strand not in ['-', '+']:
            raise AttributeError('Strand must be "+" or "-"')
        if start <= 0:
            raise AttributeError('Start value must be > 0')
        if start > stop:
            raise AttributeError('Start value cannot be > to stop value')
        sequence_size = len(base_sequence)
        if not circular and stop > sequence_size:
            raise AttributeError('Stop position cannot be after end of sequence length if sequence is not circular')
        if base_sequence.strip() == '':
            raise AttributeError('Sequence is empty')
        res = None
        if not circular or circular and stop < sequence_size:
            #if not circular sequence
            if strand == '+':
                res = base_sequence[start-1:stop]
            else:
                res = BioHelper.reverse_complement(base_sequence[start-1:stop])
        else:
            #if ciruclar sequence
            if strand == '+':
                res = base_sequence[start-1:sequence_size] + base_sequence[0:stop - sequence_size]
            else:
                res = BioHelper.reverse_complement(base_sequence[start-1:sequence_size] + base_sequence[0:stop - sequence_size])
        return res if not to_uracil else BioHelper.to_uracil(res)

#
# Copyright (C) 2016 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Regis Ongaro - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2016 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

from jflow.featureio import GFF3Record

from srnatabac.core.rna_feature import mRNA, ncRNA
from srnatabac.core.strand import Strand
from srnatabac.core.region import Region
from srnatabac.core.interaction import Interaction, InteractionDetail


class GFF3Converter(object):
    """ Utils methods to help convert objects into GFF3 records and the opposite """

    @staticmethod
    def to_mrna(gff_record):
        """ Create a mRNA object WITHOUT its sequence from a GFF3 record

        Keyword argument:
        gff_record -- a gff3 record
        """
        t_strand = Strand.get_strand_from_string(gff_record.strand)
        before_tag = 'before'
        after_tag = 'after'
        m = None
        if gff_record.type == 'gene':
            # gene definition
            m = mRNA(gff_record.seq_id, gff_record.start, gff_record.end, t_strand, gff_record.start, gff_record.end)
        elif before_tag not in list(gff_record.attributes.keys()) and after_tag not in list(gff_record.attributes.keys()):
            # simple definition UTR
            m = mRNA(gff_record.seq_id, gff_record.start, gff_record.end, t_strand)
        else:
            # simulated UTR
            if before_tag in list(gff_record.attributes.keys()):
                if Region.get_region_from_string(gff_record.type) != Region.region_3UTR:
                    raise Exception(
                        'Cannot have "{}" attribute on record-type "{}"'.format(before_tag, gff_record.type))
                before_val = int(gff_record.attributes[before_tag])
                if t_strand == Strand.plus:
                    # 3UTR +
                    t_start = gff_record.start - before_val - 1
                    t_stop = gff_record.end
                else:
                    # 3UTR -
                    t_start = gff_record.start
                    t_stop = gff_record.end + before_val + 1

            if after_tag in list(gff_record.attributes.keys()):
                if Region.get_region_from_string(gff_record.type) != Region.region_5UTR:
                    raise Exception(
                        'Cannot have "{}" attribute on record-type "{}"'.format(after_tag, gff_record.type))
                after_val = int(gff_record.attributes[after_tag])
                if t_strand == Strand.plus:
                    # 5UTR +
                    t_start = gff_record.start
                    t_stop = gff_record.end + after_val + 1
                else:
                    # 5UTR -
                    t_start = gff_record.start - after_val - 1
                    t_stop = gff_record.end
            m = mRNA(gff_record.seq_id, t_start, t_stop, t_strand)
        return m

    @staticmethod
    def to_ncrna(gff_record):
        """ Create a ncRNA object WITHOUT its sequence from a GFF3 record

        Keyword argument:
        gff_record -- a gff3 record
        """
        t_strand = Strand.get_strand_from_string(gff_record.strand)
        return ncRNA(gff_record.seq_id, gff_record.start, gff_record.end, t_strand)

    @staticmethod
    def _from_rna_feature(hstart, hstop, hstrand):
        """ Create a gff3 record from a rna feature

        Keyword argument:
        hstart -- a start position
        hstop -- a stop position
        hstrand -- a strand
        """
        if not isinstance(hstrand, Strand):
            raise TypeError
        gffr = GFF3Record()
        gffr.source = "."
        gffr.start = hstart
        gffr.end = hstop
        gffr.score = "."
        gffr.strand = Strand.get_string_value(hstrand)
        gffr.phase = "."
        return gffr

    @staticmethod
    def from_mrna(mrna):
        """ Create a gff3 record from a mRNA object

        Keyword argument:
        mrna -- a mRNA object
        """
        if not isinstance(mrna, mRNA):
            raise TypeError
        gff = GFF3Converter._from_rna_feature(mrna.gene_start, mrna.gene_stop, mrna.strand)
        gff.seq_id = mrna.id.split("_")[0].replace("*", "")
        gff.type = "gene"
        gff.attributes = {"ID": gff.seq_id}
        return gff

    @staticmethod
    def from_ncrna(ncrna):
        """ Create a gff3 record from a ncRNA object

        Keyword argument:
        ncrna -- a ncRNA object
        """
        if not isinstance(ncrna, ncRNA):
            raise TypeError
        gff = GFF3Converter._from_rna_feature(ncrna.start, ncrna.stop, ncrna.strand)
        gff.seq_id = ncrna.id.split("|")[0]
        gff.type = "ncRNA"
        gff.attributes = {"ID": gff.seq_id}
        return gff

    @staticmethod
    def to_interaction(gff_record):
        """ Build an interaction based on GFF3 line

        Keyword arguments:
        gff_record -- a gff3 record
        """
        if not isinstance(gff_record, GFF3Record):
            raise TypeError('Need a GFF3Record')
        ib = Interaction()
        ib.id = gff_record.attributes['ID']
        ib.details.software = gff_record.source
        ib.region_id = Region.get_region_from_string(gff_record.type)
        # calculate position of interaction on target
        ib.details.energy_software = float(gff_record.score)
        ib.ncrna_id = gff_record.attributes['Target'].split()[0]
        # create the mRNA object
        ib.mrna = GFF3Converter.to_mrna(gff_record)
        ib.mrna_id = gff_record.seq_id
        # gap control
        g = Gap(gff_record.attributes['gap'])
        ib.details.overlap_length = g.get_overlap_length()
        ib.details.match_count = g.get_total_matches()
        ib.details.match_successive = g.get_max_matches()
        # create the symbolism
        ib.details.mrna_seq = g.get_mrna_symbolism()
        ib.details.symbolism = g.get_interaction_symbolism()
        ib.details.ncrna_seq = g.get_ncrna_symbolism()
        #create specific interaction zone based on gap attribute
        ib.details.mrna_begin = ib.mrna.start + g.get_initial_offset()
        ib.details.mrna_end = ib.mrna.stop - g.get_final_offset()
        return ib

    @staticmethod
    def from_interaction(interaction):
        """ Build a GFF3 line based on an interaction object
        This return ONLY the interaction gff3 record, not the mRNA and ncRNA associated with

        Keyword arguments:
        interaction -- an interaction object
        """
        if not isinstance(interaction, Interaction):
            raise TypeError
        t = interaction.mrna
        # Build the region coordinate and attribute of extension
        before = 0
        after = 0
        region_start = 0
        region_stop = 0
        if t.strand == Strand.plus:
            if interaction.region_id == Region.region_5UTR:  # t.region is 5UTR
                if t.stop == t.gene_start - 1:
                    # well-defined 5UTR region
                    region_start = t.start
                    region_stop = t.stop
                else:
                    # calculated 5UTR region with offset
                    region_start = t.start
                    region_stop = t.gene_start - 1
                    after = t.stop - t.gene_start

            elif interaction.region_id == Region.region_3UTR:  # t.region is 3UTR
                if t.start == t.gene_stop + 1:
                    # well-defined 3UTR region
                    region_start = t.start
                    region_stop = t.stop
                else:
                    # calculated 3UTR region with offset
                    region_start = t.gene_stop + 1
                    region_stop = t.stop
                    before = t.gene_stop - t.start

            else:  # t.region is CDS
                region_start = t.start
                region_stop = t.stop

        else:  # t.strand is "-"
            if interaction.region_id == Region.region_5UTR:  # t.region is 5UTR
                if t.start == t.gene_stop + 1:
                    region_start = t.start
                    region_stop = t.stop
                else:
                    region_start = t.gene_stop + 1
                    region_stop = t.stop
                    after = t.gene_stop - t.start

            elif interaction.region_id == Region.region_3UTR:  # t.region is 3UTR
                if t.stop == t.gene_start - 1:
                    region_start = t.start
                    region_stop = t.stop
                else:
                    region_start = t.start
                    region_stop = t.gene_start - 1
                    before = t.stop - t.gene_start
            else:  # t.region is CDS
                region_start = t.start
                region_stop = t.stop

        gffr = GFF3Record()
        gffr.seq_id = interaction.mrna_id.replace("*", "")
        gffr.source = interaction.details.software
        gffr.type = Region.get_gff3_value(interaction.region_id)
        gffr.phase = "0" if interaction.region_id == Region.region_CDS else "."
        gffr.start = region_start
        gffr.end = region_stop
        gffr.score = round(interaction.details.energy_software, 2) if int(
            interaction.details.energy_software) != 0 else 0
        gffr.strand = Strand.get_string_value(interaction.mrna.strand)

        # prepare 9th column attributes
        # target attribute
        if interaction.ncrna.strand == Strand.plus:
            ta_begin = interaction.details.ncrna_begin - interaction.ncrna.start
            ta_end = interaction.details.ncrna_end - interaction.ncrna.start
        else:
            ta_end = interaction.ncrna.stop - interaction.details.ncrna_begin
            ta_begin = interaction.ncrna.stop - interaction.details.ncrna_end
        # gap attribute
        g = Gap.from_interaction(interaction)
        # define attribute for the GFF' 9th column
        gffr.attributes = {}
        gffr.addToAttribute("ID", interaction.id)
        gffr.addToAttribute("Parent", gffr.seq_id)
        gffr.addToAttribute("Target", '{} {} {}'.format(interaction.ncrna_id, ta_begin, ta_end))
        gffr.addToAttribute("gap", g.get_sequence())
        if before != 0:
            gffr.addToAttribute("before", str(before))
        if after != 0:
            gffr.addToAttribute("after", str(after))
        return gffr


class Gap(object):
    """ Specific gap attribute for GFF3 records
    sRNA-TaBac handles a modified 'gap' attribute

    Keyword attributes:
    _sequence -- the gap sequence. ex: 'S34 M2 X3 D1 M54 S21'
    _gap -- the gap decomposition
    _interaction_symbolism -- the interaction symbolism sequence
    _mrna_sequence_symbolism -- the mrna symbolism sequence
    _ncrna_sequence_symbolism -- the ncrna symbolism sequence
    """

    M = 'M'  # match
    X = 'X'  # mismatch
    D = 'D'  # deletion
    I = 'I'  # insertion
    S = 'S'  # number of nt from region which are not involved into interaction

    def __init__(self, sequence):
        if not isinstance(sequence, str):
            raise TypeError
        if not Gap.is_gap_sequence(sequence):
            raise AttributeError('The sequence "{}" is not a Gap-type string'.format(sequence))

        self._sequence = sequence
        self._gap = {Gap.M: [], Gap.X: [], Gap.D: [], Gap.I: [], Gap.S: []}
        self._interaction_symbolism = ''
        self._mrna_sequence_symbolism = ''
        self._ncrna_sequence_symbolism = ''

        #--------------------------------------
        # parsing Gap string
        try:
            for i in self._sequence.split():
                self._gap[i[0]].append(int(i[1:]))
        except KeyError:
            raise AttributeError('The sequence "{}" is not a Gap-type string'.format(sequence))

        #--------------------------------------
        # building symbolisms
        for g_seq in self._sequence.split():
            # Gap element count number
            # ex: 'S34' => t is 'S', n is 34
            t = g_seq[0]
            n = int(g_seq[1:])
            if t == Gap.M:
                self._interaction_symbolism += n * '|'
                self._mrna_sequence_symbolism += n * 'N'
                self._ncrna_sequence_symbolism += n * 'N'

            elif t == Gap.X:
                self._interaction_symbolism += n * ' '
                self._mrna_sequence_symbolism += n * 'N'
                self._ncrna_sequence_symbolism += n * 'N'

            elif t == Gap.D:
                self._interaction_symbolism += n * ' '
                self._mrna_sequence_symbolism += n * 'N'
                self._ncrna_sequence_symbolism += n * '-'

            elif t == Gap.I:
                self._interaction_symbolism += n * ' '
                self._mrna_sequence_symbolism += n * '-'
                self._ncrna_sequence_symbolism += n * 'N'

    @staticmethod
    def is_gap_sequence(sequence):
        """ https://docs.python.org/3/library/re.html#re.fullmatch """
        import re
        return re.fullmatch(r'([SDXIM][0-9]+ )+[SDXIM][0-9]+', sequence)

    def get_gap(self):
        return self._gap

    def get_sequence(self):
        return self._sequence

    def get_interaction_symbolism(self):
        return self._interaction_symbolism

    def get_mrna_symbolism(self):
        return self._mrna_sequence_symbolism

    def get_ncrna_symbolism(self):
        return self._ncrna_sequence_symbolism

    # attr
    def get_m(self):
        return self._gap[Gap.M]

    def get_x(self):
        return self._gap[Gap.X]

    def get_d(self):
        return self._gap[Gap.D]

    def get_i(self):
        return self._gap[Gap.I]

    def get_s(self):
        return self._gap[Gap.S]

    # Method
    def get_total_matches(self):
        return sum(self.get_m())

    def get_max_matches(self):
        return max(self.get_m())

    def get_overlap_length(self):
        return sum(self.get_m()) + sum(self.get_x()) + sum(self.get_d()) + sum(self.get_i())

    def get_initial_offset(self):
        t = self.get_sequence().split()[0]
        return int(t[1:]) if t[0] == Gap.S else 0

    def get_final_offset(self):
        t = self.get_sequence().split()[-1]
        return int(t[1:]) if t[0] == Gap.S else 0

    @staticmethod
    def from_interaction(interaction):
        if not isinstance(interaction, Interaction):
            raise TypeError
        if not interaction.mrna:
            raise AttributeError
        # var
        last_nb = None
        last_type = None
        sequence = []
        #get index type
        symb = interaction.details.symbolism
        mrna_seq = interaction.details.mrna_seq
        ncrna_seq = interaction.details.ncrna_seq
        def _get_type_of_index(s):
            """ get the current type of the interaction triplet """
            t = None
            if (symb[s] == '|' or symb[s] == '.'):
                t = Gap.M
            else:
                if ncrna_seq[s] != '-' and mrna_seq[s] != '-':
                    t = Gap.X
                elif ncrna_seq[s] == '-' and mrna_seq[s] != '-':
                    t = Gap.D
                elif ncrna_seq[s] != '-' and mrna_seq[s] == '-':
                    t = Gap.I
                else:
                    raise Exception
            return t
        # iter
        for s in range(len(mrna_seq)):
            current_type = _get_type_of_index(s)
            if last_type is None:
                if s != 0:
                    raise Exception
                #first record
                last_type = current_type
                last_nb = 1

            else:
                if current_type == last_type:
                    #same type
                    last_nb += 1
                else:
                    #other type encounter
                    sequence.append("{}{}".format(last_type, last_nb))
                    last_type = current_type
                    last_nb = 1
        #last record
        sequence.append("{}{}".format(current_type, last_nb))
        #clipped values, S from begin and from end
        clipped_before = interaction.details.mrna_begin - interaction.mrna.start
        if clipped_before < 0:
            raise Exception
        clipped_after = interaction.mrna.stop - interaction.details.mrna_end
        if clipped_after < 0:
            raise Exception
        if clipped_before != 0:
            sequence.insert(0, "S{}".format(clipped_before))
        if clipped_after != 0:
            sequence.append("S{}".format(clipped_after))
        return Gap(' '.join(sequence))

#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys
import argparse
import time

try:
    import _preamble
except ImportError:
    sys.exc_clear()

from jflow.workflows_manager import WorkflowsManager
from jflow.workflow import Workflow
import jflow.utils as utils

from srnatabac.utils.common import Common

class JflowArgumentParser (argparse.ArgumentParser):

    def _read_args_from_files(self, arg_strings):
        # expand arguments referencing files
        new_arg_strings = []
        for arg_string in arg_strings:
            # if it's not a comment or an empty line
            if not arg_string.startswith("#") and arg_string:
                # for regular arguments, just add them back into the list
                if not arg_string or arg_string[0] not in self.fromfile_prefix_chars:
                    new_arg_strings.append(arg_string)
                # replace arguments referencing files with the file content
                else:
                    try:
                        with open(arg_string[1:]) as args_file:
                            arg_strings = []
                            # give to the convert_arg_line_to_args a table of
                            # lines instead of line per line
                            for arg in self.convert_arg_line_to_args(args_file.read().splitlines()):
                                arg_strings.append(arg)
                            arg_strings = self._read_args_from_files(
                                arg_strings)
                            new_arg_strings.extend(arg_strings)
                    except OSError:
                        err = _sys.exc_info()[1]
                        self.error(str(err))
        # return the modified argument list
        return new_arg_strings


if __name__ == '__main__':

    # Create a workflow manager to get access to our workflows
    wfmanager = WorkflowsManager()

    # Create the top-level parser
    parser = JflowArgumentParser()
    subparsers = parser.add_subparsers(title='Available sub commands')

    # Add status workflow availability
    sub_parser = subparsers.add_parser(
        "status", help="Monitor a specific workflow")
    sub_parser.add_argument(
        "--workflow-id", type=str, help="Which workflow status should be displayed",
        default=None, dest="workflow_id")
    sub_parser.add_argument(
        "--all", action="store_true", help="Display all workflows status",
        default=False, dest="all")
    sub_parser.add_argument(
        "--errors", action="store_true", help="Display failed commands",
        default=False, dest="display_errors")
    sub_parser.set_defaults(cmd_object="status")

    # Clean projects
    sub_parser = subparsers.add_parser(
        "clean", help="Delete permanently user projects")
    grp = sub_parser.add_mutually_exclusive_group(required=True)
    grp.add_argument("-a", "--all",\
        help="Clean all users projects",\
        action="store_true", default=False)
    grp.add_argument("-u", "--user", type=str,\
        help="Clean only user specific projects",\
        default=None, dest="user_email")
    grp.add_argument("-w", "--workflow-id", type=str,\
        help="Clean only a specific project by its workflow id",\
        default=None, dest="workflow_id")
    sub_parser.add_argument("-f", "--force",\
        help="Use with care. By default, only clean when outdated. Use this to clean even if projects are not outdated",\
        action="store_true", default=False)
    sub_parser.set_defaults(cmd_object="clean")

    # sRNA-TaBac check
    sub_parser = subparsers.add_parser(
        "check", help="Check for data corruption")
    sub_parser.set_defaults(cmd_object="check")

    # DEBUG
    sub_parser = subparsers.add_parser("see", help="Print various informations")
    grp = sub_parser.add_mutually_exclusive_group(required=True)
    grp.add_argument("-u", "--user", type=str,\
        help="See informations about a specific user",\
        default=None, dest="user_mail_or_id")
    grp.add_argument("-p", "--project", type=str,\
        help="See informations about a specific user project",\
        default=None, dest="project_or_wf_id")
    sub_parser.set_defaults(cmd_object="see")

    # # sRNA-TaBac test suite and version
    grp = parser.add_mutually_exclusive_group(required=False)
    grp.add_argument("-v", "--version",\
        help="print sRNA-TaBac current version and exit",\
        action="store_true", default=False)
    grp.add_argument("-t", "--test", type=int,\
        help="Run all unit tests and exit", default=-1, choices=range(3))

    # Add available pipelines
    wf_instances, wf_methodes = wfmanager.get_available_workflows()
    wf_classes = []
    for instance in wf_instances:
        wf_classes.append(instance.__class__.__name__)
        # create the subparser for each applications
        sub_parser = subparsers.add_parser(
            instance.name, help=instance.description, fromfile_prefix_chars='@')
        sub_parser.convert_arg_line_to_args = instance.__class__.config_parser
        [parameters_groups,
            parameters_order] = instance.get_parameters_per_groups()
        for group in parameters_order:
            if group == "default":
                for param in parameters_groups[group]:
                    sub_parser.add_argument(
                        param.flag, **param.export_to_argparse())
            elif group.startswith("exclude-"):
                is_required = False
                for param in parameters_groups[group]:
                    if param.required:
                        is_required = True
                        # an exlcusive parameter cannot be required, the
                        # require is at the group level
                        param.required = False
                pgroup = sub_parser.add_mutually_exclusive_group(
                    required=is_required)
                for param in parameters_groups[group]:
                    pgroup.add_argument(
                        param.flag, **param.export_to_argparse())
            else:
                pgroup = sub_parser.add_argument_group(group)
                for param in parameters_groups[group]:
                    pgroup.add_argument(
                        param.flag, **param.export_to_argparse())
        sub_parser.set_defaults(cmd_object=instance.__class__.__name__)
    args = vars(parser.parse_args())

    if not "cmd_object" in args:
        #version message
        if 'version' in args and args['version']:
            print('sRNA-TaBac v1.0.0')

        elif 'test' in args and args['test'] != -1:
            import unittest
            #load tests
            loader = unittest.TestLoader()
            suite = loader.loadTestsFromNames([
                'srnatabac.tests.test_core_authkey',
                'srnatabac.tests.test_core_region',
                'srnatabac.tests.test_core_strand',
                'srnatabac.tests.test_core_rna_feature',
                'srnatabac.tests.test_utils_bio_helper',
                'srnatabac.tests.functional_test_file',
                'srnatabac.tests.test_dao',
            ])
            sep = '----------------------------------------------------------------------'
            print('\n{}\nsRNA-TaBac: test suite\n{}\n'.format(sep, sep))
            #run tests
            unittest.TextTestRunner(verbosity=args['test']).run(suite)

        else:
            #help message
            print(parser.format_help())
        parser.exit(0, "")

    if args["cmd_object"] in wf_classes:
        # run workflow and register user new project
        workflow = wfmanager.run_workflow(args["cmd_object"], args)

    elif args["cmd_object"] == "see":
        from srnatabac.dao.user_handler import UserHandler
        from srnatabac.core.authkey import UserAuthkey, ProjectAuthkey
        uh = UserHandler()
        if args['user_mail_or_id']:
            #search by mail address
            user_found = uh.get_user_by_email(args['user_mail_or_id'])
            if user_found is not None:
                print(user_found)
            else:
                # search by user_id
                user_found = uh.get_user_data(UserAuthkey(args['user_mail_or_id']))
                if user_found is not None:
                    print(user_found)
                else:
                    print('[error] user "{}" not found'.format(args['user_mail_or_id']))

        elif args['project_or_wf_id']:
            # search by project id
            project_found = uh.find_user_project(ProjectAuthkey(args['project_or_wf_id']))
            if project_found is not None:
                print(project_found)
            else:
                #serach by workflow id
                user_data = uh.get_user_by_workflow_id(args['project_or_wf_id'])
                if user_data is not None:
                    print(user_data.get_project_by_workflow_id(args['project_or_wf_id']))
                else:
                    print('[error] project "{}" not found'.format(args['project_or_wf_id']))

    elif args["cmd_object"] == "clean":
        from srnatabac.ui.controller.clean_controller import CleanController
        cleaner = CleanController()
        def _format_deleted(d_type, d_obj):
            res = ''
            if d_type == 'workflow_id':
                res = 'Project{}{}deleted'.format(
                    ' ' if d_obj['deleted_project'] else 'not ',
                    'and user ' if d_obj['deleted_user'] else ''
                )
            elif d_type == 'all' or d_type == 'user_email':
                res = '{} of {} projects and {} of {} users deleted'.format(
                    d_obj['deleted_projects'],
                    d_obj['projects'],
                    d_obj['deleted_users'],
                    d_obj['users'],
                )
            return res
        try:
            _process = '[FORCE] ' if args["force"] else '[ONLY OUTDATED] '
            if args["all"]:
                cleaned = cleaner.clean_all(args["force"])
                print('{}DELETE all projects... done'.format(_process))
                print(_format_deleted('all', cleaned))

            elif args["user_email"]:
                cleaned = cleaner.clean_user(args['user_email'], args["force"])
                print('{}DELETE all user projects... done'.format(_process))
                print(_format_deleted('user_email', cleaned))

            elif args["workflow_id"]:
                cleaned = cleaner.clean_workflow(args["workflow_id"], args["force"])
                print('{}DELETE workflow... done'.format(_process))
                print(_format_deleted('workflow_id', cleaned))

            else:
                utils.display_error_message("No 'clean-project' option specified")
        except Exception as e:
            utils.display_error_message(str(e))

    elif args["cmd_object"] == "check":
        from srnatabac.dao.project_handler import ProjectHandler
        res = ProjectHandler().check_projects()
        tmp = 'Errors found:'
        for errors_type, errors_list in res.items():
            if not errors_list:
                tmp += '\n checking {}:\t\033[92m[clear]\033[0m'.format(errors_type)
            else:
                tmp += '\n {}:'.format(errors_type)
                for e in errors_list:
                    tmp += '\n  - \033[91m[error]\033[0m: {}'.format(e)
        print(tmp)

    elif args["cmd_object"] == "status":
        if args["workflow_id"]:
            try:
                workflow = wfmanager.get_workflow(args["workflow_id"])
            except Exception as e:
                utils.display_error_message(str(e))
            print(Workflow.get_status_under_text_format(workflow, True, args["display_errors"]))
        else:
            try:
                workflows = wfmanager.get_workflows(use_cache=True)
            except Exception as e:
                utils.display_error_message(str(e))
            if len(workflows) > 0:
                workflows_by_id, wfids = {}, []
                # first sort workflow by ID
                for workflow in workflows:
                    wfids.append(workflow.id)
                    workflows_by_id[workflow.id] = workflow
                status = "ID\tNAME\tSTATUS\tELAPSED_TIME\tSTART_TIME\tEND_TIME\n"
                for i, wfid in enumerate(sorted(wfids, reverse=True)):
                    status += Workflow.get_status_under_text_format(
                        workflows_by_id[wfid])
                    if i < len(workflows) - 1:
                        status += "\n"
            else:
                status = "no workflow available"
            print(status)

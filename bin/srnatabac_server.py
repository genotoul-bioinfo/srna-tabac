#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import cherrypy
import cgi
import tempfile
import json
import sys
import datetime
from functools import wraps
import time
import os
import argparse

try:
    import _preamble
except ImportError:
    sys.exc_clear()

from jflow.server import JFlowServer

import srnatabac
from srnatabac.utils.srnatabac_config_reader import SrnatabacConfigReader
from srnatabac.utils.common import Common
from srnatabac.core.authkey import UserAuthkey, ProjectAuthkey, CompleteAuthkey
from srnatabac.utils.srnatabac_config_reader import SrnatabacConfigReader
from srnatabac.utils.srnatabac_json_encoder import SRNATabacJSONEncoder

from srnatabac.ui.controller.datatable_controller import DatatableController
from srnatabac.ui.controller.clean_controller import CleanController
from srnatabac.dao.user_handler import UserHandler
from srnatabac.dao.project_handler import ProjectHandler
from srnatabac.dao.rna_handler import RNAHandler
from srnatabac.dao.interaction_handler import InteractionHandler
from srnatabac.core.region import Region
from srnatabac.core.strand import Strand


class SRNATabacServer( JFlowServer ):

    """ The sRNA-tabac server """
    def __init__(self):
        JFlowServer.__init__(self)
        self.config_reader = SrnatabacConfigReader()
        #controllers
        self.datatable_controller = DatatableController()
        self.clean_controller = CleanController()
        #handlers
        self.user_handler = UserHandler()
        self.project_handler = ProjectHandler()
        self.interaction_handler = InteractionHandler()

    def jsonify(func):
        '''JSON and JSONP decorator for CherryPy'''
        @wraps(func)
        def wrapper(*args, **kw):
            value = func(*args, **kw)
            cherrypy.response.headers["Content-Type"] = "application/json"
            # if JSONP request
            if "callback" in kw:
                return ('%s(%s)' % (kw["callback"], json.dumps(value, cls=SRNATabacJSONEncoder))).encode('utf8')
            # else return the JSON
            else:
                return json.dumps(value, cls=SRNATabacJSONEncoder).encode('utf8')
        return wrapper

    ### AJAX

    @cherrypy.expose
    @jsonify
    def get_workflow_outputs(self, **kwargs):
        from jflow.utils import get_octet_string_representation
        import itertools
        on_web_outputs = {}
        if 'workflow_id' in list(kwargs.keys()) and 'user' in list(kwargs.keys()) and 'project' in list(kwargs.keys()):
            #check user and project
            valid_user = False
            user_authkey = UserAuthkey(kwargs['user'])
            project_authkey = ProjectAuthkey(kwargs['project'])
            complete_authkey = CompleteAuthkey(user_authkey, project_authkey)
            try:
                self.user_handler.get_project_by_complete_key(complete_authkey)
                valid_user = True
            except Exception as e:
                pass

            #user and project exist
            if valid_user:
                #not send temp files
                softs = ['risearch', 'rnacofold', 'rnaplex', 'ssearch', 'intarna', 'tmp', 'rnaeval']
                ext = ['fasta', 'pgz', 'raw', 'stderr', 'fa', 'energy', 'rnaeval', 'txt']
                invalid_ends = tuple([ "_{}.{}".format(x,y) for x,y in itertools.product(softs, ext) ])
                # invalid_ends = tuple('.pgz') + invalid_ends
                #search
                on_disk_outputs = self.wfmanager.get_workflow_outputs(kwargs["workflow_id"])
                for cpt_name in list(on_disk_outputs.keys()):
                    on_web_outputs[cpt_name] = {}
                    for outf in on_disk_outputs[cpt_name]:
                        #filter
                        if outf.endswith(invalid_ends):
                            continue
                        path = on_disk_outputs[cpt_name][outf]
                        work_dir  = self.config_reader.get_work_directory()
                        if work_dir.endswith("/"):
                            work_dir = work_dir[:-1]
                        socket_opt = self.config_reader.get_socket_options()
                        try:
                            on_web_outputs[cpt_name][outf] = {
                                'url':'http://' + socket_opt[0] + ':' + str(socket_opt[1]) + '/' + path.replace(work_dir, 'data'),
                                'size': get_octet_string_representation(os.path.getsize(os.path.abspath(path))),
                                'extension': os.path.splitext(path)[1]
                            }
                        except FileNotFoundError:
                            pass
        return on_web_outputs

    @cherrypy.expose
    @jsonify
    def delete_projects(self, **kwargs):
        answer = {}
        if len(list(kwargs.keys()))==2\
            and 'user' in list(kwargs.keys())\
            and 'projects' in list(kwargs.keys()):
            #check user and project
            valid_user = False
            user_authkey = UserAuthkey(kwargs['user'])
            #get projects list
            for p in kwargs['projects'].split(','):
                #get values
                tp = p.split('!')
                #check user/project validity
                complete_authkey = CompleteAuthkey(user_authkey, ProjectAuthkey(tp[0]))
                try:
                    self.user_handler.get_project_by_complete_key(complete_authkey)
                    valid_user = True
                except Exception as e:
                    print('ERROR')
                    break
                #wf_id:False --> not deleted
                answer[tp[1]] = False
                #user and project exist
                if valid_user:
                    temp_answer = self.clean_controller.clean_workflow(tp[1], force=True)
                    answer[tp[1]] = temp_answer['deleted_project']
        return answer

    @cherrypy.expose
    @jsonify
    def get_user_workspace(self, **kwargs):
        res = None
        if len(kwargs) == 1 and 'workflow_id' in list(kwargs.keys()):
            user_data = self.user_handler.get_user_by_workflow_id(kwargs['workflow_id'])
            if user_data is not None:
                res = user_data.get_user_key().get_key()
        return res

    @cherrypy.expose
    @jsonify
    def get_datatable_data(self, **kwargs):
        """ Build and send the datatable content according to the kwargs details.
        Sent by the datatable itself or by ajax request.
        """
        return self.datatable_controller.get_datatable_content(**kwargs)

    @cherrypy.expose
    def get_datatable_interaction(self, **kwargs):
        if len(list(kwargs.keys())) == 3 and 'user' in list(kwargs.keys())\
            and 'project' in list(kwargs.keys()) and 'interaction' in list(kwargs.keys()):
            user_authkey = UserAuthkey(kwargs['user'])
            project_authkey = ProjectAuthkey(kwargs['project'])
            complete_authkey = CompleteAuthkey(user_authkey, project_authkey)
            # get the prediction
            i = self.interaction_handler.get_interaction_by_id(complete_authkey, kwargs['interaction'])
            # build page
            if i is not None:
                modal_title = "Interaction [{}] informations: mRNA {}, ncRNA {}".format(i.id, i.mrna_id, i.ncrna_id)
                return Common.get_template('modal_interaction.tmpl', Region.get_string_value).render(
                    i=i, modal_title=modal_title,
                    user=user_authkey.get_key(),
                    project=project_authkey.get_key(),
                    strand_minus=Strand.minus #usefull for cmp
                )
            else:
                return Common.get_template('page_error.tmpl').render(error=DEFAULT_ERROR_404)
        else:
            return Common.get_template('page_error.tmpl').render(error=DEFAULT_ERROR_404)

    ### INDEX

    @cherrypy.expose
    def index(self, **kwargs):
        if not kwargs:
            return Common.get_template('page_home.tmpl').render()
        else:
            return Common.get_template('page_error.tmpl').render(error=DEFAULT_ERROR_404)

    ### WORKFLOWS

    def _get_wf_render_infos(self, wfclass=None, wfname=None):
        """ Get rendering informations for each workflow

        Keyword arguments:
        wfclass -- the workflow class
        wfname -- the workflow name
        """
        if wfclass is None or not isinstance(wfclass, str): raise TypeError
        if wfname is None or not isinstance(wfname, str): raise TypeError
        socket_opt = self.config_reader.get_socket_options()
        base_sample_adr = 'http://{}:{}/sample'.format(socket_opt[0],socket_opt[1])
        return {
            'wf_description': self.project_handler.get_project_description(wfname.lower()),
            'wf_class': wfclass,
            'wf_name': wfname,
            'page_id': wfname.lower(),
            'example_download_link': '{}/srnatabac_{}_sample.tgz'.format(base_sample_adr, wfname.lower())
        }

    @cherrypy.expose
    def load(self, **kwargs):
        if not kwargs:
            wf_name = 'Load'
            wf_class = 'Load'
            return Common.get_template('page_load.tmpl').render(self._get_wf_render_infos(wf_class, wf_name))
        else:
            return Common.get_template('page_error.tmpl').render(error=DEFAULT_ERROR_404)

    @cherrypy.expose
    def predictFasta(self, **kwargs):
        if not kwargs:
            wf_name = 'PredictFasta'
            wf_class = 'Predict'
            return Common.get_template('page_predictfasta.tmpl').render(self._get_wf_render_infos(wf_class, wf_name))
        else:
            return Common.get_template('page_error.tmpl').render(error=DEFAULT_ERROR_404)

    @cherrypy.expose
    def predictGff3(self, **kwargs):
        if not kwargs:
            wf_name = 'PredictGFF3'
            wf_class = 'Predict'
            return Common.get_template('page_predictgff3.tmpl').render(self._get_wf_render_infos(wf_class, wf_name))
        else:
            return Common.get_template('page_error.tmpl').render(error=DEFAULT_ERROR_404)

    ### HELP

    @cherrypy.expose
    def comingsoon(self, **kwargs):
        if not kwargs:
            return Common.get_template('page_comingsoon.tmpl').render(
                msg={'user_msg':"""sRNA-TaBac already allows you to get generated
                    binairy data, but you can not handle them on your own.<br/>
                    Soon, you will.<br/>Let us prepare that for you."""}
            )
        else:
            return Common.get_template('page_error.tmpl').render(error=DEFAULT_ERROR_404)

    @cherrypy.expose
    def manual(self, **kwargs):
        if not kwargs:
            return Common.get_template('page_manual.tmpl').render()
        else:
            return Common.get_template('page_error.tmpl').render(error=DEFAULT_ERROR_404)

    @cherrypy.expose
    def manualadmin(self, **kwargs):
        if not kwargs:
            from srnatabac.utils.doc_helper import DocHelper
            return Common.get_template('page_manualadmin.tmpl').render(
                workflows_args=DocHelper.get_workflows_arguments(),
                softwares=DocHelper.get_softwares()
            )
        else:
            return Common.get_template('page_error.tmpl').render(error=DEFAULT_ERROR_404)

    @cherrypy.expose
    def manualformat(self, **kwargs):
        if not kwargs:
            return Common.get_template('page_manualformat.tmpl').render()
        else:
            return Common.get_template('page_error.tmpl').render(error=DEFAULT_ERROR_404)

    ### USER PROJECTS AND RESULTS

    @cherrypy.expose
    def projects(self, **kwargs):
        if len(list(kwargs.keys())) == 1 and 'user' in list(kwargs.keys()):
            user_authkey = UserAuthkey(kwargs['user'])
            if self.user_handler.user_exists(user_authkey):
                return Common.get_template('page_projects.tmpl').render(
                    useruuid=user_authkey.get_key(),
                    workflows_list=self.project_handler.get_user_projects_status(user_authkey),
                    page_modal=Common.get_template('modal_confirmation.tmpl').render()
                )
            else:
                return Common.get_template('page_error.tmpl').render(error=DEFAULT_ERROR_404)
        else:
            return Common.get_template('page_error.tmpl').render(error=DEFAULT_ERROR_404)

    @cherrypy.expose
    def result(self, **kwargs):
        if len(list(kwargs.keys())) == 2 and 'user' in list(kwargs.keys()) and 'project' in list(kwargs.keys()):
            user_authkey = UserAuthkey(kwargs['user'])
            complete_authkey = CompleteAuthkey(user_authkey, ProjectAuthkey(kwargs['project']))
            project_infos = self.project_handler.get_user_project_status(complete_authkey)
            if project_infos is not None:
                rnac = RNAHandler()
                return Common.get_template('page_result.tmpl').render(
                    useruuid=user_authkey.get_key(),
                    project_infos=project_infos,
                    actors_list=sorted(list(rnac.get_actors(project_infos['id']))),
                    targets_list=sorted(list(rnac.get_targets(project_infos['id']))),
                    regions_list=self.interaction_handler.get_involved_regions(complete_authkey),
                    softwares_list=[s for s,v in project_infos['softwares'].items() if v == True]
                )
            else:
                return Common.get_template('page_error.tmpl').render(error=DEFAULT_ERROR_404)
        else:
            return Common.get_template('page_error.tmpl').render(error=DEFAULT_ERROR_404)

    @cherrypy.expose
    def outputs(self, **kwargs):
        if len(list(kwargs.keys())) == 2 and 'user' in list(kwargs.keys()) and 'project' in list(kwargs.keys()):
            user_authkey = UserAuthkey(kwargs['user'])
            project_authkey = ProjectAuthkey(kwargs['project'])
            complete_authkey = CompleteAuthkey(user_authkey, project_authkey)
            project_infos = self.project_handler.get_user_project_status(complete_authkey)
            if project_infos is not None:
                return Common.get_template('page_outputs.tmpl').render(
                    useruuid = user_authkey.get_key(),
                    projectuuid = project_authkey.get_key(),
                    project_infos = project_infos
                )
            else:
                return Common.get_template('page_error.tmpl').render(error=DEFAULT_ERROR_404)
        else:
            return Common.get_template('page_error.tmpl').render(error=DEFAULT_ERROR_404)

    @cherrypy.expose
    def forna(self, **kwargs):
        if len(list(kwargs.keys())) == 3 and 'user' in list(kwargs.keys())\
            and 'project' in list(kwargs.keys()) and 'interaction' in list(kwargs.keys()):
            user_authkey = UserAuthkey(kwargs['user'])
            project_authkey = ProjectAuthkey(kwargs['project'])
            complete_authkey = CompleteAuthkey(user_authkey, project_authkey)
            project_infos = self.project_handler.get_user_project_status(complete_authkey)
            ncrna_infos = self.interaction_handler.get_interaction_ncrna_infos(complete_authkey, kwargs['interaction'])
            if project_infos is not None and ncrna_infos is not None:
                return Common.get_template('page_forna.tmpl').render(
                    useruuid = user_authkey.get_key(),
                    projectuuid = project_authkey.get_key(),
                    project_infos = project_infos,
                    ncrna_infos = ncrna_infos,
                    strand_plus = Strand.plus
                )
            else:
                return Common.get_template('page_error.tmpl').render(error=DEFAULT_ERROR_404)
        else:
            return Common.get_template('page_error.tmpl').render(error=DEFAULT_ERROR_404)

""" default 404 error description """
DEFAULT_ERROR_404 = {
    "code": 404,
    "desc": "page not found",
    "msg": "The page doesn't exist."
}

""" default 500 error description """
DEFAULT_ERROR_500 = {
    "code": 500,
    "desc": "An error occurred",
    "msg": "We are sorry, an error occurred on the server. Please, retry later."
}

def error_page_404(status, message, traceback, version):
    return Common.get_template('page_error.tmpl').render(error=DEFAULT_ERROR_404)

def error_unexpected():
    return Common.get_template('page_error.tmpl').render(error=DEFAULT_ERROR_500)


    ## ---------------------------------------------------------------------------
    ## MAIN

if __name__ == '__main__':

    STATIC_WEB_DIR = os.path.abspath(os.path.join(srnatabac.__path__[0], "ui/"))

    parser = argparse.ArgumentParser()
    parser.add_argument("--daemon", action="store_true", dest="daemon",
        default=False, help="Run the server as daemon")
    args = vars(parser.parse_args())
    # define the socket host and port
    confreader = SrnatabacConfigReader()
    socket_opts = confreader.get_socket_options()
    # app config
    app_conf = {
        '/':
            {'tools.staticdir.root': STATIC_WEB_DIR},
        os.path.join('/', 'data'):
            {'tools.staticdir.on'  : True, 'tools.staticdir.dir' : confreader.get_work_directory()},
        os.path.join('/', 'css'):
            {'tools.staticdir.on'  : True, 'tools.staticdir.dir' : './asset/css/'},
        os.path.join('/', 'js'):
            {'tools.staticdir.on'  : True, 'tools.staticdir.dir' : './asset/js/'},
        os.path.join('/', 'images'):
            {'tools.staticdir.on'  : True, 'tools.staticdir.dir' : './asset/img/'},
        os.path.join('/', 'img'):
            {'tools.staticdir.on'  : True, 'tools.staticdir.dir' : './asset/img/'},
        os.path.join('/', 'fonts'):
            {'tools.staticdir.on'  : True, 'tools.staticdir.dir' : './asset/fonts/'},
        os.path.join('/', 'sample'):
            {'tools.staticdir.on'  : True, 'tools.staticdir.dir' : './asset/sample/'}
    }

    # socket options
    cherrypy.config.update({'server.socket_host': socket_opts[0], 'server.socket_port': socket_opts[1]})

    # error handlers
    cherrypy.config.update({'error_page.404': error_page_404 })
    cherrypy.config.update({'error_page.500': error_unexpected })
    cherrypy.config.update({'request.error_response': error_unexpected })

    # start the server
    SRNATabacServer.quickstart(SRNATabacServer, app_conf, daemon=args["daemon"])
